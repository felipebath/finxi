(function(){
	var validador = new Validador(),
		interface = new Interface(),

		nome_empresa_cadastrar_administrador = $('#nome_empresa_cadastrar_administrador'),
		usuario_cadastrar_administrador = $('#usuario_cadastrar_administrador'),
		senha_cadastrar_administrador = $('#senha_cadastrar_administrador'),
		permissao_cliente_cadastrar_administrador = $('#permissao_cliente_cadastrar_administrador'),
		permissao_colaborador_cadastrar_administrador = $('#permissao_colaborador_cadastrar_administrador'),
		permissao_financeiro_cadastrar_administrador = $('#permissao_financeiro_cadastrar_administrador'),
		permissao_conta_cadastrar_administrador = $('#permissao_conta_cadastrar_administrador'),
		permissao_estoque_cadastrar_administrador = $('#permissao_estoque_cadastrar_administrador'),
		permissao_fornecedor_cadastrar_administrador = $('#permissao_fornecedor_cadastrar_administrador'),
		permissao_produto_cadastrar_administrador = $('#permissao_produto_cadastrar_administrador'),
		permissao_servico_cadastrar_administrador = $('#permissao_servico_cadastrar_administrador'),
		permissao_banco_cadastrar_administrador = $('#permissao_banco_cadastrar_administrador'),
		permissao_centro_custo_cadastrar_administrador = $('#permissao_centro_custo_cadastrar_administrador'),

		alerta_nome_empresa_cadastrar_administrador = $('#alerta_nome_empresa_cadastrar_administrador'),
		alerta_usuario_cadastrar_administrador = $('#alerta_usuario_cadastrar_administrador'),
		alerta_senha_cadastrar_administrador = $('#alerta_senha_cadastrar_administrador'),

		codigo_empresa_editar_administrador = $('#codigo_empresa_editar_administrador').val(),
		nome_empresa_editar_administrador = $('#nome_empresa_editar_administrador'),
		usuario_editar_administrador = $('#usuario_editar_administrador'),
		senha_editar_administrador = $('#senha_editar_administrador'),
		permissao_cliente_editar_administrador = $('#permissao_cliente_editar_administrador'),
		permissao_colaborador_editar_administrador = $('#permissao_colaborador_editar_administrador'),
		permissao_financeiro_editar_administrador = $('#permissao_financeiro_editar_administrador'),
		permissao_conta_editar_administrador = $('#permissao_conta_editar_administrador'),
		permissao_estoque_editar_administrador = $('#permissao_estoque_editar_administrador'),
		permissao_fornecedor_editar_administrador = $('#permissao_fornecedor_editar_administrador'),
		permissao_produto_editar_administrador = $('#permissao_produto_editar_administrador'),
		permissao_servico_editar_administrador = $('#permissao_servico_editar_administrador'),
		permissao_banco_editar_administrador = $('#permissao_banco_editar_administrador'),
		permissao_centro_custo_editar_administrador = $('#permissao_centro_custo_editar_administrador'),

		alerta_nome_empresa_editar_administrador = $('#alerta_nome_empresa_editar_administrador'),
		alerta_usuario_editar_administrador = $('#alerta_usuario_editar_administrador'),
		alerta_senha_editar_administrador = $('#alerta_senha_editar_administrador');


	// seleciona todos as permissoes
	$('#selecionar_todos_permissao_cadastrar_administrador,#selecionar_todos_permissao_editar_administrador').change(function(){
		if($(this).is(':checked')){
			permissao_conta_cadastrar_administrador.prop('checked','checked');
			permissao_produto_cadastrar_administrador.prop('checked','checked');
			permissao_financeiro_cadastrar_administrador.prop('checked','checked');
			permissao_colaborador_cadastrar_administrador.prop('checked','checked');
			permissao_cliente_cadastrar_administrador.prop('checked','checked');
			permissao_fornecedor_cadastrar_administrador.prop('checked','checked');
			permissao_estoque_cadastrar_administrador.prop('checked','checked');
			permissao_servico_cadastrar_administrador.prop('checked','checked');
			permissao_banco_cadastrar_administrador.prop('checked','checked');
			permissao_centro_custo_cadastrar_administrador.prop('checked','checked');

			permissao_conta_editar_administrador.prop('checked','checked');
			permissao_produto_editar_administrador.prop('checked','checked');
			permissao_financeiro_editar_administrador.prop('checked','checked');
			permissao_colaborador_editar_administrador.prop('checked','checked');
			permissao_cliente_editar_administrador.prop('checked','checked');
			permissao_fornecedor_editar_administrador.prop('checked','checked');
			permissao_estoque_editar_administrador.prop('checked','checked');
			permissao_servico_editar_administrador.prop('checked','checked');
			permissao_banco_editar_administrador.prop('checked','checked');
			permissao_centro_custo_editar_administrador.prop('checked','checked');

		}else{
			permissao_conta_cadastrar_administrador.prop('checked','');
			permissao_produto_cadastrar_administrador.prop('checked','');
			permissao_financeiro_cadastrar_administrador.prop('checked','');
			permissao_colaborador_cadastrar_administrador.prop('checked','');
			permissao_cliente_cadastrar_administrador.prop('checked','');
			permissao_fornecedor_cadastrar_administrador.prop('checked','');
			permissao_estoque_cadastrar_administrador.prop('checked','');
			permissao_servico_cadastrar_administrador.prop('checked','');
			permissao_banco_cadastrar_administrador.prop('checked','');
			permissao_centro_custo_cadastrar_administrador.prop('checked','');


			permissao_conta_editar_administrador.prop('checked','');
			permissao_produto_editar_administrador.prop('checked','');
			permissao_financeiro_editar_administrador.prop('checked','');
			permissao_colaborador_editar_administrador.prop('checked','');
			permissao_cliente_editar_administrador.prop('checked','');
			permissao_fornecedor_editar_administrador.prop('checked','');
			permissao_estoque_editar_administrador.prop('checked','');
			permissao_servico_editar_administrador.prop('checked','');
			permissao_banco_editar_administrador.prop('checked','');
			permissao_centro_custo_editar_administrador.prop('checked','');

		}
	});

							// CADASTRAR
	// -------------------------------------------------------------------------

	senha_cadastrar_administrador.blur(function(){
		if(validador.requerido(senha_cadastrar_administrador.val())){
			if(!validador.senha(senha_cadastrar_administrador.val())){
				alerta_senha_cadastrar_administrador.text('Formato inválido.');
			}else{
				alerta_senha_cadastrar_administrador.text('');
			}
		}
	});

	$('#formulario_cadastrar_administrador').submit(function(){
		if(!validador.requerido(nome_empresa_cadastrar_administrador.val())){
			alerta_nome_empresa_cadastrar_administrador.text('Campo obrigatório.');
			nome_empresa_cadastrar_administrador.focus();

			return false;
		}else{
			alerta_nome_empresa_cadastrar_administrador.text('');
		}

		if(!validador.requerido(usuario_cadastrar_administrador.val())){
			alerta_usuario_cadastrar_administrador.text('Campo obrigatório.');
			usuario_cadastrar_administrador.focus();

			return false;
		}else{
			alerta_usuario_cadastrar_administrador.text('');
		}

		if(!validador.requerido(senha_cadastrar_administrador.val())){
			alerta_senha_cadastrar_administrador.text('Campo obrigatório.');
			senha_cadastrar_administrador.focus();

			return false;
		}else{
			alerta_senha_cadastrar_administrador.text('');
		}

		if(permissao_cliente_cadastrar_administrador.is(':checked') === false && permissao_conta_cadastrar_administrador.is(':checked') === false && permissao_produto_cadastrar_administrador.is(':checked') === false && permissao_financeiro_cadastrar_administrador.is(':checked') === false && permissao_colaborador_cadastrar_administrador.is(':checked') === false && permissao_fornecedor_cadastrar_administrador.is(':checked') === false && permissao_estoque_cadastrar_administrador.is(':checked') === false && permissao_servico_cadastrar_administrador.is(':checked') === false && permissao_banco_cadastrar_administrador.is(':checked') === false && permissao_centro_custo_cadastrar_administrador.is(':checked') === false){
			interface.exibirAlerta('Existe um aviso para você','Selecione pelo menos uma permissão.');
            return false;
		}

		$.ajax({
			url:$(this).attr('action'),
			type:'POST',
			dataType:'json',
			data:{
				nome_empresa_cadastrar_administrador:nome_empresa_cadastrar_administrador.val(),
				usuario_cadastrar_administrador:usuario_cadastrar_administrador.val(),
				senha_cadastrar_administrador:senha_cadastrar_administrador.val(),
				permissao_cliente_cadastrar_administrador:permissao_cliente_cadastrar_administrador.is(':checked'),
				permissao_colaborador_cadastrar_administrador:permissao_colaborador_cadastrar_administrador.is(':checked'),
				permissao_conta_cadastrar_administrador:permissao_conta_cadastrar_administrador.is(':checked'),
				permissao_produto_cadastrar_administrador:permissao_produto_cadastrar_administrador.is(':checked'),
				permissao_financeiro_cadastrar_administrador:permissao_financeiro_cadastrar_administrador.is(':checked'),
				permissao_fornecedor_cadastrar_administrador:permissao_fornecedor_cadastrar_administrador.is(':checked'),
				permissao_estoque_cadastrar_administrador:permissao_estoque_cadastrar_administrador.is(':checked'),
				permissao_servico_cadastrar_administrador:permissao_servico_cadastrar_administrador.is(':checked'),
				permissao_banco_cadastrar_administrador:permissao_banco_cadastrar_administrador.is(':checked'),
				permissao_centro_custo_cadastrar_administrador:permissao_centro_custo_cadastrar_administrador.is(':checked')


			},
                
            beforeSend:function(){
                interface.iniciar_carregamento();
            },

            success:function(json){
                if(json !== undefined){
                    if(json.alerta !== undefined){
                        interface.exibirAlerta('Existe um aviso para você',json.alerta);
                        interface.finalizar_carregamento();
                        return false;
                    }

                    if(json.sucesso === 'true'){
                       window.location.href = base_url + 'administrador';
                    }
                }
            }
		});

		return false;
	});

							// EDITAR
	// -------------------------------------------------------------------------

	senha_editar_administrador.blur(function(){
		if(validador.requerido(senha_editar_administrador.val())){
			if(!validador.senha(senha_editar_administrador.val())){
				alerta_senha_editar_administrador.text('Formato inválido.');
			}else{
				alerta_senha_editar_administrador.text('');
			}
		}
	});

	$('#formulario_editar_administrador').submit(function(){
		if(!validador.requerido(nome_empresa_editar_administrador.val())){
			alerta_nome_empresa_editar_administrador.text('Campo obrigatório.');
			nome_empresa_editar_administrador.focus();

			return false;
		}else{
			alerta_nome_empresa_editar_administrador.text('');
		}

		if(!validador.requerido(usuario_editar_administrador.val())){
			alerta_usuario_editar_administrador.text('Campo obrigatório.');
			usuario_editar_administrador.focus();

			return false;
		}else{
			alerta_usuario_editar_administrador.text('');
		}

		if(!validador.requerido(senha_editar_administrador.val())){
			alerta_senha_editar_administrador.text('Campo obrigatório.');
			senha_editar_administrador.focus();

			return false;
		}else{
			alerta_senha_editar_administrador.text('');
		}

		if(permissao_cliente_editar_administrador.is(':checked') === false && permissao_conta_editar_administrador.is(':checked') === false && permissao_produto_cadastrar_administrador.is(':checked') === false && permissao_financeiro_cadastrar_administrador.is(':checked') === false && permissao_colaborador_cadastrar_administrador.is(':checked') === false && permissao_fornecedor_cadastrar_administrador.is(':checked') === false && permissao_estoque_cadastrar_administrador.is(':checked') === false && permissao_servico_cadastrar_administrador.is(':checked') === false && permissao_banco_cadastrar_administrador.is(':checked') === false && permissao_centro_custo_cadastrar_administrador.is(':checked') === false){
			interface.exibirAlerta('Existe um aviso para você','Selecione pelo menos uma permissão.');
            return false;
		}

		$.ajax({
			url:$(this).attr('action'),
			type:'POST',
			dataType:'json',
			data:{
				codigo_empresa_editar_administrador:codigo_empresa_editar_administrador,
				nome_empresa_editar_administrador:nome_empresa_editar_administrador.val(),
				usuario_editar_administrador:usuario_editar_administrador.val(),
				senha_editar_administrador:senha_editar_administrador.val(),
				permissao_cliente_editar_administrador:permissao_cliente_editar_administrador.is(':checked'),
				permissao_colaborador_editar_administrador:permissao_colaborador_editar_administrador.is(':checked'),
				permissao_conta_editar_administrador:permissao_conta_editar_administrador.is(':checked'),
				permissao_produto_editar_administrador:permissao_produto_editar_administrador.is(':checked'),
				permissao_financeiro_editar_administrador:permissao_financeiro_editar_administrador.is(':checked'),
				permissao_fornecedor_editar_administrador:permissao_fornecedor_editar_administrador.is(':checked'),
				permissao_estoque_editar_administrador:permissao_estoque_editar_administrador.is(':checked'),
				permissao_servico_editar_administrador:permissao_servico_editar_administrador.is(':checked'),
				permissao_banco_editar_administrador:permissao_banco_editar_administrador.is(':checked'),
				permissao_centro_custo_editar_administrador:permissao_centro_custo_editar_administrador.is(':checked')


			},
                
            beforeSend:function(){
                interface.iniciar_carregamento();
            },

            success:function(json){
                if(json !== undefined){
                    if(json.alerta !== undefined){
                        interface.exibirAlerta('Existe um aviso para você',json.alerta);
                        interface.finalizar_carregamento();
                        return false;
                    }

                    if(json.sucesso === 'true'){
                       window.location.href = base_url + 'administrador';
                    }
                }
            }
		});

		return false;
	});

									// EXCLUSAO
	// -------------------------------------------------------------------------
	$('.deletar_administrador').click(function(){
		var url = $(this).attr('href');

		interface.exibirAlertaConfirmacaoRedirecionamento(
			'Pedido de exclusão',
			'Você deseja mesmo excluir esse administrador?',
			url,
			base_url + 'administrador'
		);

		return false;
	});
})();