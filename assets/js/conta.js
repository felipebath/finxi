(function(){
	var validador = new Validador(),
		interface = new Interface(),

		usuario_entrar_conta = $('#usuario_entrar_conta'),
		senha_entrar_conta = $('#senha_entrar_conta'),

		alerta_usuario_entrar_conta = $('#alerta_usuario_entrar_conta'),
		alerta_senha_entrar_conta = $('#alerta_senha_entrar_conta'),

		usuario_cadastrar_conta = $('#usuario_cadastrar_conta'),
		senha_cadastrar_conta = $('#senha_cadastrar_conta'),

		alerta_usuario_cadastrar_conta = $('#alerta_usuario_cadastrar_conta'),
		alerta_senha_cadastrar_conta = $('#alerta_senha_cadastrar_conta'),

		permissao_cadastrar_conta_cadastrar_conta = $('#permissao_cadastrar_conta_cadastrar_conta'),
		permissao_visualizar_conta_cadastrar_conta = $('#permissao_visualizar_conta_cadastrar_conta'),
		permissao_editar_conta_cadastrar_conta = $('#permissao_editar_conta_cadastrar_conta'),
		permissao_excluir_conta_cadastrar_conta = $('#permissao_excluir_conta_cadastrar_conta'),

		permissao_cadastrar_cliente_cadastrar_conta = $('#permissao_cadastrar_cliente_cadastrar_conta'),
		permissao_visualizar_cliente_cadastrar_conta = $('#permissao_visualizar_cliente_cadastrar_conta'),
		permissao_editar_cliente_cadastrar_conta = $('#permissao_editar_cliente_cadastrar_conta'),
		permissao_excluir_cliente_cadastrar_conta = $('#permissao_excluir_cliente_cadastrar_conta'),

		permissao_cadastrar_estoque_cadastrar_conta = $('#permissao_cadastrar_estoque_cadastrar_conta'),
		permissao_visualizar_estoque_cadastrar_conta = $('#permissao_visualizar_estoque_cadastrar_conta'),
		permissao_editar_estoque_cadastrar_conta = $('#permissao_editar_estoque_cadastrar_conta'),
		permissao_excluir_estoque_cadastrar_conta = $('#permissao_excluir_estoque_cadastrar_conta'),

		permissao_cadastrar_financeiro_cadastrar_conta = $('#permissao_cadastrar_financeiro_cadastrar_conta'),
		permissao_visualizar_financeiro_cadastrar_conta = $('#permissao_visualizar_financeiro_cadastrar_conta'),
		permissao_editar_financeiro_cadastrar_conta = $('#permissao_editar_financeiro_cadastrar_conta'),
		permissao_excluir_financeiro_cadastrar_conta = $('#permissao_excluir_financeiro_cadastrar_conta'),

		permissao_cadastrar_produto_cadastrar_conta = $('#permissao_cadastrar_produto_cadastrar_conta'),
		permissao_visualizar_produto_cadastrar_conta = $('#permissao_visualizar_produto_cadastrar_conta'),
		permissao_editar_produto_cadastrar_conta = $('#permissao_editar_produto_cadastrar_conta'),
		permissao_excluir_produto_cadastrar_conta = $('#permissao_excluir_produto_cadastrar_conta'),

		permissao_cadastrar_servico_cadastrar_conta = $('#permissao_cadastrar_servico_cadastrar_conta'),
		permissao_visualizar_servico_cadastrar_conta = $('#permissao_visualizar_servico_cadastrar_conta'),
		permissao_editar_servico_cadastrar_conta = $('#permissao_editar_servico_cadastrar_conta'),
		permissao_excluir_servico_cadastrar_conta = $('#permissao_excluir_servico_cadastrar_conta'),

		permissao_cadastrar_colaborador_cadastrar_conta = $('#permissao_cadastrar_colaborador_cadastrar_conta'),
		permissao_visualizar_colaborador_cadastrar_conta = $('#permissao_visualizar_colaborador_cadastrar_conta'),
		permissao_editar_colaborador_cadastrar_conta = $('#permissao_editar_colaborador_cadastrar_conta'),
		permissao_excluir_colaborador_cadastrar_conta = $('#permissao_excluir_colaborador_cadastrar_conta'),

		permissao_cadastrar_fornecedor_cadastrar_conta = $('#permissao_cadastrar_fornecedor_cadastrar_conta'),
		permissao_visualizar_fornecedor_cadastrar_conta = $('#permissao_visualizar_fornecedor_cadastrar_conta'),
		permissao_editar_fornecedor_cadastrar_conta = $('#permissao_editar_fornecedor_cadastrar_conta'),
		permissao_excluir_fornecedor_cadastrar_conta = $('#permissao_excluir_fornecedor_cadastrar_conta'),

		permissao_cadastrar_centro_custo_cadastrar_conta = $('#permissao_cadastrar_centro_custo_cadastrar_conta'),
		permissao_visualizar_centro_custo_cadastrar_conta = $('#permissao_visualizar_centro_custo_cadastrar_conta'),
		permissao_editar_centro_custo_cadastrar_conta = $('#permissao_editar_centro_custo_cadastrar_conta'),
		permissao_excluir_centro_custo_cadastrar_conta = $('#permissao_excluir_centro_custo_cadastrar_conta'),

		permissao_cadastrar_banco_cadastrar_conta = $('#permissao_cadastrar_banco_cadastrar_conta'),
		permissao_visualizar_banco_cadastrar_conta = $('#permissao_visualizar_banco_cadastrar_conta'),
		permissao_editar_banco_cadastrar_conta = $('#permissao_editar_banco_cadastrar_conta'),
		permissao_excluir_banco_cadastrar_conta = $('#permissao_excluir_banco_cadastrar_conta'),

		usuario_cadastrar_conta = $('#usuario_cadastrar_conta'),
		senha_cadastrar_conta = $('#senha_cadastrar_conta'),

		alerta_usuario_cadastrar_conta = $('#alerta_usuario_cadastrar_conta'),
		alerta_senha_cadastrar_conta = $('#alerta_senha_cadastrar_conta'),

		permissao_cadastrar_conta_cadastrar_conta = $('#permissao_cadastrar_conta_cadastrar_conta'),
		permissao_visualizar_conta_cadastrar_conta = $('#permissao_visualizar_conta_cadastrar_conta'),
		permissao_editar_conta_cadastrar_conta = $('#permissao_editar_conta_cadastrar_conta'),
		permissao_excluir_conta_cadastrar_conta = $('#permissao_excluir_conta_cadastrar_conta'),

		permissao_cadastrar_cliente_cadastrar_conta = $('#permissao_cadastrar_cliente_cadastrar_conta'),
		permissao_visualizar_cliente_cadastrar_conta = $('#permissao_visualizar_cliente_cadastrar_conta'),
		permissao_editar_cliente_cadastrar_conta = $('#permissao_editar_cliente_cadastrar_conta'),
		permissao_excluir_cliente_cadastrar_conta = $('#permissao_excluir_cliente_cadastrar_conta'),

		permissao_cadastrar_estoque_cadastrar_conta = $('#permissao_cadastrar_estoque_cadastrar_conta'),
		permissao_visualizar_estoque_cadastrar_conta = $('#permissao_visualizar_estoque_cadastrar_conta'),
		permissao_editar_estoque_cadastrar_conta = $('#permissao_editar_estoque_cadastrar_conta'),
		permissao_excluir_estoque_cadastrar_conta = $('#permissao_excluir_estoque_cadastrar_conta'),

		permissao_cadastrar_financeiro_cadastrar_conta = $('#permissao_cadastrar_financeiro_cadastrar_conta'),
		permissao_visualizar_financeiro_cadastrar_conta = $('#permissao_visualizar_financeiro_cadastrar_conta'),
		permissao_editar_financeiro_cadastrar_conta = $('#permissao_editar_financeiro_cadastrar_conta'),
		permissao_excluir_financeiro_cadastrar_conta = $('#permissao_excluir_financeiro_cadastrar_conta'),

		permissao_cadastrar_produto_cadastrar_conta = $('#permissao_cadastrar_produto_cadastrar_conta'),
		permissao_visualizar_produto_cadastrar_conta = $('#permissao_visualizar_produto_cadastrar_conta'),
		permissao_editar_produto_cadastrar_conta = $('#permissao_editar_produto_cadastrar_conta'),
		permissao_excluir_produto_cadastrar_conta = $('#permissao_excluir_produto_cadastrar_conta'),

		permissao_cadastrar_servico_cadastrar_conta = $('#permissao_cadastrar_servico_cadastrar_conta'),
		permissao_visualizar_servico_cadastrar_conta = $('#permissao_visualizar_servico_cadastrar_conta'),
		permissao_editar_servico_cadastrar_conta = $('#permissao_editar_servico_cadastrar_conta'),
		permissao_excluir_servico_cadastrar_conta = $('#permissao_excluir_servico_cadastrar_conta'),

		permissao_cadastrar_colaborador_cadastrar_conta = $('#permissao_cadastrar_colaborador_cadastrar_conta'),
		permissao_visualizar_colaborador_cadastrar_conta = $('#permissao_visualizar_colaborador_cadastrar_conta'),
		permissao_editar_colaborador_cadastrar_conta = $('#permissao_editar_colaborador_cadastrar_conta'),
		permissao_excluir_colaborador_cadastrar_conta = $('#permissao_excluir_colaborador_cadastrar_conta'),

		permissao_cadastrar_fornecedor_cadastrar_conta = $('#permissao_cadastrar_fornecedor_cadastrar_conta'),
		permissao_visualizar_fornecedor_cadastrar_conta = $('#permissao_visualizar_fornecedor_cadastrar_conta'),
		permissao_editar_fornecedor_cadastrar_conta = $('#permissao_editar_fornecedor_cadastrar_conta'),
		permissao_excluir_fornecedor_cadastrar_conta = $('#permissao_excluir_fornecedor_cadastrar_conta'),

		permissao_cadastrar_centro_custo_cadastrar_conta = $('#permissao_cadastrar_centro_custo_cadastrar_conta'),
		permissao_visualizar_centro_custo_cadastrar_conta = $('#permissao_visualizar_centro_custo_cadastrar_conta'),
		permissao_editar_centro_custo_cadastrar_conta = $('#permissao_editar_centro_custo_cadastrar_conta'),
		permissao_excluir_centro_custo_cadastrar_conta = $('#permissao_excluir_centro_custo_cadastrar_conta'),
		
		permissao_cadastrar_banco_cadastrar_conta = $('#permissao_cadastrar_banco_cadastrar_conta'),
		permissao_visualizar_banco_cadastrar_conta = $('#permissao_visualizar_banco_cadastrar_conta'),
		permissao_editar_banco_cadastrar_conta = $('#permissao_editar_banco_cadastrar_conta'),
		permissao_excluir_banco_cadastrar_conta = $('#permissao_excluir_banco_cadastrar_conta'),

		usuario_editar_conta = $('#usuario_editar_conta'),
		senha_editar_conta = $('#senha_editar_conta'),

		alerta_usuario_editar_conta = $('#alerta_usuario_editar_conta'),
		alerta_senha_editar_conta = $('#alerta_senha_editar_conta'),

		permissao_cadastrar_conta_editar_conta = $('#permissao_cadastrar_conta_editar_conta'),
		permissao_visualizar_conta_editar_conta = $('#permissao_visualizar_conta_editar_conta'),
		permissao_editar_conta_editar_conta = $('#permissao_editar_conta_editar_conta'),
		permissao_excluir_conta_editar_conta = $('#permissao_excluir_conta_editar_conta'),

		permissao_cadastrar_cliente_editar_conta = $('#permissao_cadastrar_cliente_editar_conta'),
		permissao_visualizar_cliente_editar_conta = $('#permissao_visualizar_cliente_editar_conta'),
		permissao_editar_cliente_editar_conta = $('#permissao_editar_cliente_editar_conta'),
		permissao_excluir_cliente_editar_conta = $('#permissao_excluir_cliente_editar_conta'),

		permissao_cadastrar_estoque_editar_conta = $('#permissao_cadastrar_estoque_editar_conta'),
		permissao_visualizar_estoque_editar_conta = $('#permissao_visualizar_estoque_editar_conta'),
		permissao_editar_estoque_editar_conta = $('#permissao_editar_estoque_editar_conta'),
		permissao_excluir_estoque_editar_conta = $('#permissao_excluir_estoque_editar_conta'),

		permissao_cadastrar_financeiro_editar_conta = $('#permissao_cadastrar_financeiro_editar_conta'),
		permissao_visualizar_financeiro_editar_conta = $('#permissao_visualizar_financeiro_editar_conta'),
		permissao_editar_financeiro_editar_conta = $('#permissao_editar_financeiro_editar_conta'),
		permissao_excluir_financeiro_editar_conta = $('#permissao_excluir_financeiro_editar_conta'),

		permissao_cadastrar_produto_editar_conta = $('#permissao_cadastrar_produto_editar_conta'),
		permissao_visualizar_produto_editar_conta = $('#permissao_visualizar_produto_editar_conta'),
		permissao_editar_produto_editar_conta = $('#permissao_editar_produto_editar_conta'),
		permissao_excluir_produto_editar_conta = $('#permissao_excluir_produto_editar_conta'),

		permissao_cadastrar_servico_editar_conta = $('#permissao_cadastrar_servico_editar_conta'),
		permissao_visualizar_servico_editar_conta = $('#permissao_visualizar_servico_editar_conta'),
		permissao_editar_servico_editar_conta = $('#permissao_editar_servico_editar_conta'),
		permissao_excluir_servico_editar_conta = $('#permissao_excluir_servico_editar_conta'),

		permissao_cadastrar_colaborador_editar_conta = $('#permissao_cadastrar_colaborador_editar_conta'),
		permissao_visualizar_colaborador_editar_conta = $('#permissao_visualizar_colaborador_editar_conta'),
		permissao_editar_colaborador_editar_conta = $('#permissao_editar_colaborador_editar_conta'),
		permissao_excluir_colaborador_editar_conta = $('#permissao_excluir_colaborador_editar_conta'),

		permissao_cadastrar_fornecedor_editar_conta = $('#permissao_cadastrar_fornecedor_editar_conta'),
		permissao_visualizar_fornecedor_editar_conta = $('#permissao_visualizar_fornecedor_editar_conta'),
		permissao_editar_fornecedor_editar_conta = $('#permissao_editar_fornecedor_editar_conta'),
		permissao_excluir_fornecedor_editar_conta = $('#permissao_excluir_fornecedor_editar_conta');

		permissao_cadastrar_centro_custo_editar_conta = $('#permissao_cadastrar_centro_custo_editar_conta'),
		permissao_visualizar_centro_custo_editar_conta = $('#permissao_visualizar_centro_custo_editar_conta'),
		permissao_editar_centro_custo_editar_conta = $('#permissao_editar_centro_custo_editar_conta'),
		permissao_excluir_centro_custo_editar_conta = $('#permissao_excluir_centro_custo_editar_conta'),
		
		permissao_cadastrar_banco_editar_conta = $('#permissao_cadastrar_banco_editar_conta'),
		permissao_visualizar_banco_editar_conta = $('#permissao_visualizar_banco_editar_conta'),
		permissao_editar_banco_editar_conta = $('#permissao_editar_banco_editar_conta'),
		permissao_excluir_banco_editar_conta = $('#permissao_excluir_banco_editar_conta'),

	// seleciona todos as permissoes
	$('#selecionar_todos_permissao_cadastrar_conta,#selecionar_todos_permissao_editar_conta').change(function(){
		if($(this).is(':checked')){
			permissao_cadastrar_conta_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_conta_cadastrar_conta.prop('checked','checked');
			permissao_editar_conta_cadastrar_conta.prop('checked','checked');
			permissao_excluir_conta_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_conta_editar_conta.prop('checked','checked');
			permissao_visualizar_conta_editar_conta.prop('checked','checked');
			permissao_editar_conta_editar_conta.prop('checked','checked');
			permissao_excluir_conta_editar_conta.prop('checked','checked');

			permissao_cadastrar_cliente_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_cliente_cadastrar_conta.prop('checked','checked');
			permissao_editar_cliente_cadastrar_conta.prop('checked','checked');
			permissao_excluir_cliente_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_cliente_editar_conta.prop('checked','checked');
			permissao_visualizar_cliente_editar_conta.prop('checked','checked');
			permissao_editar_cliente_editar_conta.prop('checked','checked');
			permissao_excluir_cliente_editar_conta.prop('checked','checked');

			permissao_cadastrar_estoque_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_estoque_cadastrar_conta.prop('checked','checked');
			permissao_editar_estoque_cadastrar_conta.prop('checked','checked');
			permissao_excluir_estoque_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_estoque_editar_conta.prop('checked','checked');
			permissao_visualizar_estoque_editar_conta.prop('checked','checked');
			permissao_editar_estoque_editar_conta.prop('checked','checked');
			permissao_excluir_estoque_editar_conta.prop('checked','checked');

			permissao_cadastrar_financeiro_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_financeiro_cadastrar_conta.prop('checked','checked');
			permissao_editar_financeiro_cadastrar_conta.prop('checked','checked');
			permissao_excluir_financeiro_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_financeiro_editar_conta.prop('checked','checked');
			permissao_visualizar_financeiro_editar_conta.prop('checked','checked');
			permissao_editar_financeiro_editar_conta.prop('checked','checked');
			permissao_excluir_financeiro_editar_conta.prop('checked','checked');

			permissao_cadastrar_produto_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_produto_cadastrar_conta.prop('checked','checked');
			permissao_editar_produto_cadastrar_conta.prop('checked','checked');
			permissao_excluir_produto_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_produto_editar_conta.prop('checked','checked');
			permissao_visualizar_produto_editar_conta.prop('checked','checked');
			permissao_editar_produto_editar_conta.prop('checked','checked');
			permissao_excluir_produto_editar_conta.prop('checked','checked');

			permissao_cadastrar_servico_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_servico_cadastrar_conta.prop('checked','checked');
			permissao_editar_servico_cadastrar_conta.prop('checked','checked');
			permissao_excluir_servico_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_servico_editar_conta.prop('checked','checked');
			permissao_visualizar_servico_editar_conta.prop('checked','checked');
			permissao_editar_servico_editar_conta.prop('checked','checked');
			permissao_excluir_servico_editar_conta.prop('checked','checked');

			permissao_cadastrar_colaborador_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_colaborador_cadastrar_conta.prop('checked','checked');
			permissao_editar_colaborador_cadastrar_conta.prop('checked','checked');
			permissao_excluir_colaborador_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_colaborador_editar_conta.prop('checked','checked');
			permissao_visualizar_colaborador_editar_conta.prop('checked','checked');
			permissao_editar_colaborador_editar_conta.prop('checked','checked');
			permissao_excluir_colaborador_editar_conta.prop('checked','checked');

			permissao_cadastrar_fornecedor_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_fornecedor_cadastrar_conta.prop('checked','checked');
			permissao_editar_fornecedor_cadastrar_conta.prop('checked','checked');
			permissao_excluir_fornecedor_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_fornecedor_editar_conta.prop('checked','checked');
			permissao_visualizar_fornecedor_editar_conta.prop('checked','checked');
			permissao_editar_fornecedor_editar_conta.prop('checked','checked');
			permissao_excluir_fornecedor_editar_conta.prop('checked','checked');

			permissao_cadastrar_centro_custo_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_centro_custo_cadastrar_conta.prop('checked','checked');
			permissao_editar_centro_custo_cadastrar_conta.prop('checked','checked');
			permissao_excluir_centro_custo_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_centro_custo_editar_conta.prop('checked','checked');
			permissao_visualizar_centro_custo_editar_conta.prop('checked','checked');
			permissao_editar_centro_custo_editar_conta.prop('checked','checked');
			permissao_excluir_centro_custo_editar_conta.prop('checked','checked');

			permissao_cadastrar_banco_cadastrar_conta.prop('checked','checked');
			permissao_visualizar_banco_cadastrar_conta.prop('checked','checked');
			permissao_editar_banco_cadastrar_conta.prop('checked','checked');
			permissao_excluir_banco_cadastrar_conta.prop('checked','checked');

			permissao_cadastrar_banco_editar_conta.prop('checked','checked');
			permissao_visualizar_banco_editar_conta.prop('checked','checked');
			permissao_editar_banco_editar_conta.prop('checked','checked');
			permissao_excluir_banco_editar_conta.prop('checked','checked');
		}else{
			permissao_cadastrar_conta_cadastrar_conta.prop('checked','');
			permissao_visualizar_conta_cadastrar_conta.prop('checked','');
			permissao_editar_conta_cadastrar_conta.prop('checked','');
			permissao_excluir_conta_cadastrar_conta.prop('checked','');

			permissao_cadastrar_conta_editar_conta.prop('checked','');
			permissao_visualizar_conta_editar_conta.prop('checked','');
			permissao_editar_conta_editar_conta.prop('checked','');
			permissao_excluir_conta_editar_conta.prop('checked','');

			permissao_cadastrar_cliente_cadastrar_conta.prop('checked','');
			permissao_visualizar_cliente_cadastrar_conta.prop('checked','');
			permissao_editar_cliente_cadastrar_conta.prop('checked','');
			permissao_excluir_cliente_cadastrar_conta.prop('checked','');

			permissao_cadastrar_cliente_editar_conta.prop('checked','');
			permissao_visualizar_cliente_editar_conta.prop('checked','');
			permissao_editar_cliente_editar_conta.prop('checked','');
			permissao_excluir_cliente_editar_conta.prop('checked','');

			permissao_cadastrar_estoque_cadastrar_conta.prop('checked','');
			permissao_visualizar_estoque_cadastrar_conta.prop('checked','');
			permissao_editar_estoque_cadastrar_conta.prop('checked','');
			permissao_excluir_estoque_cadastrar_conta.prop('checked','');

			permissao_cadastrar_estoque_editar_conta.prop('checked','');
			permissao_visualizar_estoque_editar_conta.prop('checked','');
			permissao_editar_estoque_editar_conta.prop('checked','');
			permissao_excluir_estoque_editar_conta.prop('checked','');

			permissao_cadastrar_financeiro_cadastrar_conta.prop('checked','');
			permissao_visualizar_financeiro_cadastrar_conta.prop('checked','');
			permissao_editar_financeiro_cadastrar_conta.prop('checked','');
			permissao_excluir_financeiro_cadastrar_conta.prop('checked','');

			permissao_cadastrar_financeiro_editar_conta.prop('checked','');
			permissao_visualizar_financeiro_editar_conta.prop('checked','');
			permissao_editar_financeiro_editar_conta.prop('checked','');
			permissao_excluir_financeiro_editar_conta.prop('checked','');

			permissao_cadastrar_produto_cadastrar_conta.prop('checked','');
			permissao_visualizar_produto_cadastrar_conta.prop('checked','');
			permissao_editar_produto_cadastrar_conta.prop('checked','');
			permissao_excluir_produto_cadastrar_conta.prop('checked','');

			permissao_cadastrar_produto_editar_conta.prop('checked','');
			permissao_visualizar_produto_editar_conta.prop('checked','');
			permissao_editar_produto_editar_conta.prop('checked','');
			permissao_excluir_produto_editar_conta.prop('checked','');

			permissao_cadastrar_servico_cadastrar_conta.prop('checked','');
			permissao_visualizar_servico_cadastrar_conta.prop('checked','');
			permissao_editar_servico_cadastrar_conta.prop('checked','');
			permissao_excluir_servico_cadastrar_conta.prop('checked','');

			permissao_cadastrar_servico_editar_conta.prop('checked','');
			permissao_visualizar_servico_editar_conta.prop('checked','');
			permissao_editar_servico_editar_conta.prop('checked','');
			permissao_excluir_servico_editar_conta.prop('checked','');

			permissao_cadastrar_colaborador_cadastrar_conta.prop('checked','');
			permissao_visualizar_colaborador_cadastrar_conta.prop('checked','');
			permissao_editar_colaborador_cadastrar_conta.prop('checked','');
			permissao_excluir_colaborador_cadastrar_conta.prop('checked','');

			permissao_cadastrar_colaborador_editar_conta.prop('checked','');
			permissao_visualizar_colaborador_editar_conta.prop('checked','');
			permissao_editar_colaborador_editar_conta.prop('checked','');
			permissao_excluir_colaborador_editar_conta.prop('checked','');

			permissao_cadastrar_fornecedor_cadastrar_conta.prop('checked','');
			permissao_visualizar_fornecedor_cadastrar_conta.prop('checked','');
			permissao_editar_fornecedor_cadastrar_conta.prop('checked','');
			permissao_excluir_fornecedor_cadastrar_conta.prop('checked','');

			permissao_cadastrar_fornecedor_editar_conta.prop('checked','');
			permissao_visualizar_fornecedor_editar_conta.prop('checked','');
			permissao_editar_fornecedor_editar_conta.prop('checked','');
			permissao_excluir_fornecedor_editar_conta.prop('checked','');

			permissao_cadastrar_centro_custo_cadastrar_conta.prop('checked','');
			permissao_visualizar_centro_custo_cadastrar_conta.prop('checked','');
			permissao_editar_centro_custo_cadastrar_conta.prop('checked','');
			permissao_excluir_centro_custo_cadastrar_conta.prop('checked','');

			permissao_cadastrar_centro_custo_editar_conta.prop('checked','');
			permissao_visualizar_centro_custo_editar_conta.prop('checked','');
			permissao_editar_centro_custo_editar_conta.prop('checked','');
			permissao_excluir_centro_custo_editar_conta.prop('checked','');

			permissao_cadastrar_banco_cadastrar_conta.prop('checked','');
			permissao_visualizar_banco_cadastrar_conta.prop('checked','');
			permissao_editar_banco_cadastrar_conta.prop('checked','');
			permissao_excluir_banco_cadastrar_conta.prop('checked','');

			permissao_cadastrar_banco_editar_conta.prop('checked','');
			permissao_visualizar_banco_editar_conta.prop('checked','');
			permissao_editar_banco_editar_conta.prop('checked','');
			permissao_excluir_banco_editar_conta.prop('checked','');
		}
	});	

								// ENTRAR
	// ----------------------------------------------------------------------
	senha_entrar_conta.blur(function(){
		if(validador.requerido(senha_entrar_conta.val())){
			if(!validador.senha(senha_entrar_conta.val())){
				alerta_senha_entrar_conta.text('Formato inválido.');
			}else{
				alerta_senha_entrar_conta.text('');
			}
		}
	});	
		
	$('#formulario_entrar_conta').submit(function(){
		if(!validador.requerido(usuario_entrar_conta.val())){
			alerta_usuario_entrar_conta.text('Campo obrigatório.');
			usuario_entrar_conta.focus();

			return false;
		}else{
			alerta_usuario_entrar_conta.text('');
		}

		if(!validador.requerido(senha_entrar_conta.val())){
			alerta_senha_entrar_conta.text('Campo obrigatório.');
			senha_entrar_conta.focus();

			return false;
		}else{
			if(!validador.senha(senha_entrar_conta.val())){
				alerta_senha_entrar_conta.text('Formato inválido.');

				return false;
			}else{
				alerta_senha_entrar_conta.text('');
			}
		}

		$.ajax({
			url:$(this).attr('action'),
			type:'post',
			dataType:'json',
			data:{
				usuario_entrar_conta:usuario_entrar_conta.val(),
				senha_entrar_conta:senha_entrar_conta.val()
			},

			beforeSend:function(){
                interface.iniciar_carregamento();
            },

            success:function(json){
                if(json !== undefined){
                    if(json.alerta !== undefined){
                        interface.exibirAlerta('Existe um aviso para você',json.alerta);
                        interface.finalizar_carregamento();
                        return false;
                    }

                    if(json.sucesso === 'true'){
                    	if(json.administrador !== undefined && json.administrador === 'true'){
	                    	window.location.href = base_url + 'administrador';
	                    }else{
	                   		window.location.href = base_url + 'inicio'; 	
	                    }	
                    }                    
                }
            }
		});

		return false;
	});

										// CADASTRAR
	// ----------------------------------------------------------------------------------------

	senha_cadastrar_conta.blur(function(){
		if(validador.requerido(senha_cadastrar_conta.val())){
			if(!validador.senha(senha_cadastrar_conta.val())){
				alerta_senha_cadastrar_conta.text('Formato inválido.');
			}else{
				alerta_senha_cadastrar_conta.text('');
			}
		}
	});	

	$('#formulario_cadastrar_conta').submit(function(){
		if(!validador.requerido(usuario_cadastrar_conta.val())){
			alerta_usuario_cadastrar_conta.text('Campo obrigatório.');
			usuario_cadastrar_conta.focus();

			return false;
		}else{
			alerta_usuario_cadastrar_conta.text('');
		}

		if(!validador.requerido(senha_cadastrar_conta.val())){
			alerta_senha_cadastrar_conta.text('Campo obrigatório.');
			senha_cadastrar_conta.focus();

			return false;
		}else{
			if(!validador.senha(senha_cadastrar_conta.val())){
				alerta_senha_cadastrar_conta.text('Formato inválido.');

				return false;
			}else{
				alerta_senha_cadastrar_conta.text('');
			}
		}

		$.ajax({
			url:$(this).attr('action'),
			type:'post',
			dataType:'json',
			data:{
				codigo_empresa_cadastrar_conta:$('#codigo_empresa_cadastrar_conta').val(),
				usuario_cadastrar_conta:usuario_cadastrar_conta.val(),
				senha_cadastrar_conta:senha_cadastrar_conta.val(),

				permissao_cadastrar_conta_cadastrar_conta:permissao_cadastrar_conta_cadastrar_conta.is(':checked'),
				permissao_visualizar_conta_cadastrar_conta:permissao_visualizar_conta_cadastrar_conta.is(':checked'),
				permissao_editar_conta_cadastrar_conta:permissao_editar_conta_cadastrar_conta.is(':checked'),
				permissao_excluir_conta_cadastrar_conta:permissao_excluir_conta_cadastrar_conta.is(':checked'),

				permissao_cadastrar_cliente_cadastrar_conta:permissao_cadastrar_cliente_cadastrar_conta.is(':checked'),
				permissao_visualizar_cliente_cadastrar_conta:permissao_visualizar_cliente_cadastrar_conta.is(':checked'),
				permissao_editar_cliente_cadastrar_conta:permissao_editar_cliente_cadastrar_conta.is(':checked'),
				permissao_excluir_cliente_cadastrar_conta:permissao_excluir_cliente_cadastrar_conta.is(':checked'),

				permissao_cadastrar_colaborador_cadastrar_conta:permissao_cadastrar_colaborador_cadastrar_conta.is(':checked'),
				permissao_visualizar_colaborador_cadastrar_conta:permissao_visualizar_colaborador_cadastrar_conta.is(':checked'),
				permissao_editar_colaborador_cadastrar_conta:permissao_editar_colaborador_cadastrar_conta.is(':checked'),
				permissao_excluir_colaborador_cadastrar_conta:permissao_excluir_colaborador_cadastrar_conta.is(':checked'),

				permissao_cadastrar_financeiro_cadastrar_conta:permissao_cadastrar_financeiro_cadastrar_conta.is(':checked'),
				permissao_visualizar_financeiro_cadastrar_conta:permissao_visualizar_financeiro_cadastrar_conta.is(':checked'),
				permissao_editar_financeiro_cadastrar_conta:permissao_editar_financeiro_cadastrar_conta.is(':checked'),
				permissao_excluir_financeiro_cadastrar_conta:permissao_excluir_financeiro_cadastrar_conta.is(':checked'),

				permissao_cadastrar_servico_cadastrar_conta:permissao_cadastrar_servico_cadastrar_conta.is(':checked'),
				permissao_visualizar_servico_cadastrar_conta:permissao_visualizar_servico_cadastrar_conta.is(':checked'),
				permissao_editar_servico_cadastrar_conta:permissao_editar_servico_cadastrar_conta.is(':checked'),
				permissao_excluir_servico_cadastrar_conta:permissao_excluir_servico_cadastrar_conta.is(':checked'),

				permissao_cadastrar_produto_cadastrar_conta:permissao_cadastrar_produto_cadastrar_conta.is(':checked'),
				permissao_visualizar_produto_cadastrar_conta:permissao_visualizar_produto_cadastrar_conta.is(':checked'),
				permissao_editar_produto_cadastrar_conta:permissao_editar_produto_cadastrar_conta.is(':checked'),
				permissao_excluir_produto_cadastrar_conta:permissao_excluir_produto_cadastrar_conta.is(':checked'),

				permissao_cadastrar_fornecedor_cadastrar_conta:permissao_cadastrar_fornecedor_cadastrar_conta.is(':checked'),
				permissao_visualizar_fornecedor_cadastrar_conta:permissao_visualizar_fornecedor_cadastrar_conta.is(':checked'),
				permissao_editar_fornecedor_cadastrar_conta:permissao_editar_fornecedor_cadastrar_conta.is(':checked'),
				permissao_excluir_fornecedor_cadastrar_conta:permissao_excluir_fornecedor_cadastrar_conta.is(':checked'),

				permissao_cadastrar_estoque_cadastrar_conta:permissao_cadastrar_estoque_cadastrar_conta.is(':checked'),
				permissao_visualizar_estoque_cadastrar_conta:permissao_visualizar_estoque_cadastrar_conta.is(':checked'),
				permissao_editar_estoque_cadastrar_conta:permissao_editar_estoque_cadastrar_conta.is(':checked'),
				permissao_excluir_estoque_cadastrar_conta:permissao_excluir_estoque_cadastrar_conta.is(':checked'),

				permissao_cadastrar_centro_custo_cadastrar_conta:permissao_cadastrar_centro_custo_cadastrar_conta.is(':checked'),
				permissao_visualizar_centro_custo_cadastrar_conta:permissao_visualizar_centro_custo_cadastrar_conta.is(':checked'),
				permissao_editar_centro_custo_cadastrar_conta:permissao_editar_centro_custo_cadastrar_conta.is(':checked'),
				permissao_excluir_centro_custo_cadastrar_conta:permissao_excluir_centro_custo_cadastrar_conta.is(':checked'),

				permissao_cadastrar_banco_cadastrar_conta:permissao_cadastrar_banco_cadastrar_conta.is(':checked'),
				permissao_visualizar_banco_cadastrar_conta:permissao_visualizar_banco_cadastrar_conta.is(':checked'),
				permissao_editar_banco_cadastrar_conta:permissao_editar_banco_cadastrar_conta.is(':checked'),
				permissao_excluir_banco_cadastrar_conta:permissao_excluir_banco_cadastrar_conta.is(':checked'),
			},

			beforeSend:function(){
                interface.iniciar_carregamento();
            },

            success:function(json){
                if(json !== undefined){
                    if(json.alerta !== undefined){
                        interface.exibirAlerta('Existe um aviso para você',json.alerta);
                        interface.finalizar_carregamento();
                        return false;
                    }

                    if(json.sucesso === 'true'){
                    	window.location.href = base_url + 'conta/listarContas'; 
                    }                    
                }
            }
		});

		return false;
	});

	
										// EDITAR
	// ----------------------------------------------------------------------------------------

	senha_editar_conta.blur(function(){
		if(validador.requerido(senha_editar_conta.val())){
			if(!validador.senha(senha_editar_conta.val())){
				alerta_senha_editar_conta.text('Formato inválido.');
			}else{
				alerta_senha_editar_conta.text('');
			}
		}
	});	

	$('#formulario_editar_conta').submit(function(){
		if(!validador.requerido(usuario_editar_conta.val())){
			alerta_usuario_editar_conta.text('Campo obrigatório.');
			usuario_editar_conta.focus();

			return false;
		}else{
			alerta_usuario_editar_conta.text('');
		}

		if(!validador.requerido(senha_editar_conta.val())){
			alerta_senha_editar_conta.text('Campo obrigatório.');
			senha_editar_conta.focus();

			return false;
		}else{
			if(!validador.senha(senha_editar_conta.val())){
				alerta_senha_editar_conta.text('Formato inválido.');

				return false;
			}else{
				alerta_senha_editar_conta.text('');
			}
		}

		$.ajax({
			url:$(this).attr('action'),
			type:'post',
			dataType:'json',
			data:{
				codigo_conta_editar_conta:$('#codigo_conta_editar_conta').val(),
				usuario_editar_conta:usuario_editar_conta.val(),
				senha_editar_conta:senha_editar_conta.val(),

				permissao_cadastrar_conta_editar_conta:permissao_cadastrar_conta_editar_conta.is(':checked'),
				permissao_visualizar_conta_editar_conta:permissao_visualizar_conta_editar_conta.is(':checked'),
				permissao_editar_conta_editar_conta:permissao_editar_conta_editar_conta.is(':checked'),
				permissao_excluir_conta_editar_conta:permissao_excluir_conta_editar_conta.is(':checked'),

				permissao_cadastrar_cliente_editar_conta:permissao_cadastrar_cliente_editar_conta.is(':checked'),
				permissao_visualizar_cliente_editar_conta:permissao_visualizar_cliente_editar_conta.is(':checked'),
				permissao_editar_cliente_editar_conta:permissao_editar_cliente_editar_conta.is(':checked'),
				permissao_excluir_cliente_editar_conta:permissao_excluir_cliente_editar_conta.is(':checked'),

				permissao_cadastrar_colaborador_editar_conta:permissao_cadastrar_colaborador_editar_conta.is(':checked'),
				permissao_visualizar_colaborador_editar_conta:permissao_visualizar_colaborador_editar_conta.is(':checked'),
				permissao_editar_colaborador_editar_conta:permissao_editar_colaborador_editar_conta.is(':checked'),
				permissao_excluir_colaborador_editar_conta:permissao_excluir_colaborador_editar_conta.is(':checked'),

				permissao_cadastrar_financeiro_editar_conta:permissao_cadastrar_financeiro_editar_conta.is(':checked'),
				permissao_visualizar_financeiro_editar_conta:permissao_visualizar_financeiro_editar_conta.is(':checked'),
				permissao_editar_financeiro_editar_conta:permissao_editar_financeiro_editar_conta.is(':checked'),
				permissao_excluir_financeiro_editar_conta:permissao_excluir_financeiro_editar_conta.is(':checked'),

				permissao_cadastrar_servico_editar_conta:permissao_cadastrar_servico_editar_conta.is(':checked'),
				permissao_visualizar_servico_editar_conta:permissao_visualizar_servico_editar_conta.is(':checked'),
				permissao_editar_servico_editar_conta:permissao_editar_servico_editar_conta.is(':checked'),
				permissao_excluir_servico_editar_conta:permissao_excluir_servico_editar_conta.is(':checked'),

				permissao_cadastrar_produto_editar_conta:permissao_cadastrar_produto_editar_conta.is(':checked'),
				permissao_visualizar_produto_editar_conta:permissao_visualizar_produto_editar_conta.is(':checked'),
				permissao_editar_produto_editar_conta:permissao_editar_produto_editar_conta.is(':checked'),
				permissao_excluir_produto_editar_conta:permissao_excluir_produto_editar_conta.is(':checked'),

				permissao_cadastrar_fornecedor_editar_conta:permissao_cadastrar_fornecedor_editar_conta.is(':checked'),
				permissao_visualizar_fornecedor_editar_conta:permissao_visualizar_fornecedor_editar_conta.is(':checked'),
				permissao_editar_fornecedor_editar_conta:permissao_editar_fornecedor_editar_conta.is(':checked'),
				permissao_excluir_fornecedor_editar_conta:permissao_excluir_fornecedor_editar_conta.is(':checked'),

				permissao_cadastrar_estoque_editar_conta:permissao_cadastrar_estoque_editar_conta.is(':checked'),
				permissao_visualizar_estoque_editar_conta:permissao_visualizar_estoque_editar_conta.is(':checked'),
				permissao_editar_estoque_editar_conta:permissao_editar_estoque_editar_conta.is(':checked'),
				permissao_excluir_estoque_editar_conta:permissao_excluir_estoque_editar_conta.is(':checked'),

				permissao_cadastrar_centro_custo_editar_conta:permissao_cadastrar_centro_custo_editar_conta.is(':checked'),
				permissao_visualizar_centro_custo_editar_conta:permissao_visualizar_centro_custo_editar_conta.is(':checked'),
				permissao_editar_centro_custo_editar_conta:permissao_editar_centro_custo_editar_conta.is(':checked'),
				permissao_excluir_centro_custo_editar_conta:permissao_excluir_centro_custo_editar_conta.is(':checked'),
				
				permissao_cadastrar_banco_editar_conta:permissao_cadastrar_banco_editar_conta.is(':checked'),
				permissao_visualizar_banco_editar_conta:permissao_visualizar_banco_editar_conta.is(':checked'),
				permissao_editar_banco_editar_conta:permissao_editar_banco_editar_conta.is(':checked'),
				permissao_excluir_banco_editar_conta:permissao_excluir_banco_editar_conta.is(':checked'),
			},

			beforeSend:function(){
                interface.iniciar_carregamento();
            },

            success:function(json){
                if(json !== undefined){
                    if(json.alerta !== undefined){
                        interface.exibirAlerta('Existe um aviso para você',json.alerta);
                        interface.finalizar_carregamento();
                        return false;
                    }

                    if(json.sucesso === 'true'){
                    	window.location.href = base_url + 'conta/listarContas'; 
                    }                    
                }
            }
		});

		return false;
	});

									// EXCLUSAO
	// -------------------------------------------------------------------------
	$('.deletar_conta').click(function(){
		var url = $(this).attr('href');

		interface.exibirAlertaConfirmacaoRedirecionamento(
			'Pedido de exclusão',
			'Você deseja mesmo excluir essa conta?',
			url,
			base_url + 'conta/listarContas'
		);

		return false;
	});

})();