<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
	function limpaCep($cep) {
	    $cep = str_replace(array('.','-',' '), array('','',''), $cep);
		return $cep;
	}
	
    function limpaCpf($cpf) {
    	$cpf = str_replace(array('-','.'), array('',''), $cpf);
		return $cpf;
    }

    function limpaCnpj($cnpj) {
    	$cnpj = str_replace(array('-','.','/'), array('','',''), $cnpj);
		return $cnpj;
    }

    function limpaTelefone($telefone) {
    	$telefone = str_replace(array('-','(',')',' '), array('','','',''), $telefone);
    	return $telefone;
    }

    function converteData($data) {
    	$data = strip_tags(implode(preg_match("~\/~", $data) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data) == 0 ? "-" : "/", $data))));
		return $data;
    }

    function formataMoeda($valor) {
    	$valor = str_replace(array('.',','), array('','.'), $valor);
		return $valor;
    }

    function exibeMoeda($valor) {
    	$valor = number_format($valor, 2, ",", "." );
		return $valor;
    }

	