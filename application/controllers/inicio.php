<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends MY_Controller {
	public function __construct(){
		parent::__construct();

		if(!$this->validarSessao()){
			header("Location:" . base_url() . 'obra');
		}
	}

	public function index(){
		$this->smarty_ci->display('inicio.tpl');
	}
}