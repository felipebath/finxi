<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends MY_Controller {
	
	public function index(){
		$this->smarty_ci->display('entrar-empresa.tpl');
	}

	public function cadastrar(){
		$this->smarty_ci->display('cadastrar-empresa.tpl');
	}

	public function testeCadastrar(){
		$this->load->model('empresa_model','empresa_model');
		$this->empresa_model->setNome('Nome da empresa de teste');
		$this->empresa_model->setStatus('1');
		$this->empresa_model->setPermissaoCliente('1');
		$this->empresa_model->setPermissaoColaborador('0');
		$this->empresa_model->setPermissaoConta('1');
		$this->empresa_model->setPermissaoEstoque('1');
		$this->empresa_model->setPermissaoFinanceiro('1');
		$this->empresa_model->setPermissaoFornecedor('1');
		$this->empresa_model->setPermissaoProduto('1');
		$this->empresa_model->setPermissaoServico('1');
		
		try{
			$retorno = $this->empresa_model->getCadastrar();

			if($retorno){
				print("Teste - empresaCadastrar - Sucesso <br>");
			}else{
				print("Teste - empresaCadastrar - Falhou <br>");
			}
		}catch(Exception $e){
			print($e->getMessage());
		}
	}

	public function testeEditar(){
		$this->load->model('empresa_model','empresa_model');
		$this->empresa_model->setCodigo('5');
		$this->empresa_model->setNome('Edit Nome');
		$this->empresa_model->setStatus('0');
		$this->empresa_model->setPermissaoCliente('0');
		$this->empresa_model->setPermissaoColaborador('0');
		$this->empresa_model->setPermissaoConta('0');
		$this->empresa_model->setPermissaoEstoque('1');
		$this->empresa_model->setPermissaoFinanceiro('1');
		$this->empresa_model->setPermissaoFornecedor('1');
		$this->empresa_model->setPermissaoProduto('1');
		$this->empresa_model->setPermissaoServico('1');
		
		try{
			$retorno = $this->empresa_model->getEditar();

			if($retorno){
				print("Teste - empresaEditar - Sucesso <br>");
			}else{
				print("Teste - empresaEditar - Falhou <br>");
			}
		}catch(Exception $e){
			print($e->getMessage());
		}
	}

	public function testeAtivar(){
		$this->load->model('empresa_model','empresa_model');
		
		$this->empresa_model->setCodigo('5');
		$this->empresa_model->setStatus('1');
		
		try{
			$retorno = $this->empresa_model->getAtivar();

			if($retorno){
				print("Teste - empresaAtivar - Sucesso <br>");
			}else{
				print("Teste - empresaAtivar - Falhou <br>");
			}
		}catch(Exception $e){
			print($e->getMessage());
		}
	}

	public function testeDesativar(){
		$this->load->model('empresa_model','empresa_model');
		
		$this->empresa_model->setCodigo('5');
		$this->empresa_model->setStatus('0');
		
		try{
			$retorno = $this->empresa_model->getDesativar();

			if($retorno){
				print("Teste - empresaDesativar - Sucesso <br>");
			}else{
				print("Teste - empresaDesativar - Falhou <br>");
			}
		}catch(Exception $e){
			print($e->getMessage());
		}
	}

} // fecha class conta