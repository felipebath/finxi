<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imovel extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('imovel_model','imovel');
	}

	public function index($pagina){
		$pagina = (int)$pagina;

		if($pagina <= 0 || $pagina === 1){
			$pagina_posterior = 0;
		}else{
			$pagina_posterior = $pagina;
		}

		$this->db->limit(10,$pagina_posterior);		
		$this->db->order_by('nome ASC');
		$sql_listar = $this->db->get('imovel');

		if ($sql_listar->num_rows() !== 0) {
			foreach ($sql_listar->result_array() as $dados) {
				$id[] 			= $dados['id_imovel'];
				$nome[] 		= $dados['nome'];
				$descricao[] 	= $dados['descricao'];
				$endereco[] 	= $dados['endereco'];
				$cep[] 			= $dados['cep'];
				$imagem[] 		= $dados['imagem'];
				$valor[] 		= exibeMoeda($dados['valor']);
			}
		} else {
				$id 			= 0;
				$nome 			= 0;
				$descricao 		= 0;
				$endereco 		= 0;
				$cep 		 	= 0;
				$imagem 		= 0;
				$valor 			= 0;
		}

		$sql_numero_registros = $this->db->count_all('imovel');
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'imovel/index/';
		$config['total_rows'] = $sql_numero_registros;
		$config['per_page'] = 10; 
		$config['first_link'] = 'Primeira';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$this->pagination->initialize($config); 
		$links_paginacao = $this->pagination->create_links();
		$this->smarty_ci->assign('numero_registros',$sql_numero_registros);
		$this->smarty_ci->assign('pagina',$pagina);
		$this->smarty_ci->assign('links_paginacao',$links_paginacao);
		$this->smarty_ci->assign('id',$id);
		$this->smarty_ci->assign('nome',$nome);
		$this->smarty_ci->assign('descricao',$descricao);
		$this->smarty_ci->assign('endereco',$endereco);
		$this->smarty_ci->assign('cep',$cep);
		$this->smarty_ci->assign('imagem',$imagem);
		$this->smarty_ci->assign('valor',$valor);
		$this->smarty_ci->display('listar-imovel.tpl');
	}

	public function cadastrar(){
		$this->smarty_ci->display('cadastrar-imovel.tpl');
	}

	public function fazerCadastro(){
		$this->form_validation->set_rules('nome','Nome','required|max_length[50]');
		
		if (!$this->form_validation->run()) {
			$this->form_validation->set_error_delimiters("", "");
			$this->json_collection->assign('alerta',form_error('nome'));
			$this->json_collection->display();
			exit;
		} else {
			$dados = elements(
					array(
						'nome',
						'descricao',
						'endereco',
						'cep',
						'imagem',
						'valor'
					),$this->input->post()
				);
			
			// extrai as coordenadas do endereço
			$endereco_extract = $dados['endereco'];
			$endereco_extract    = str_replace(',', ' ', $endereco_extract);
			$endereco_extract    = str_replace(' ', '+', $endereco_extract);
			$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$endereco_extract.'&sensor=false');
			$output	= json_decode($geocode);
			$latitude	= $output->results[0]->geometry->location->lat;
			$longitude 	= $output->results[0]->geometry->location->lng;

			$this->imovel->setCodigoEmpresa  	($this->session->userdata('codigo_empresa'));
			$this->imovel->setNome   			($dados['nome']);
			$this->imovel->setDescricao  		($dados['descricao']);
			$this->imovel->setEndereco  		($dados['endereco']);
			$this->imovel->setCep 				($dados['cep']);
			$this->imovel->setImagem  			($dados['imagem']);
			$this->imovel->setValor 			(formataMoeda($dados['valor']));
			$this->imovel->setLatitude 			($latitude);
			$this->imovel->setLongitude			($longitude);

			try{
				
				$sql = $this->imovel->getCadastrar();
				
				if ($sql) {
					$this->json_collection->assign('sucesso','true');
					$this->json_collection->display();
					exit;
				} else {
					$this->json_collection->assign('alerta','Ocorreu uma falha no cadastro');
					$this->json_collection->display();
				}

			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível realizar a operação.'.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}		
	}

	public function editar($id){
		$id = (int) $id;

		$this->db->where('id_imovel',$id);
		$sql_listar = $this->db->get('imovel');

		if ($sql_listar->num_rows() !== 0) {
			foreach ($sql_listar->result_array() as $dados) {
				$id 		= $dados['id_imovel'];
				$nome 		= $dados['nome'];
				$descricao 	= $dados['descricao'];
				$endereco 	= $dados['endereco'];
				$cep 		= $dados['cep'];
				$imagem 	= $dados['imagem'];
				$valor 		= exibeMoeda($dados['valor']);
			}
		} else {
				$id 			= 0;
				$nome 			= 0;
				$descricao 		= 0;
				$endereco 		= 0;
				$cep 		 	= 0;
				$imagem 		= 0;
				$valor 			= 0;
		}

		$this->smarty_ci->assign('id',$id);
		$this->smarty_ci->assign('nome',$nome);
		$this->smarty_ci->assign('descricao',$descricao);
		$this->smarty_ci->assign('endereco',$endereco);
		$this->smarty_ci->assign('cep',$cep);
		$this->smarty_ci->assign('imagem',$imagem);
		$this->smarty_ci->assign('valor',$valor);
		$this->smarty_ci->display('editar-imovel.tpl');
	}

	public function fazerEdicao(){
		$this->form_validation->set_rules('nome','Nome','required|max_length[50]');
		
		if (!$this->form_validation->run()) {
			$this->form_validation->set_error_delimiters("", "");
			$this->json_collection->assign('alerta',form_error('nome'));
			$this->json_collection->display();
			exit;
		} else {
			$dados = elements(
					array(
						'id',
						'nome',
						'descricao',
						'endereco',
						'cep',
						'imagem',
						'valor'
					),$this->input->post()
				);
			
			// extrai as coordenadas do endereço
			$endereco_extract = $dados['endereco'];
			$endereco_extract    = str_replace(',', ' ', $endereco_extract);
			$endereco_extract    = str_replace(' ', '+', $endereco_extract);
			$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$endereco_extract.'&sensor=false');
			$output	= json_decode($geocode);
			$latitude	= $output->results[0]->geometry->location->lat;
			$longitude 	= $output->results[0]->geometry->location->lng;

			$this->imovel->setId             	($dados['id']);
			$this->imovel->setCodigoEmpresa  	($this->session->userdata('codigo_empresa'));
			$this->imovel->setNome   			($dados['nome']);
			$this->imovel->setDescricao  		($dados['descricao']);
			$this->imovel->setEndereco  		($dados['endereco']);
			$this->imovel->setCep 				($dados['cep']);
			$this->imovel->setImagem  			($dados['imagem']);
			$this->imovel->setValor 			(formataMoeda($dados['valor']));
			$this->imovel->setLatitude 			($latitude);
			$this->imovel->setLongitude			($longitude);

			try{
				$sql = $this->imovel->getEditar();
				if ($sql) {
					$this->json_collection->assign('sucesso','true');
					$this->json_collection->display();
					exit;
				} else {
					$this->json_collection->assign('alerta','Ocorreu uma falha na edição');
					$this->json_collection->display();
				}		
			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível realizar a operação.'.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}		
	}
	
	public function inativar($id){
		$id = (int) $id;
		$this->imovel->setId($id);
		
		try{
			$entrada = $this->imovel->getInativar();
			if ($entrada) {
				header("Location:" . base_url() . 'imovel');
			} else {
				$this->json_collection->assign('alerta',form_error('Ocorreu uma falha ao inativar'));
				$this->json_collection->display();
			}			
		}catch(Exception $e){
			$this->json_collection->assign('alerta','Não foi possível realizar a operação.'.$e->getMessage());
			$this->json_collection->display();
			exit;
		}
	}

	public function filtrar(){
		
		$valor_filtro 	= strip_tags($_GET['valor_filtro']);
		$raio_filtro 	= strip_tags($_GET['raio_filtro']) ?: '5';
		
		// extrai as coordenadas do endereço
		$endereco_extract 	 = strip_tags($_GET['valor_filtro']);
		$endereco_extract    = str_replace(',', ' ', $endereco_extract);
		$endereco_extract    = str_replace(' ', '+', $endereco_extract);
		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$endereco_extract.'&sensor=false');
		$output	= json_decode($geocode);
		$latitude	= $output->results[0]->geometry->location->lat;
		$longitude 	= $output->results[0]->geometry->location->lng;
		
		if ( !empty($valor_filtro) ) {

			$sql = "SELECT 
						Geo('$latitude','$longitude',latitude, longitude) AS distancia, 
						id_imovel,
						nome,
						descricao,
						endereco,
						cep,
						imagem,
						valor
					FROM 
						imovel
					HAVING
						distancia <= '$raio_filtro'	
					";
			
			$result = mysql_query($sql);
	  		
			while($consulta = mysql_fetch_array($result)) { 
			  
			  	$id[] 			= $consulta[1];
			  	$nome[]     	= $consulta[2];
			  	$descricao[]  	= $consulta[3];
			  	$endereco[]   	= $consulta[4];
			  	$cep[]     		= $consulta[5];
			  	$imagem[]    	= $consulta[6];
			  	$valor[]     	= $consulta[7];
			}
			
				$this->smarty_ci->assign('valor_filtro',$valor_filtro);
				$this->smarty_ci->assign('raio_filtro',$raio_filtro);
				$this->smarty_ci->assign('id',$id);
				$this->smarty_ci->assign('nome',$nome);
				$this->smarty_ci->assign('descricao',$descricao);
				$this->smarty_ci->assign('endereco',$endereco);
				$this->smarty_ci->assign('cep',$cep);
				$this->smarty_ci->assign('imagem',$imagem);
				$this->smarty_ci->assign('valor',$valor);
				$this->smarty_ci->display('resultado-filtro-imovel.tpl');
		}
	}

}