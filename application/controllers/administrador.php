<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrador extends MY_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model('conta_model','conta_model');

		$this->conta_model->setUsuario($this->session->userdata('usuario_administrador'));
		$retorno_conta = $this->conta_model->getValidarSessao();

		if(!$retorno_conta){
			header("Location:".base_url());
		}
	}

	public function index(){
		$this->db->select('
			codigo_empresa,
			nome_empresa,
			status_empresa
		');

		$this->db->from('empresa');
		$this->db->order_by('nome_empresa','ASC');
		$this->db->where('status_empresa','1');
		$sql_empresa = $this->db->get();
		
		if($sql_empresa->num_rows() !== 0){
			foreach($sql_empresa->result_array() as $dados_empresa){
				$codigo_empresa[] = $dados_empresa['codigo_empresa'];
				$nome_empresa[] = $dados_empresa['nome_empresa'];
			}
		}else{
			$codigo_empresa = 0;
			$nome_empresa = 0;
		}

		$this->smarty_ci->assign('codigo_empresa',$codigo_empresa);
		$this->smarty_ci->assign('nome_empresa',$nome_empresa);
		$this->smarty_ci->display('listar-administradores.tpl');
	}

	public function cadastrar(){
		$this->smarty_ci->display('cadastrar-administrador.tpl');
	}

	public function fazerCadastro(){
		$this->load->model('conta_model','conta_model');
		$this->load->model('empresa_model','empresa_model');

		$this->form_validation->set_message('is_unique','O campo %s ja foi cadastrado.');

		$this->form_validation->set_rules('nome_empresa_cadastrar_administrador','nome da empresa','max_length[80]|required|is_unique[empresa.nome_empresa]');
		$this->form_validation->set_rules('usuario_cadastrar_administrador','usuário','max_length[20]|min_length[6]|required|alpha_dash|is_unique[conta.usuario_conta]');
		$this->form_validation->set_rules('senha_cadastrar_administrador','senha','senha|alpha_dash|required');

		if(!$this->form_validation->run()){
			$this->form_validation->set_error_delimiters("", "");
			
			$this->json_collection->assign('alerta',form_error('nome_empresa_cadastrar_administrador'));
			$this->json_collection->assign('alerta',form_error('usuario_cadastrar_administrador'));
			$this->json_collection->assign('alerta',form_error('senha_cadastrar_administrador'));
			$this->json_collection->display();

			exit;
		}else{
			$dados = elements(
				array(
					'nome_empresa_cadastrar_administrador',
					'usuario_cadastrar_administrador',
					'senha_cadastrar_administrador',
					'permissao_conta_cadastrar_administrador',
					'permissao_cliente_cadastrar_administrador',
					'permissao_colaborador_cadastrar_administrador',
					'permissao_fornecedor_cadastrar_administrador',
					'permissao_estoque_cadastrar_administrador',
					'permissao_servico_cadastrar_administrador',
					'permissao_financeiro_cadastrar_administrador',
					'permissao_produto_cadastrar_administrador',
					'permissao_banco_cadastrar_administrador',
					'permissao_centro_custo_cadastrar_administrador'
				), $this->input->post()
			);

			$permissao_conta_cadastrar_administrador = $dados['permissao_conta_cadastrar_administrador'];
			$permissao_cliente_cadastrar_administrador = $dados['permissao_cliente_cadastrar_administrador'];
			$permissao_colaborador_cadastrar_administrador = $dados['permissao_colaborador_cadastrar_administrador'];
			$permissao_fornecedor_cadastrar_administrador = $dados['permissao_fornecedor_cadastrar_administrador'];
			$permissao_estoque_cadastrar_administrador = $dados['permissao_estoque_cadastrar_administrador'];
			$permissao_servico_cadastrar_administrador = $dados['permissao_servico_cadastrar_administrador'];
			$permissao_financeiro_cadastrar_administrador = $dados['permissao_financeiro_cadastrar_administrador'];
			$permissao_produto_cadastrar_administrador = $dados['permissao_produto_cadastrar_administrador'];
			$permissao_banco_cadastrar_administrador = $dados['permissao_banco_cadastrar_administrador'];
			$permissao_centro_custo_cadastrar_administrador = $dados['permissao_centro_custo_cadastrar_administrador'];


			$permissao_conta_cadastrar_administrador = str_replace('true','1',$permissao_conta_cadastrar_administrador);
			$permissao_conta_cadastrar_administrador = str_replace('false','0',$permissao_conta_cadastrar_administrador);

			$permissao_cliente_cadastrar_administrador = str_replace('true','1',$permissao_cliente_cadastrar_administrador);
			$permissao_cliente_cadastrar_administrador = str_replace('false','0',$permissao_cliente_cadastrar_administrador);

			$permissao_colaborador_cadastrar_administrador = str_replace('true','1',$permissao_colaborador_cadastrar_administrador);
			$permissao_colaborador_cadastrar_administrador = str_replace('false','0',$permissao_colaborador_cadastrar_administrador);

			$permissao_fornecedor_cadastrar_administrador = str_replace('true','1',$permissao_fornecedor_cadastrar_administrador);
			$permissao_fornecedor_cadastrar_administrador = str_replace('false','0',$permissao_fornecedor_cadastrar_administrador);

			$permissao_estoque_cadastrar_administrador = str_replace('true','1',$permissao_estoque_cadastrar_administrador);
			$permissao_estoque_cadastrar_administrador = str_replace('false','0',$permissao_estoque_cadastrar_administrador);

			$permissao_servico_cadastrar_administrador = str_replace('true','1',$permissao_servico_cadastrar_administrador);
			$permissao_servico_cadastrar_administrador = str_replace('false','0',$permissao_servico_cadastrar_administrador);

			$permissao_financeiro_cadastrar_administrador = str_replace('true','1',$permissao_financeiro_cadastrar_administrador);
			$permissao_financeiro_cadastrar_administrador = str_replace('false','0',$permissao_financeiro_cadastrar_administrador);

			$permissao_produto_cadastrar_administrador = str_replace('true','1',$permissao_produto_cadastrar_administrador);
			$permissao_produto_cadastrar_administrador = str_replace('false','0',$permissao_produto_cadastrar_administrador);

			$permissao_banco_cadastrar_administrador = str_replace('true','1',$permissao_banco_cadastrar_administrador);
			$permissao_banco_cadastrar_administrador = str_replace('false','0',$permissao_banco_cadastrar_administrador);

			$permissao_centro_custo_cadastrar_administrador = str_replace('true','1',$permissao_centro_custo_cadastrar_administrador);
			$permissao_centro_custo_cadastrar_administrador = str_replace('false','0',$permissao_centro_custo_cadastrar_administrador);

			if($permissao_conta_cadastrar_administrador === '0' && $permissao_cliente_cadastrar_administrador === '0' && $permissao_colaborador_cadastrar_administrador === '0' && $permissao_fornecedor_cadastrar_administrador === '0' && $permissao_estoque_cadastrar_administrador === '0' && $permissao_servico_cadastrar_administrador === '0' && $permissao_financeiro_cadastrar_administrador === '0' && $permissao_produto_cadastrar_administrador === '0' && $permissao_banco_cadastrar_administrador === '0' && $permissao_centro_custo_administrador === '0'){
				$this->json_collection->assign('alerta','Selecione pelo menos uma permissão');
				$this->json_collection->display();
				
				exit;
			}

			$this->empresa_model->setNome($this->db->escape_str($dados['nome_empresa_cadastrar_administrador']));
			$this->empresa_model->setPermissaoCliente($permissao_cliente_cadastrar_administrador);
			$this->empresa_model->setPermissaoColaborador($permissao_colaborador_cadastrar_administrador);
			$this->empresa_model->setPermissaoConta($permissao_conta_cadastrar_administrador);
			$this->empresa_model->setPermissaoEstoque($permissao_estoque_cadastrar_administrador);
			$this->empresa_model->setPermissaoFinanceiro($permissao_financeiro_cadastrar_administrador);
			$this->empresa_model->setPermissaoFornecedor($permissao_fornecedor_cadastrar_administrador);
			$this->empresa_model->setPermissaoProduto($permissao_produto_cadastrar_administrador);
			$this->empresa_model->setPermissaoServico($permissao_servico_cadastrar_administrador);
			$this->empresa_model->setPermissaoCentroCusto($permissao_centro_custo_cadastrar_administrador);
			$this->empresa_model->setPermissaoBanco($permissao_banco_cadastrar_administrador);

			try{
				$sql_empresa = $this->empresa_model->getCadastrar();

				if($sql_empresa){
					$codigo_empresa = $this->db->insert_id();

					$usuario = $dados['usuario_cadastrar_administrador'];
					$senha = $dados['senha_cadastrar_administrador'];

					$usuario = strtolower($usuario);
					$senha = strtolower($senha);

					$senha = $this->crypt->getEncrypt($senha,$this->config->config['encryption_key']);
					$senha = base64_encode($senha);

					$this->conta_model->setCodigoEmpresa($codigo_empresa);
					$this->conta_model->setPermissaoAdministrador('1');
					$this->conta_model->setUsuario($usuario);
					$this->conta_model->setSenha($senha);

					if($permissao_cliente_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarCliente('1');
						$this->conta_model->setPermissaoVisualizarCliente('1');
						$this->conta_model->setPermissaoEditarCliente('1');
						$this->conta_model->setPermissaoExcluirCliente('1');
					}else{
						$this->conta_model->setPermissaoCadastrarCliente('1');
						$this->conta_model->setPermissaoVisualizarCliente('1');
						$this->conta_model->setPermissaoEditarCliente('1');
						$this->conta_model->setPermissaoExcluirCliente('1');
					}

					if($permissao_colaborador_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarColaborador('1');
						$this->conta_model->setPermissaoVisualizarColaborador('1');
						$this->conta_model->setPermissaoEditarColaborador('1');
						$this->conta_model->setPermissaoExcluirColaborador('1');
					}else{
						$this->conta_model->setPermissaoCadastrarColaborador('0');
						$this->conta_model->setPermissaoVisualizarColaborador('0');
						$this->conta_model->setPermissaoEditarColaborador('0');
						$this->conta_model->setPermissaoExcluirColaborador('0');
					}

					if($permissao_conta_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarConta('1');
						$this->conta_model->setPermissaoVisualizarConta('1');
						$this->conta_model->setPermissaoEditarConta('1');
						$this->conta_model->setPermissaoExcluirConta('1');
					}else{
						$this->conta_model->setPermissaoCadastrarConta('0');
						$this->conta_model->setPermissaoVisualizarConta('0');
						$this->conta_model->setPermissaoEditarConta('0');
						$this->conta_model->setPermissaoExcluirConta('0');
					}

					if($permissao_estoque_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarEstoque('1');
						$this->conta_model->setPermissaoVisualizarEstoque('1');
						$this->conta_model->setPermissaoEditarEstoque('1');
						$this->conta_model->setPermissaoExcluirEstoque('1');	
					}else{
						$this->conta_model->setPermissaoCadastrarEstoque('0');
						$this->conta_model->setPermissaoVisualizarEstoque('0');
						$this->conta_model->setPermissaoEditarEstoque('0');
						$this->conta_model->setPermissaoExcluirEstoque('0');
					}

					if($permissao_financeiro_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarFinanceiro('1');
						$this->conta_model->setPermissaoVisualizarFinanceiro('1');
						$this->conta_model->setPermissaoEditarFinanceiro('1');
						$this->conta_model->setPermissaoExcluirFinanceiro('1');	
					}else{
						$this->conta_model->setPermissaoCadastrarFinanceiro('0');
						$this->conta_model->setPermissaoVisualizarFinanceiro('0');
						$this->conta_model->setPermissaoEditarFinanceiro('0');
						$this->conta_model->setPermissaoExcluirFinanceiro('0');
					}

					if($permissao_fornecedor_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarFornecedor('1');
						$this->conta_model->setPermissaoVisualizarFornecedor('1');
						$this->conta_model->setPermissaoEditarFornecedor('1');
						$this->conta_model->setPermissaoExcluirFornecedor('1');
					}else{
						$this->conta_model->setPermissaoCadastrarFornecedor('0');
						$this->conta_model->setPermissaoVisualizarFornecedor('0');
						$this->conta_model->setPermissaoEditarFornecedor('0');
						$this->conta_model->setPermissaoExcluirFornecedor('0');
					}

					if($permissao_produto_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarProduto('1');
						$this->conta_model->setPermissaoVisualizarProduto('1');
						$this->conta_model->setPermissaoEditarProduto('1');
						$this->conta_model->setPermissaoExcluirProduto('1');	
					}else{
						$this->conta_model->setPermissaoCadastrarProduto('0');
						$this->conta_model->setPermissaoVisualizarProduto('0');
						$this->conta_model->setPermissaoEditarProduto('0');
						$this->conta_model->setPermissaoExcluirProduto('0');
					}

					if($permissao_servico_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarServico('1');
						$this->conta_model->setPermissaoVisualizarServico('1');
						$this->conta_model->setPermissaoEditarServico('1');
						$this->conta_model->setPermissaoExcluirServico('1');
					}else{
						$this->conta_model->setPermissaoCadastrarServico('0');
						$this->conta_model->setPermissaoVisualizarServico('0');
						$this->conta_model->setPermissaoEditarServico('0');
						$this->conta_model->setPermissaoExcluirServico('0');
					}

					if($permissao_banco_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarBanco('1');
						$this->conta_model->setPermissaoVisualizarBanco('1');
						$this->conta_model->setPermissaoEditarBanco('1');
						$this->conta_model->setPermissaoExcluirBanco('1');
					}else{
						$this->conta_model->setPermissaoCadastrarBanco('0');
						$this->conta_model->setPermissaoVisualizarBanco('0');
						$this->conta_model->setPermissaoEditarBanco('0');
						$this->conta_model->setPermissaoExcluirBanco('0');
					}

					if($permissao_centro_custo_cadastrar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarCentroCusto('1');
						$this->conta_model->setPermissaoVisualizarCentroCusto('1');
						$this->conta_model->setPermissaoEditarCentroCusto('1');
						$this->conta_model->setPermissaoExcluirCentroCusto('1');
					}else{
						$this->conta_model->setPermissaoCadastrarCentroCusto('0');
						$this->conta_model->setPermissaoVisualizarCentroCusto('0');
						$this->conta_model->setPermissaoEditarCentroCusto('0');
						$this->conta_model->setPermissaoExcluirCentroCusto('0');
					}
					
					try{
						$sql_conta = $this->conta_model->getCadastrar();

						if($sql_conta){
							$this->json_collection->assign('sucesso','true');
							$this->json_collection->display();
							exit;
						}else{
							$this->empresa_model->setCodigo($codigo_empresa);
							$this->empresa_model->getDesativar();

							$this->json_collection->assign('alerta','Não foi possível cadastrar essa conta no sistema, por favor tente novamente.');
							$this->json_collection->display();
							exit;
						}
					}catch(Exception $e){
						$this->json_collection->assign('alerta','Não foi possível cadastrar essa conta no sistema, por favor tente novamente. Copie a informação a seguir e entre em contato com o desenvolvedor responsável: '.$e->getMessage());
						$this->json_collection->display();
						exit;
					}
				}else{
					$this->json_collection->assign('alerta','Não foi possível cadastrar esse administrador, por favor verifique seus dados e tente novamente.');
					$this->json_collection->display();
					exit;
				}
			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível cadastrar essa empresa no sistema, por favor tente novamente. Copie a informação a seguir e entre em contato com o desenvolvedor responsável: '.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}
	}

	public function excluir($codigo_empresa){
		$this->load->model('empresa_model','empresa_model');
		$this->load->model('conta_model','conta_model');

		$this->db->select('
			codigo_conta,
			codigo_empresa_conta
		');

		$this->db->from('conta');
		$sql_consulta_conta = $this->db->get();

		if($sql_consulta_conta->num_rows === 1){
			foreach($sql_consulta_conta->result_array() as $dados_conta){
				$codigo_conta = $dados_conta['codigo_conta'];
			}
		}

		$this->conta_model->setCodigo($codigo_conta);
		$this->conta_model->getDesativar();

		$this->empresa_model->setCodigo($codigo_empresa);
		$this->empresa_model->getDesativar();
		
		header("Location:" . base_url() . 'administrador');
	}

	public function editar($codigo_empresa){
		
		$this->db->select('
			codigo_empresa,
			nome_empresa,
			status_empresa,

			permissao_cliente_empresa,
			permissao_colaborador_empresa,
			permissao_conta_empresa,
			permissao_estoque_empresa,
			permissao_financeiro_empresa,
			permissao_fornecedor_empresa,
			permissao_produto_empresa,
			permissao_servico_empresa,
			permissao_banco_empresa,
			permissao_centro_custo_empresa,

			codigo_conta,
			codigo_empresa_conta,
			status_conta,
			usuario_conta,
			senha_conta,
			permissao_administrador_conta,

			permissao_cadastrar_cliente_conta,
			permissao_visualizar_cliente_conta,
			permissao_editar_cliente_conta,
			permissao_excluir_cliente_conta,

			permissao_cadastrar_colaborador_conta,
			permissao_visualizar_colaborador_conta,
			permissao_editar_colaborador_conta,
			permissao_excluir_colaborador_conta,

			permissao_cadastrar_conta_conta,
			permissao_visualizar_conta_conta,
			permissao_editar_conta_conta,
			permissao_excluir_conta_conta,

			permissao_cadastrar_estoque_conta,
			permissao_visualizar_estoque_conta,
			permissao_editar_estoque_conta,
			permissao_excluir_estoque_conta,

			permissao_cadastrar_financeiro_conta,
			permissao_visualizar_financeiro_conta,
			permissao_editar_financeiro_conta,
			permissao_excluir_financeiro_conta,

			permissao_cadastrar_fornecedor_conta,
			permissao_visualizar_fornecedor_conta,
			permissao_editar_fornecedor_conta,
			permissao_excluir_fornecedor_conta,

			permissao_cadastrar_produto_conta,
			permissao_visualizar_produto_conta,
			permissao_editar_produto_conta,
			permissao_excluir_produto_conta,

			permissao_cadastrar_produto_conta,
			permissao_visualizar_produto_conta,
			permissao_editar_produto_conta,
			permissao_excluir_produto_conta,

			permissao_cadastrar_servico_conta,
			permissao_visualizar_servico_conta,
			permissao_editar_servico_conta,
			permissao_excluir_servico_conta,

			permissao_cadastrar_centro_custo_conta,
			permissao_visualizar_centro_custo_conta,
			permissao_editar_centro_custo_conta,
			permissao_excluir_centro_custo_conta,

			permissao_cadastrar_banco_conta,
			permissao_visualizar_banco_conta,
			permissao_editar_banco_conta,
			permissao_excluir_banco_conta
		');
		
		$this->db->from('empresa');
		$this->db->join('conta','conta.codigo_empresa_conta = empresa.codigo_empresa');
		$this->db->where('empresa.status_empresa','1');
		$this->db->where('conta.status_conta','1');
		$this->db->where('conta.permissao_administrador_conta','1');
		$this->db->where('empresa.codigo_empresa',(int)$codigo_empresa);
		$sql_tabelas = $this->db->get();

		if($sql_tabelas->num_rows !== 0){
			foreach($sql_tabelas->result_array() as $dados_tabelas){
				$nome_empresa = $dados_tabelas['nome_empresa'];

				$permissao_cliente_empresa = $dados_tabelas['permissao_cliente_empresa'];
				$permissao_colaborador_empresa = $dados_tabelas['permissao_colaborador_empresa'];
				$permissao_conta_empresa = $dados_tabelas['permissao_conta_empresa'];
				$permissao_estoque_empresa = $dados_tabelas['permissao_estoque_empresa'];
				$permissao_financeiro_empresa = $dados_tabelas['permissao_financeiro_empresa'];
				$permissao_fornecedor_empresa = $dados_tabelas['permissao_fornecedor_empresa'];
				$permissao_produto_empresa = $dados_tabelas['permissao_produto_empresa'];
				$permissao_servico_empresa = $dados_tabelas['permissao_servico_empresa'];
				$permissao_banco_empresa = $dados_tabelas['permissao_banco_empresa'];
				$permissao_centro_custo_empresa = $dados_tabelas['permissao_centro_custo_empresa'];

				$codigo_conta = $dados_tabelas['codigo_conta'];
				$usuario_conta = $dados_tabelas['usuario_conta'];
				$senha_conta = $dados_tabelas['senha_conta'];

				$permissao_cadastrar_cliente_conta = $dados_tabelas['permissao_cadastrar_cliente_conta'];
				$permissao_visualizar_cliente_conta = $dados_tabelas['permissao_visualizar_cliente_conta'];
				$permissao_editar_cliente_conta = $dados_tabelas['permissao_editar_cliente_conta'];
				$permissao_excluir_cliente_conta = $dados_tabelas['permissao_excluir_cliente_conta'];

				$permissao_cadastrar_colaborador_conta = $dados_tabelas['permissao_cadastrar_colaborador_conta'];
				$permissao_visualizar_colaborador_conta = $dados_tabelas['permissao_visualizar_colaborador_conta'];
				$permissao_editar_colaborador_conta = $dados_tabelas['permissao_editar_colaborador_conta'];
				$permissao_excluir_colaborador_conta = $dados_tabelas['permissao_excluir_colaborador_conta'];

				$permissao_cadastrar_conta_conta = $dados_tabelas['permissao_cadastrar_conta_conta'];
				$permissao_visualizar_conta_conta = $dados_tabelas['permissao_visualizar_conta_conta'];
				$permissao_editar_conta_conta = $dados_tabelas['permissao_editar_conta_conta'];
				$permissao_excluir_conta_conta = $dados_tabelas['permissao_excluir_conta_conta'];

				$permissao_cadastrar_estoque_conta = $dados_tabelas['permissao_cadastrar_estoque_conta'];
				$permissao_visualizar_estoque_conta = $dados_tabelas['permissao_visualizar_estoque_conta'];
				$permissao_editar_estoque_conta = $dados_tabelas['permissao_editar_estoque_conta'];
				$permissao_excluir_estoque_conta = $dados_tabelas['permissao_excluir_estoque_conta'];

				$permissao_cadastrar_financeiro_conta = $dados_tabelas['permissao_cadastrar_financeiro_conta'];
				$permissao_visualizar_financeiro_conta = $dados_tabelas['permissao_visualizar_financeiro_conta'];
				$permissao_editar_financeiro_conta = $dados_tabelas['permissao_editar_financeiro_conta'];
				$permissao_excluir_financeiro_conta = $dados_tabelas['permissao_excluir_financeiro_conta'];

				$permissao_cadastrar_fornecedor_conta = $dados_tabelas['permissao_cadastrar_fornecedor_conta'];
				$permissao_visualizar_fornecedor_conta = $dados_tabelas['permissao_visualizar_fornecedor_conta'];
				$permissao_editar_fornecedor_conta = $dados_tabelas['permissao_editar_fornecedor_conta'];
				$permissao_excluir_fornecedor_conta = $dados_tabelas['permissao_excluir_fornecedor_conta'];

				$permissao_cadastrar_produto_conta = $dados_tabelas['permissao_cadastrar_produto_conta'];
				$permissao_visualizar_produto_conta = $dados_tabelas['permissao_visualizar_produto_conta'];
				$permissao_editar_produto_conta = $dados_tabelas['permissao_editar_produto_conta'];
				$permissao_excluir_produto_conta = $dados_tabelas['permissao_excluir_produto_conta'];

				$permissao_cadastrar_servico_conta = $dados_tabelas['permissao_cadastrar_servico_conta'];
				$permissao_visualizar_servico_conta = $dados_tabelas['permissao_visualizar_servico_conta'];
				$permissao_editar_servico_conta = $dados_tabelas['permissao_editar_servico_conta'];
				$permissao_excluir_servico_conta = $dados_tabelas['permissao_excluir_servico_conta'];

				$permissao_cadastrar_centro_custo_conta = $dados_tabelas['permissao_cadastrar_centro_custo_conta'];
				$permissao_visualizar_centro_custo_conta = $dados_tabelas['permissao_visualizar_centro_custo_conta'];
				$permissao_editar_centro_custo_conta = $dados_tabelas['permissao_editar_centro_custo_conta'];
				$permissao_excluir_centro_custo_conta = $dados_tabelas['permissao_excluir_centro_custo_conta'];

				$permissao_cadastrar_banco_conta = $dados_tabelas['permissao_cadastrar_banco_conta'];
				$permissao_visualizar_banco_conta = $dados_tabelas['permissao_visualizar_banco_conta'];
				$permissao_editar_banco_conta = $dados_tabelas['permissao_editar_banco_conta'];
				$permissao_excluir_banco_conta = $dados_tabelas['permissao_excluir_banco_conta'];
			}
			
			$senha_conta = base64_decode($senha_conta);
			$senha_conta = $this->crypt->getDecrypt($senha_conta,$this->config->config['encryption_key']);
			$senha_conta = strip_tags($senha_conta);

			$this->smarty_ci->assign('codigo_empresa',$codigo_empresa);
			$this->smarty_ci->assign('nome_empresa',$nome_empresa);
			
			$this->smarty_ci->assign('permissao_cliente_empresa',$permissao_cliente_empresa);
			$this->smarty_ci->assign('permissao_colaborador_empresa',$permissao_colaborador_empresa);
			$this->smarty_ci->assign('permissao_conta_empresa',$permissao_conta_empresa);
			$this->smarty_ci->assign('permissao_estoque_empresa',$permissao_estoque_empresa);
			$this->smarty_ci->assign('permissao_financeiro_empresa',$permissao_financeiro_empresa);
			$this->smarty_ci->assign('permissao_fornecedor_empresa',$permissao_fornecedor_empresa);
			$this->smarty_ci->assign('permissao_produto_empresa',$permissao_produto_empresa);
			$this->smarty_ci->assign('permissao_servico_empresa',$permissao_servico_empresa);
			$this->smarty_ci->assign('permissao_centro_custo_empresa',$permissao_centro_custo_empresa);
			$this->smarty_ci->assign('permissao_banco_empresa',$permissao_banco_empresa);
			$this->smarty_ci->assign('codigo_conta',$codigo_conta);
			$this->smarty_ci->assign('usuario_conta',$usuario_conta);
			$this->smarty_ci->assign('senha_conta',$senha_conta);

			$this->smarty_ci->assign('permissao_cadastrar_cliente_conta',$permissao_cadastrar_cliente_conta);
			$this->smarty_ci->assign('permissao_visualizar_cliente_conta',$permissao_visualizar_cliente_conta);
			$this->smarty_ci->assign('permissao_editar_cliente_conta',$permissao_editar_cliente_conta);
			$this->smarty_ci->assign('permissao_excluir_cliente_conta',$permissao_excluir_cliente_conta);

			$this->smarty_ci->assign('permissao_cadastrar_colaborador_conta',$permissao_cadastrar_colaborador_conta);
			$this->smarty_ci->assign('permissao_visualizar_colaborador_conta',$permissao_visualizar_colaborador_conta);
			$this->smarty_ci->assign('permissao_editar_colaborador_conta',$permissao_editar_colaborador_conta);
			$this->smarty_ci->assign('permissao_excluir_colaborador_conta',$permissao_excluir_colaborador_conta);

			$this->smarty_ci->assign('permissao_cadastrar_conta_conta',$permissao_cadastrar_conta_conta);
			$this->smarty_ci->assign('permissao_visualizar_conta_conta',$permissao_visualizar_conta_conta);
			$this->smarty_ci->assign('permissao_editar_conta_conta',$permissao_editar_conta_conta);
			$this->smarty_ci->assign('permissao_excluir_conta_conta',$permissao_excluir_conta_conta);

			$this->smarty_ci->assign('permissao_cadastrar_estoque_conta',$permissao_cadastrar_estoque_conta);
			$this->smarty_ci->assign('permissao_visualizar_estoque_conta',$permissao_visualizar_estoque_conta);
			$this->smarty_ci->assign('permissao_editar_estoque_conta',$permissao_editar_estoque_conta);
			$this->smarty_ci->assign('permissao_excluir_estoque_conta',$permissao_excluir_estoque_conta);

			$this->smarty_ci->assign('permissao_cadastrar_financeiro_conta',$permissao_cadastrar_financeiro_conta);
			$this->smarty_ci->assign('permissao_visualizar_financeiro_conta',$permissao_visualizar_financeiro_conta);
			$this->smarty_ci->assign('permissao_editar_financeiro_conta',$permissao_editar_financeiro_conta);
			$this->smarty_ci->assign('permissao_excluir_financeiro_conta',$permissao_excluir_financeiro_conta);

			$this->smarty_ci->assign('permissao_cadastrar_fornecedor_conta',$permissao_cadastrar_fornecedor_conta);
			$this->smarty_ci->assign('permissao_visualizar_fornecedor_conta',$permissao_visualizar_fornecedor_conta);
			$this->smarty_ci->assign('permissao_editar_fornecedor_conta',$permissao_editar_fornecedor_conta);
			$this->smarty_ci->assign('permissao_excluir_fornecedor_conta',$permissao_excluir_fornecedor_conta);

			$this->smarty_ci->assign('permissao_cadastrar_produto_conta',$permissao_cadastrar_produto_conta);
			$this->smarty_ci->assign('permissao_visualizar_produto_conta',$permissao_visualizar_produto_conta);
			$this->smarty_ci->assign('permissao_editar_produto_conta',$permissao_editar_produto_conta);
			$this->smarty_ci->assign('permissao_excluir_produto_conta',$permissao_excluir_produto_conta);

			$this->smarty_ci->assign('permissao_cadastrar_servico_conta',$permissao_cadastrar_servico_conta);
			$this->smarty_ci->assign('permissao_visualizar_servico_conta',$permissao_visualizar_servico_conta);
			$this->smarty_ci->assign('permissao_editar_servico_conta',$permissao_editar_servico_conta);
			$this->smarty_ci->assign('permissao_excluir_servico_conta',$permissao_excluir_servico_conta);

			$this->smarty_ci->assign('permissao_cadastrar_centro_custo_conta',$permissao_cadastrar_centro_custo_conta);
			$this->smarty_ci->assign('permissao_visualizar_centro_custo_conta',$permissao_visualizar_centro_custo_conta);
			$this->smarty_ci->assign('permissao_editar_centro_custo_conta',$permissao_editar_centro_custo_conta);
			$this->smarty_ci->assign('permissao_excluir_centro_custo_conta',$permissao_excluir_centro_custo_conta);

			$this->smarty_ci->assign('permissao_cadastrar_banco_conta',$permissao_cadastrar_banco_conta);
			$this->smarty_ci->assign('permissao_visualizar_banco_conta',$permissao_visualizar_banco_conta);
			$this->smarty_ci->assign('permissao_editar_banco_conta',$permissao_editar_banco_conta);
			$this->smarty_ci->assign('permissao_excluir_banco_conta',$permissao_excluir_banco_conta);

			$this->smarty_ci->display('editar-administrador.tpl');
		}
	}

	public function fazerEdicao(){
		$this->load->model('conta_model','conta_model');
		$this->load->model('empresa_model','empresa_model');

		$this->form_validation->set_rules('nome_empresa_editar_administrador','nome da empresa','max_length[80]|required');
		$this->form_validation->set_rules('usuario_editar_administrador','usuário','max_length[20]|alpha_dash|min_length[6]|required');
		$this->form_validation->set_rules('senha_editar_administrador','senha','senha|alpha_dash|required');

		if(!$this->form_validation->run()){
			$this->form_validation->set_error_delimiters("", "");
			
			$this->json_collection->assign('alerta',form_error('nome_empresa_editar_administrador'));
			$this->json_collection->assign('alerta',form_error('usuario_editar_administrador'));
			$this->json_collection->assign('alerta',form_error('senha_editar_administrador'));
			$this->json_collection->display();

			exit;
		}else{
			$dados = elements(
				array(
					'codigo_empresa_editar_administrador',
					'nome_empresa_editar_administrador',
					'usuario_editar_administrador',
					'senha_editar_administrador',
					'permissao_conta_editar_administrador',
					'permissao_cliente_editar_administrador',
					'permissao_colaborador_editar_administrador',
					'permissao_fornecedor_editar_administrador',
					'permissao_estoque_editar_administrador',
					'permissao_servico_editar_administrador',
					'permissao_financeiro_editar_administrador',
					'permissao_produto_editar_administrador',
					'permissao_banco_editar_administrador',
					'permissao_centro_custo_editar_administrador'
				), $this->input->post()
			);

			$codigo_empresa = (int)$dados['codigo_empresa_editar_administrador'];

			// pega o codigo da conta administradora dessa empresa
			$this->db->select('
				codigo_empresa,

				codigo_conta,
				codigo_empresa_conta,
				status_conta,
				permissao_administrador_conta
			');

			$this->db->from('conta');
			$this->db->join('empresa','empresa.codigo_empresa = conta.codigo_empresa_conta');
			$this->db->where('conta.permissao_administrador_conta','1');
			$this->db->where('conta.status_conta','1');
			$this->db->where('empresa.codigo_empresa',$codigo_empresa);
			$sql_consulta_tabelas = $this->db->get();

			if($sql_consulta_tabelas->num_rows === 1){
				foreach($sql_consulta_tabelas->result_array() as $dados_consulta_tabelas){
					$codigo_conta = $dados_consulta_tabelas['codigo_conta'];
				}
			}else{
				$this->json_collection->assign('alerta','Essa empresa está corrompida, por favor exclua.');
				$this->json_collection->display();
				exit;
			}

			// verifica se existe uma empresa com o mesmo nome digitado
			$this->db->select('
				codigo_empresa,
				nome_empresa
			');

			$this->db->from('empresa');
			$this->db->where('codigo_empresa !=',$codigo_empresa);
			$this->db->where('nome_empresa',$dados['nome_empresa_editar_administrador']);
			$sql_consulta_empresa = $this->db->get();

			if($sql_consulta_empresa->num_rows !== 0){
				$this->json_collection->assign('alerta','O campo nome empresa ja foi cadastrado.');
				$this->json_collection->display();
				exit;
			}

			// verifica se existe uma conta com o mesmo nome de usuario digitado
			$this->db->select('
				codigo_conta,
				usuario_conta
			');

			$this->db->from('conta');
			$this->db->where('codigo_conta !=',$codigo_conta);
			$this->db->where('usuario_conta',$dados['usuario_editar_administrador']);
			$sql_consulta_conta = $this->db->get();

			if($sql_consulta_conta->num_rows !== 0){
				$this->json_collection->assign('alerta','O campo usuário ja foi cadastrado.');
				$this->json_collection->display();
				exit;
			}
			
			$permissao_conta_editar_administrador = $dados['permissao_conta_editar_administrador'];
			$permissao_cliente_editar_administrador = $dados['permissao_cliente_editar_administrador'];
			$permissao_colaborador_editar_administrador = $dados['permissao_colaborador_editar_administrador'];
			$permissao_fornecedor_editar_administrador = $dados['permissao_fornecedor_editar_administrador'];
			$permissao_estoque_editar_administrador = $dados['permissao_estoque_editar_administrador'];
			$permissao_servico_editar_administrador = $dados['permissao_servico_editar_administrador'];
			$permissao_financeiro_editar_administrador = $dados['permissao_financeiro_editar_administrador'];
			$permissao_produto_editar_administrador = $dados['permissao_produto_editar_administrador'];
			$permissao_banco_editar_administrador = $dados['permissao_banco_editar_administrador'];
			$permissao_centro_custo_editar_administrador = $dados['permissao_centro_custo_editar_administrador'];

			$permissao_conta_editar_administrador = str_replace('true','1',$permissao_conta_editar_administrador);
			$permissao_conta_editar_administrador = str_replace('false','0',$permissao_conta_editar_administrador);

			$permissao_cliente_editar_administrador = str_replace('true','1',$permissao_cliente_editar_administrador);
			$permissao_cliente_editar_administrador = str_replace('false','0',$permissao_cliente_editar_administrador);

			$permissao_colaborador_editar_administrador = str_replace('true','1',$permissao_colaborador_editar_administrador);
			$permissao_colaborador_editar_administrador = str_replace('false','0',$permissao_colaborador_editar_administrador);

			$permissao_fornecedor_editar_administrador = str_replace('true','1',$permissao_fornecedor_editar_administrador);
			$permissao_fornecedor_editar_administrador = str_replace('false','0',$permissao_fornecedor_editar_administrador);

			$permissao_estoque_editar_administrador = str_replace('true','1',$permissao_estoque_editar_administrador);
			$permissao_estoque_editar_administrador = str_replace('false','0',$permissao_estoque_editar_administrador);

			$permissao_servico_editar_administrador = str_replace('true','1',$permissao_servico_editar_administrador);
			$permissao_servico_editar_administrador = str_replace('false','0',$permissao_servico_editar_administrador);

			$permissao_financeiro_editar_administrador = str_replace('true','1',$permissao_financeiro_editar_administrador);
			$permissao_financeiro_editar_administrador = str_replace('false','0',$permissao_financeiro_editar_administrador);

			$permissao_produto_editar_administrador = str_replace('true','1',$permissao_produto_editar_administrador);
			$permissao_produto_editar_administrador = str_replace('false','0',$permissao_produto_editar_administrador);

			$permissao_banco_editar_administrador = str_replace('true','1',$permissao_banco_editar_administrador);
			$permissao_banco_editar_administrador = str_replace('false','0',$permissao_banco_editar_administrador);

			$permissao_centro_custo_editar_administrador = str_replace('true','1',$permissao_centro_custo_editar_administrador);
			$permissao_centro_custo_editar_administrador = str_replace('false','0',$permissao_centro_custo_editar_administrador);

			if($permissao_conta_editar_administrador === '0' && $permissao_cliente_editar_administrador === '0' && $permissao_colaborador_editar_administrador === '0' && $permissao_fornecedor_editar_administrador === '0' && $permissao_estoque_editar_administrador === '0' && $permissao_servico_editar_administrador === '0' && $permissao_financeiro_editar_administrador === '0' && $permissao_produto_editar_administrador === '0' && $permissao_banco_editar_administrador === '0' && $permissao_centro_custo_editar_administrador === '0'){
				$this->json_collection->assign('alerta','Selecione pelo menos uma permissão');
				$this->json_collection->display();

				exit;
			}

			$this->empresa_model->setCodigo($codigo_empresa);
			$this->empresa_model->setNome($this->db->escape_str($dados['nome_empresa_editar_administrador']));
			$this->empresa_model->setPermissaoCliente($permissao_cliente_editar_administrador);
			$this->empresa_model->setPermissaoColaborador($permissao_colaborador_editar_administrador);
			$this->empresa_model->setPermissaoConta($permissao_conta_editar_administrador);
			$this->empresa_model->setPermissaoEstoque($permissao_estoque_editar_administrador);
			$this->empresa_model->setPermissaoFinanceiro($permissao_financeiro_editar_administrador);
			$this->empresa_model->setPermissaoFornecedor($permissao_fornecedor_editar_administrador);
			$this->empresa_model->setPermissaoProduto($permissao_produto_editar_administrador);
			$this->empresa_model->setPermissaoServico($permissao_servico_editar_administrador);
			$this->empresa_model->setPermissaoCentroCusto($permissao_centro_custo_editar_administrador);
			$this->empresa_model->setPermissaoBanco($permissao_banco_editar_administrador);
			
			try{
				$sql_empresa = $this->empresa_model->getEditar();

				if($sql_empresa){
					$usuario = $dados['usuario_editar_administrador'];
					$senha = $dados['senha_editar_administrador'];

					$usuario = strtolower($usuario);
					$senha = strtolower($senha);

					$senha = $this->crypt->getEncrypt($senha,$this->config->config['encryption_key']);
					$senha = base64_encode($senha);

					$this->conta_model->setCodigo($codigo_conta);
					$this->conta_model->setUsuario($usuario);
					$this->conta_model->setSenha($senha);

					if($permissao_cliente_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarCliente('1');
						$this->conta_model->setPermissaoVisualizarCliente('1');
						$this->conta_model->setPermissaoEditarCliente('1');
						$this->conta_model->setPermissaoExcluirCliente('1');
					}else{
						$this->conta_model->setPermissaoCadastrarCliente('1');
						$this->conta_model->setPermissaoVisualizarCliente('1');
						$this->conta_model->setPermissaoEditarCliente('1');
						$this->conta_model->setPermissaoExcluirCliente('1');
					}

					if($permissao_colaborador_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarColaborador('1');
						$this->conta_model->setPermissaoVisualizarColaborador('1');
						$this->conta_model->setPermissaoEditarColaborador('1');
						$this->conta_model->setPermissaoExcluirColaborador('1');
					}else{
						$this->conta_model->setPermissaoCadastrarColaborador('0');
						$this->conta_model->setPermissaoVisualizarColaborador('0');
						$this->conta_model->setPermissaoEditarColaborador('0');
						$this->conta_model->setPermissaoExcluirColaborador('0');
					}

					if($permissao_conta_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarConta('1');
						$this->conta_model->setPermissaoVisualizarConta('1');
						$this->conta_model->setPermissaoEditarConta('1');
						$this->conta_model->setPermissaoExcluirConta('1');
					}else{
						$this->conta_model->setPermissaoCadastrarConta('0');
						$this->conta_model->setPermissaoVisualizarConta('0');
						$this->conta_model->setPermissaoEditarConta('0');
						$this->conta_model->setPermissaoExcluirConta('0');
					}

					if($permissao_estoque_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarEstoque('1');
						$this->conta_model->setPermissaoVisualizarEstoque('1');
						$this->conta_model->setPermissaoEditarEstoque('1');
						$this->conta_model->setPermissaoExcluirEstoque('1');	
					}else{
						$this->conta_model->setPermissaoCadastrarEstoque('0');
						$this->conta_model->setPermissaoVisualizarEstoque('0');
						$this->conta_model->setPermissaoEditarEstoque('0');
						$this->conta_model->setPermissaoExcluirEstoque('0');
					}

					if($permissao_financeiro_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarFinanceiro('1');
						$this->conta_model->setPermissaoVisualizarFinanceiro('1');
						$this->conta_model->setPermissaoEditarFinanceiro('1');
						$this->conta_model->setPermissaoExcluirFinanceiro('1');	
					}else{
						$this->conta_model->setPermissaoCadastrarFinanceiro('0');
						$this->conta_model->setPermissaoVisualizarFinanceiro('0');
						$this->conta_model->setPermissaoEditarFinanceiro('0');
						$this->conta_model->setPermissaoExcluirFinanceiro('0');
					}

					if($permissao_fornecedor_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarFornecedor('1');
						$this->conta_model->setPermissaoVisualizarFornecedor('1');
						$this->conta_model->setPermissaoEditarFornecedor('1');
						$this->conta_model->setPermissaoExcluirFornecedor('1');
					}else{
						$this->conta_model->setPermissaoCadastrarFornecedor('0');
						$this->conta_model->setPermissaoVisualizarFornecedor('0');
						$this->conta_model->setPermissaoEditarFornecedor('0');
						$this->conta_model->setPermissaoExcluirFornecedor('0');
					}

					if($permissao_produto_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarProduto('1');
						$this->conta_model->setPermissaoVisualizarProduto('1');
						$this->conta_model->setPermissaoEditarProduto('1');
						$this->conta_model->setPermissaoExcluirProduto('1');	
					}else{
						$this->conta_model->setPermissaoCadastrarProduto('0');
						$this->conta_model->setPermissaoVisualizarProduto('0');
						$this->conta_model->setPermissaoEditarProduto('0');
						$this->conta_model->setPermissaoExcluirProduto('0');
					}

					if($permissao_servico_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarServico('1');
						$this->conta_model->setPermissaoVisualizarServico('1');
						$this->conta_model->setPermissaoEditarServico('1');
						$this->conta_model->setPermissaoExcluirServico('1');
					}else{
						$this->conta_model->setPermissaoCadastrarServico('0');
						$this->conta_model->setPermissaoVisualizarServico('0');
						$this->conta_model->setPermissaoEditarServico('0');
						$this->conta_model->setPermissaoExcluirServico('0');
					}

					if($permissao_banco_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarBanco('1');
						$this->conta_model->setPermissaoVisualizarBanco('1');
						$this->conta_model->setPermissaoEditarBanco('1');
						$this->conta_model->setPermissaoExcluirBanco('1');
					}else{
						$this->conta_model->setPermissaoCadastrarBanco('0');
						$this->conta_model->setPermissaoVisualizarBanco('0');
						$this->conta_model->setPermissaoEditarBanco('0');
						$this->conta_model->setPermissaoExcluirBanco('0');
					}

					if($permissao_centro_custo_editar_administrador === '1'){
						$this->conta_model->setPermissaoCadastrarCentroCusto('1');
						$this->conta_model->setPermissaoVisualizarCentroCusto('1');
						$this->conta_model->setPermissaoEditarCentroCusto('1');
						$this->conta_model->setPermissaoExcluirCentroCusto('1');
					}else{
						$this->conta_model->setPermissaoCadastrarCentroCusto('0');
						$this->conta_model->setPermissaoVisualizarCentroCusto('0');
						$this->conta_model->setPermissaoEditarCentroCusto('0');
						$this->conta_model->setPermissaoExcluirCentroCusto('0');
					}
					
					try{
						$sql_conta = $this->conta_model->getEditar();

						if($sql_conta){
							$this->json_collection->assign('sucesso','true');
							$this->json_collection->display();
							exit;
						}else{
							$this->empresa_model->setCodigo($codigo_empresa);
							$this->empresa_model->getDesativar();

							$this->json_collection->assign('alerta','Não foi possível cadastrar essa conta no sistema, por favor tente novamente.');
							$this->json_collection->display();
							exit;
						}
					}catch(Exception $e){
						$this->json_collection->assign('alerta','Não foi possível cadastrar essa conta no sistema, por favor tente novamente. Copie a informação a seguir e entre em contato com o desenvolvedor responsável: '.$e->getMessage());
						$this->json_collection->display();
						exit;
					}
				}else{
					$this->json_collection->assign('alerta','Não foi possível cadastrar esse administrador, por favor verifique seus dados e tente novamente.');
					$this->json_collection->display();
					exit;
				}
			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível cadastrar essa empresa no sistema, por favor tente novamente. Copie a informação a seguir e entre em contato com o desenvolvedor responsável: '.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}
	}
}