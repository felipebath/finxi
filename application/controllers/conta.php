<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conta extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$this->smarty_ci->display('entrar-conta.tpl');
	}

	public function testeCadastrar(){
		$this->load->model('conta_model','conta_model');
		
		$this->conta_model->setCodigoEmpresa('1');
		$this->conta_model->setStatus('1');
		$this->conta_model->setUsuario('gsuper');

		$senha = $this->crypt->getEncrypt('ssuser03',$this->config->config['encryption_key']);
		$senha = base64_encode('ssuser03');
		$this->conta_model->setSenha($senha);
		$this->conta_model->setPermissaoAdministrador('0');
		
		$this->conta_model->setPermissaoCadastrarCliente('1');
		$this->conta_model->setPermissaoVisualizarCliente('1');
		$this->conta_model->setPermissaoEditarCliente('1');
		$this->conta_model->setPermissaoExcluirCliente('1');
		
		$this->conta_model->setPermissaoCadastrarColaborador('1');
		$this->conta_model->setPermissaoVisualizarColaborador('1');
		$this->conta_model->setPermissaoEditarColaborador('1');
		$this->conta_model->setPermissaoExcluirColaborador('1');

		$this->conta_model->setPermissaoCadastrarConta('1');
		$this->conta_model->setPermissaoVisualizarConta('1');
		$this->conta_model->setPermissaoEditarConta('1');
		$this->conta_model->setPermissaoExcluirConta('1');
		
		$this->conta_model->setPermissaoCadastrarEstoque('1');
		$this->conta_model->setPermissaoVisualizarEstoque('1');
		$this->conta_model->setPermissaoEditarEstoque('1');
		$this->conta_model->setPermissaoExcluirEstoque('1');
		
		$this->conta_model->setPermissaoCadastrarFinanceiro('1');
		$this->conta_model->setPermissaoVisualizarFinanceiro('1');
		$this->conta_model->setPermissaoEditarFinanceiro('1');
		$this->conta_model->setPermissaoExcluirFinanceiro('1');

		$this->conta_model->setPermissaoCadastrarFornecedor('1');
		$this->conta_model->setPermissaoVisualizarFornecedor('1');
		$this->conta_model->setPermissaoEditarFornecedor('1');
		$this->conta_model->setPermissaoExcluirFornecedor('1');

		$this->conta_model->setPermissaoCadastrarProduto('1');
		$this->conta_model->setPermissaoVisualizarProduto('1');
		$this->conta_model->setPermissaoEditarProduto('1');
		$this->conta_model->setPermissaoExcluirProduto('1');
		
		$this->conta_model->setPermissaoCadastrarServico('1');
		$this->conta_model->setPermissaoVisualizarServico('1');
		$this->conta_model->setPermissaoEditarServico('1');
		$this->conta_model->setPermissaoExcluirServico('1');
		
		try{
			$retorno = $this->conta_model->getCadastrar();

			if($retorno){
				print("Teste - contaCadastrar - Sucesso <br>");
			}else{
				print("Teste - contaCadastrar - Falhou <br>");
			}
		}catch(Exception $e){
			print($e->getMessage());
		}
	}

	public function testeEditar(){
		$this->load->model('conta_model','conta_model');
		
		$this->conta_model->setCodigo('9');
		$this->conta_model->setCodigoEmpresa('2');
		$this->conta_model->setStatus('1');
		$this->conta_model->setUsuario('teste');
		$this->conta_model->setSenha('ssuser01');
		$this->conta_model->setPermissaoAdministrador('0');
		
		$this->conta_model->setPermissaoCadastrarCliente('0');
		$this->conta_model->setPermissaoVisualizarCliente('0');
		$this->conta_model->setPermissaoEditarCliente('0');
		$this->conta_model->setPermissaoExcluirCliente('0');
		
		$this->conta_model->setPermissaoCadastrarColaborador('1');
		$this->conta_model->setPermissaoVisualizarColaborador('1');
		$this->conta_model->setPermissaoEditarColaborador('1');
		$this->conta_model->setPermissaoExcluirColaborador('1');

		$this->conta_model->setPermissaoCadastrarConta('1');
		$this->conta_model->setPermissaoVisualizarConta('1');
		$this->conta_model->setPermissaoEditarConta('1');
		$this->conta_model->setPermissaoExcluirConta('1');
		
		$this->conta_model->setPermissaoCadastrarEstoque('1');
		$this->conta_model->setPermissaoVisualizarEstoque('1');
		$this->conta_model->setPermissaoEditarEstoque('1');
		$this->conta_model->setPermissaoExcluirEstoque('1');
		
		$this->conta_model->setPermissaoCadastrarFinanceiro('1');
		$this->conta_model->setPermissaoVisualizarFinanceiro('1');
		$this->conta_model->setPermissaoEditarFinanceiro('1');
		$this->conta_model->setPermissaoExcluirFinanceiro('1');

		$this->conta_model->setPermissaoCadastrarFornecedor('1');
		$this->conta_model->setPermissaoVisualizarFornecedor('1');
		$this->conta_model->setPermissaoEditarFornecedor('1');
		$this->conta_model->setPermissaoExcluirFornecedor('1');

		$this->conta_model->setPermissaoCadastrarProduto('1');
		$this->conta_model->setPermissaoVisualizarProduto('1');
		$this->conta_model->setPermissaoEditarProduto('1');
		$this->conta_model->setPermissaoExcluirProduto('1');
		
		$this->conta_model->setPermissaoCadastrarServico('1');
		$this->conta_model->setPermissaoVisualizarServico('1');
		$this->conta_model->setPermissaoEditarServico('1');
		$this->conta_model->setPermissaoExcluirServico('1');
		
		try{
			$retorno = $this->conta_model->getEditar();

			if($retorno){
				print("Teste - contaEditar - Sucesso <br>");
			}else{
				print("Teste - contaEditar - Falhou <br>");
			}
		}catch(Exception $e){
			print($e->getMessage());
		}
	}

	public function testeAtivar(){
		$this->load->model('conta_model','conta_model');
		
		$this->conta_model->setCodigo('9');
		$this->conta_model->setStatus('1');
		
		try{
			$retorno = $this->conta_model->getAtivar();

			if($retorno){
				print("Teste - contaAtivar - Sucesso <br>");
			}else{
				print("Teste - contaAtivar - Falhou <br>");
			}
		}catch(Exception $e){
			print($e->getMessage());
		}
	}

	public function testeDesativar(){
		$this->load->model('conta_model','conta_model');
		
		$this->conta_model->setCodigo('9');
		$this->conta_model->setStatus('0');
		
		try{
			$retorno = $this->conta_model->getDesativar();

			if($retorno){
				print("Teste - contaDesativar - Sucesso <br>");
			}else{
				print("Teste - contaDesativar - Falhou <br>");
			}
		}catch(Exception $e){
			print($e->getMessage());
		}
	}

	public function cadastrar(){
		if($this->session->userdata('permissao_cadastrar_conta_conta') === '1'){
			$this->db->select('
				codigo_empresa,
				nome_empresa,
				permissao_cliente_empresa,
				permissao_colaborador_empresa,
				permissao_conta_empresa,
				permissao_estoque_empresa,
				permissao_financeiro_empresa,
				permissao_fornecedor_empresa,
				permissao_produto_empresa,
				permissao_servico_empresa,
				permissao_centro_custo_empresa,
				permissao_banco_empresa,

				codigo_empresa_conta,
				usuario_conta
			');

			$this->db->from('empresa');
			$this->db->join('conta','conta.codigo_empresa_conta = empresa.codigo_empresa');
			$this->db->where('conta.usuario_conta',$this->session->userdata('usuario_conta'));
			$this->db->order_by('nome_empresa','ASC');
			$sql_consulta_tabelas = $this->db->get();

			if($sql_consulta_tabelas->num_rows === 1){
				foreach($sql_consulta_tabelas->result_array() as $dados_consulta_tabelas){
					$codigo_empresa = $dados_consulta_tabelas['codigo_empresa'];
					$permissao_cliente_empresa = $dados_consulta_tabelas['permissao_cliente_empresa'];
					$permissao_colaborador_empresa = $dados_consulta_tabelas['permissao_colaborador_empresa'];
					$permissao_conta_empresa = $dados_consulta_tabelas['permissao_conta_empresa'];
					$permissao_estoque_empresa = $dados_consulta_tabelas['permissao_estoque_empresa'];		
					$permissao_financeiro_empresa = $dados_consulta_tabelas['permissao_financeiro_empresa'];	
					$permissao_fornecedor_empresa = $dados_consulta_tabelas['permissao_fornecedor_empresa'];
					$permissao_produto_empresa = $dados_consulta_tabelas['permissao_produto_empresa'];	
					$permissao_servico_empresa = $dados_consulta_tabelas['permissao_servico_empresa'];
					$permissao_centro_custo_empresa = $dados_consulta_tabelas['permissao_centro_custo_empresa'];
					$permissao_banco_empresa = $dados_consulta_tabelas['permissao_banco_empresa'];
				}
			}else{
				header("Location:" . base_url() . 'inicio');
			}

			$this->smarty_ci->assign('codigo_empresa',$codigo_empresa);
			$this->smarty_ci->assign('permissao_cliente_empresa',$permissao_cliente_empresa);
			$this->smarty_ci->assign('permissao_colaborador_empresa',$permissao_colaborador_empresa);
			$this->smarty_ci->assign('permissao_conta_empresa',$permissao_conta_empresa);
			$this->smarty_ci->assign('permissao_estoque_empresa',$permissao_estoque_empresa);
			$this->smarty_ci->assign('permissao_financeiro_empresa',$permissao_financeiro_empresa);
			$this->smarty_ci->assign('permissao_fornecedor_empresa',$permissao_fornecedor_empresa);
			$this->smarty_ci->assign('permissao_produto_empresa',$permissao_produto_empresa);
			$this->smarty_ci->assign('permissao_servico_empresa',$permissao_servico_empresa);
			$this->smarty_ci->assign('permissao_centro_custo_empresa',$permissao_centro_custo_empresa);
			$this->smarty_ci->assign('permissao_banco_empresa',$permissao_banco_empresa);			
			$this->smarty_ci->display('cadastrar-conta.tpl');
		}else{
			header("Location:" . base_url() . 'inicio');
		}	
	}

	public function entrar(){
		$this->load->model('conta_model','conta_model');

		$this->form_validation->set_rules('usuario_entrar_conta','max_length[20]|min_length[6]|required');
		$this->form_validation->set_rules('senha_entrar_conta','senha|required');

		if(!$this->form_validation->run()){
			$this->form_validation->set_error_delimiters("", "");
			
			$this->json_collection->assign('alerta',form_error('usuario_entrar_conta'));
			$this->json_collection->assign('alerta',form_error('senha_entrar_conta'));

			$this->json_collection->display();
			exit;
		}else{
			$dados = elements(
				array(
					'usuario_entrar_conta',
					'senha_entrar_conta'
				),

				$this->input->post()
			);

			if($dados['usuario_entrar_conta'] === 'gsuper' && $dados['senha_entrar_conta'] === 'gsuper4582'){
				$sessao['usuario_administrador'] = $dados['usuario_entrar_conta'];
				$this->session->set_userdata($sessao);

				$this->json_collection->assign('sucesso','true');
				$this->json_collection->assign('administrador','true');
				$this->json_collection->display();
				exit;
			}

			$dados['senha_entrar_conta'] = $this->crypt->getEncrypt($dados['senha_entrar_conta'],$this->config->config['encryption_key']);
			$dados['senha_entrar_conta'] = base64_encode($dados['senha_entrar_conta']);			

			$this->conta_model->setUsuario($this->db->escape_str($dados['usuario_entrar_conta']));
			$this->conta_model->setSenha($this->db->escape_str($dados['senha_entrar_conta']));

			try{
				$sql = $this->conta_model->getEntrar();

				if($sql){
					$this->json_collection->assign('sucesso','true');
					$this->json_collection->display();
					exit;
				}else{
					$this->json_collection->assign('alerta','Não foi possível localizar essa conta no sistema, por favor verifique seus dados e tente novamente.');
					$this->json_collection->display();
					exit;
				}
			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível cadastrar essa conta no sistema, por favor tente novamente. Copie a informação a seguir e entre em contato com o desenvolvedor responsável: '.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}
	}

	public function listarContas($pagina){
		if(!$this->validarSessao()){
			header("Location:" . base_url() . 'conta');
		}

		$pagina = (int)$pagina;

		if($pagina <= 0 || $pagina === 1){
			$pagina_posterior = 0;
		}else{
			$pagina_posterior = $pagina;
		}

		$this->db->select('
			codigo_empresa_conta,
			usuario_conta
		');
		$this->db->limit(10,$pagina_posterior);						
		$sql_conta = $this->db->get_where('conta', array('usuario_conta' => $this->session->userdata('usuario_conta')));

		if($sql_conta->num_rows() === 1){
			$dados_conta = $sql_conta->row();

			$codigo_empresa = $dados_conta->codigo_empresa_conta;
		}
		
		$this->db->select('
			codigo_conta,
			status_conta,
			codigo_empresa_conta,
			usuario_conta,

			codigo_empresa,
			status_empresa,
			nome_empresa
		');

		$this->db->from('empresa');
		$this->db->join('conta','conta.codigo_empresa_conta = empresa.codigo_empresa');
		$this->db->where('conta.codigo_empresa_conta', $codigo_empresa);
		$this->db->where('conta.status_conta','1');
		$this->db->where('empresa.status_empresa','1');
		$this->db->where('conta.usuario_conta !=',$this->session->userdata('usuario_conta'));
		$this->db->order_by('conta.codigo_conta','ASC');
		$sql_consulta_tabelas = $this->db->get();

		if($sql_consulta_tabelas->num_rows !== 0){
			foreach($sql_consulta_tabelas->result_array() as $dados_consulta_tabelas){
				$codigo_conta[] = $dados_consulta_tabelas['codigo_conta'];
				$usuario_conta[] = $dados_consulta_tabelas['usuario_conta'];

				$nome_empresa[] = $dados_consulta_tabelas['nome_empresa'];
			}
		}else{
			$codigo_conta = 0;
			$usuario_conta = 0;
			$nome_empresa = 0;
		}

		$sql_numero_registros = $this->db->count_all('centro_de_custo');

		$this->load->library('pagination');

		$config['base_url'] = base_url() . 'centro_de_custo/index/';
		$config['total_rows'] = $sql_numero_registros;
		$config['per_page'] = 10; 
		
		$config['first_link'] = 'Primeira';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config); 
		$links_paginacao = $this->pagination->create_links();

		$this->smarty_ci->assign('numero_registros',$sql_numero_registros);
		$this->smarty_ci->assign('pagina',$pagina);
		$this->smarty_ci->assign('links_paginacao',$links_paginacao);
		
		$this->smarty_ci->assign('codigo_conta',$codigo_conta);
		$this->smarty_ci->assign('usuario_conta',$usuario_conta);
		$this->smarty_ci->assign('nome_empresa',$nome_empresa);
		$this->smarty_ci->display('listar-contas.tpl');
	}

	public function fazerCadastro(){
		$this->load->model('conta_model','conta_model');

		$this->form_validation->set_message('is_unique','O campo %s ja foi cadastrado.');

		$this->form_validation->set_rules('usuario_cadastrar_conta','usuário','max_length[20]|min_length[6]|required|alpha_dash|is_unique[conta.usuario_conta]');
		$this->form_validation->set_rules('senha_cadastrar_conta','senha','senha|alpha_dash|required');

		if(!$this->form_validation->run()){
			$this->form_validation->set_error_delimiters("", "");
			
			$this->json_collection->assign('alerta',form_error('usuario_cadastrar_conta'));
			$this->json_collection->assign('alerta',form_error('senha_cadastrar_conta'));
			$this->json_collection->display();

			exit;
		}else{
			$dados = elements(
				array(
					'codigo_empresa_cadastrar_conta',
					'usuario_cadastrar_conta',
					'senha_cadastrar_conta',

					'permissao_cadastrar_conta_cadastrar_conta',
					'permissao_visualizar_conta_cadastrar_conta',
					'permissao_editar_conta_cadastrar_conta',
					'permissao_excluir_conta_cadastrar_conta',

					'permissao_cadastrar_cliente_cadastrar_conta',
					'permissao_visualizar_cliente_cadastrar_conta',
					'permissao_editar_cliente_cadastrar_conta',
					'permissao_excluir_cliente_cadastrar_conta',

					'permissao_cadastrar_colaborador_cadastrar_conta',
					'permissao_visualizar_colaborador_cadastrar_conta',
					'permissao_editar_colaborador_cadastrar_conta',
					'permissao_excluir_colaborador_cadastrar_conta',

					'permissao_cadastrar_financeiro_cadastrar_conta',
					'permissao_visualizar_financeiro_cadastrar_conta',
					'permissao_editar_financeiro_cadastrar_conta',
					'permissao_excluir_financeiro_cadastrar_conta',

					'permissao_cadastrar_servico_cadastrar_conta',
					'permissao_visualizar_servico_cadastrar_conta',
					'permissao_editar_servico_cadastrar_conta',
					'permissao_excluir_servico_cadastrar_conta',

					'permissao_cadastrar_produto_cadastrar_conta',
					'permissao_visualizar_produto_cadastrar_conta',
					'permissao_editar_produto_cadastrar_conta',
					'permissao_excluir_produto_cadastrar_conta',

					'permissao_cadastrar_fornecedor_cadastrar_conta',
					'permissao_visualizar_fornecedor_cadastrar_conta',
					'permissao_editar_fornecedor_cadastrar_conta',
					'permissao_excluir_fornecedor_cadastrar_conta',

					'permissao_cadastrar_estoque_cadastrar_conta',
					'permissao_visualizar_estoque_cadastrar_conta',
					'permissao_editar_estoque_cadastrar_conta',
					'permissao_excluir_estoque_cadastrar_conta',

					'permissao_cadastrar_centro_custo_cadastrar_conta',
					'permissao_visualizar_centro_custo_cadastrar_conta',
					'permissao_editar_centro_custo_cadastrar_conta',
					'permissao_excluir_centro_custo_cadastrar_conta',

					'permissao_cadastrar_banco_cadastrar_conta',
					'permissao_visualizar_banco_cadastrar_conta',
					'permissao_editar_banco_cadastrar_conta',
					'permissao_excluir_banco_cadastrar_conta'					

				), $this->input->post()
			);

			$permissao_cadastrar_conta_cadastrar_conta = $dados['permissao_cadastrar_conta_cadastrar_conta'];
			$permissao_visualizar_conta_cadastrar_conta = $dados['permissao_visualizar_conta_cadastrar_conta'];
			$permissao_editar_conta_cadastrar_conta = $dados['permissao_editar_conta_cadastrar_conta'];
			$permissao_excluir_conta_cadastrar_conta = $dados['permissao_excluir_conta_cadastrar_conta'];

			$permissao_cadastrar_cliente_cadastrar_conta = $dados['permissao_cadastrar_cliente_cadastrar_conta'];
			$permissao_visualizar_cliente_cadastrar_conta = $dados['permissao_visualizar_cliente_cadastrar_conta'];
			$permissao_editar_cliente_cadastrar_conta = $dados['permissao_editar_cliente_cadastrar_conta'];
			$permissao_excluir_cliente_cadastrar_conta = $dados['permissao_excluir_cliente_cadastrar_conta'];

			$permissao_cadastrar_colaborador_cadastrar_conta = $dados['permissao_cadastrar_colaborador_cadastrar_conta'];
			$permissao_visualizar_colaborador_cadastrar_conta = $dados['permissao_visualizar_colaborador_cadastrar_conta'];
			$permissao_editar_colaborador_cadastrar_conta = $dados['permissao_editar_colaborador_cadastrar_conta'];
			$permissao_excluir_colaborador_cadastrar_conta = $dados['permissao_excluir_colaborador_cadastrar_conta'];

			$permissao_cadastrar_financeiro_cadastrar_conta = $dados['permissao_cadastrar_financeiro_cadastrar_conta'];
			$permissao_visualizar_financeiro_cadastrar_conta = $dados['permissao_visualizar_financeiro_cadastrar_conta'];
			$permissao_editar_financeiro_cadastrar_conta = $dados['permissao_editar_financeiro_cadastrar_conta'];
			$permissao_excluir_financeiro_cadastrar_conta = $dados['permissao_excluir_financeiro_cadastrar_conta'];

			$permissao_cadastrar_servico_cadastrar_conta = $dados['permissao_cadastrar_servico_cadastrar_conta'];
			$permissao_visualizar_servico_cadastrar_conta = $dados['permissao_visualizar_servico_cadastrar_conta'];
			$permissao_editar_servico_cadastrar_conta = $dados['permissao_editar_servico_cadastrar_conta'];
			$permissao_excluir_servico_cadastrar_conta = $dados['permissao_excluir_servico_cadastrar_conta'];

			$permissao_cadastrar_produto_cadastrar_conta = $dados['permissao_cadastrar_produto_cadastrar_conta'];
			$permissao_visualizar_produto_cadastrar_conta = $dados['permissao_visualizar_produto_cadastrar_conta'];
			$permissao_editar_produto_cadastrar_conta = $dados['permissao_editar_produto_cadastrar_conta'];
			$permissao_excluir_produto_cadastrar_conta = $dados['permissao_excluir_produto_cadastrar_conta'];

			$permissao_cadastrar_fornecedor_cadastrar_conta = $dados['permissao_cadastrar_fornecedor_cadastrar_conta'];
			$permissao_visualizar_fornecedor_cadastrar_conta = $dados['permissao_visualizar_fornecedor_cadastrar_conta'];
			$permissao_editar_fornecedor_cadastrar_conta = $dados['permissao_editar_fornecedor_cadastrar_conta'];
			$permissao_excluir_fornecedor_cadastrar_conta = $dados['permissao_excluir_fornecedor_cadastrar_conta'];

			$permissao_cadastrar_estoque_cadastrar_conta = $dados['permissao_cadastrar_estoque_cadastrar_conta'];
			$permissao_visualizar_estoque_cadastrar_conta = $dados['permissao_visualizar_estoque_cadastrar_conta'];
			$permissao_editar_estoque_cadastrar_conta = $dados['permissao_editar_estoque_cadastrar_conta'];
			$permissao_excluir_estoque_cadastrar_conta = $dados['permissao_excluir_estoque_cadastrar_conta'];

			$permissao_cadastrar_centro_custo_cadastrar_conta = $dados['permissao_cadastrar_centro_custo_cadastrar_conta'];
			$permissao_visualizar_centro_custo_cadastrar_conta = $dados['permissao_visualizar_centro_custo_cadastrar_conta'];
			$permissao_editar_centro_custo_cadastrar_conta = $dados['permissao_editar_centro_custo_cadastrar_conta'];
			$permissao_excluir_centro_custo_cadastrar_conta = $dados['permissao_excluir_centro_custo_cadastrar_conta'];

			$permissao_cadastrar_banco_cadastrar_conta = $dados['permissao_cadastrar_banco_cadastrar_conta'];
			$permissao_visualizar_banco_cadastrar_conta = $dados['permissao_visualizar_banco_cadastrar_conta'];
			$permissao_editar_banco_cadastrar_conta = $dados['permissao_editar_banco_cadastrar_conta'];
			$permissao_excluir_banco_cadastrar_conta = $dados['permissao_excluir_banco_cadastrar_conta'];

			$permissao_cadastrar_conta_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_conta_cadastrar_conta);
			$permissao_cadastrar_conta_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_conta_cadastrar_conta);

			$permissao_visualizar_conta_cadastrar_conta = str_replace('true','1',$permissao_visualizar_conta_cadastrar_conta);
			$permissao_visualizar_conta_cadastrar_conta = str_replace('false','0',$permissao_visualizar_conta_cadastrar_conta);

			$permissao_editar_conta_cadastrar_conta = str_replace('true','1',$permissao_editar_conta_cadastrar_conta);
			$permissao_editar_conta_cadastrar_conta = str_replace('false','0',$permissao_editar_conta_cadastrar_conta);

			$permissao_excluir_conta_cadastrar_conta = str_replace('true','1',$permissao_excluir_conta_cadastrar_conta);
			$permissao_excluir_conta_cadastrar_conta = str_replace('false','0',$permissao_excluir_conta_cadastrar_conta);

			// ---------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_cliente_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_cliente_cadastrar_conta);
			$permissao_cadastrar_cliente_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_cliente_cadastrar_conta);

			$permissao_visualizar_cliente_cadastrar_conta = str_replace('true','1',$permissao_visualizar_cliente_cadastrar_conta);
			$permissao_visualizar_cliente_cadastrar_conta = str_replace('false','0',$permissao_visualizar_cliente_cadastrar_conta);

			$permissao_editar_cliente_cadastrar_conta = str_replace('true','1',$permissao_editar_cliente_cadastrar_conta);
			$permissao_editar_cliente_cadastrar_conta = str_replace('false','0',$permissao_editar_cliente_cadastrar_conta);

			$permissao_excluir_cliente_cadastrar_conta = str_replace('true','1',$permissao_excluir_cliente_cadastrar_conta);
			$permissao_excluir_cliente_cadastrar_conta = str_replace('false','0',$permissao_excluir_cliente_cadastrar_conta);

			// ----------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_colaborador_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_colaborador_cadastrar_conta);
			$permissao_cadastrar_colaborador_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_colaborador_cadastrar_conta);

			$permissao_visualizar_colaborador_cadastrar_conta = str_replace('true','1',$permissao_visualizar_colaborador_cadastrar_conta);
			$permissao_visualizar_colaborador_cadastrar_conta = str_replace('false','0',$permissao_visualizar_colaborador_cadastrar_conta);

			$permissao_editar_colaborador_cadastrar_conta = str_replace('true','1',$permissao_editar_colaborador_cadastrar_conta);
			$permissao_editar_colaborador_cadastrar_conta = str_replace('false','0',$permissao_editar_colaborador_cadastrar_conta);

			$permissao_excluir_colaborador_cadastrar_conta = str_replace('true','1',$permissao_excluir_colaborador_cadastrar_conta);
			$permissao_excluir_colaborador_cadastrar_conta = str_replace('false','0',$permissao_excluir_colaborador_cadastrar_conta);

			// -----------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_financeiro_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_financeiro_cadastrar_conta);
			$permissao_cadastrar_financeiro_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_financeiro_cadastrar_conta);

			$permissao_visualizar_financeiro_cadastrar_conta = str_replace('true','1',$permissao_visualizar_financeiro_cadastrar_conta);
			$permissao_visualizar_financeiro_cadastrar_conta = str_replace('false','0',$permissao_visualizar_financeiro_cadastrar_conta);

			$permissao_editar_financeiro_cadastrar_conta = str_replace('true','1',$permissao_editar_financeiro_cadastrar_conta);
			$permissao_editar_financeiro_cadastrar_conta = str_replace('false','0',$permissao_editar_financeiro_cadastrar_conta);

			$permissao_excluir_financeiro_cadastrar_conta = str_replace('true','1',$permissao_excluir_financeiro_cadastrar_conta);
			$permissao_excluir_financeiro_cadastrar_conta = str_replace('false','0',$permissao_excluir_financeiro_cadastrar_conta);

			// -------------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_servico_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_servico_cadastrar_conta);
			$permissao_cadastrar_servico_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_servico_cadastrar_conta);

			$permissao_visualizar_servico_cadastrar_conta = str_replace('true','1',$permissao_visualizar_servico_cadastrar_conta);
			$permissao_visualizar_servico_cadastrar_conta = str_replace('false','0',$permissao_visualizar_servico_cadastrar_conta);

			$permissao_editar_servico_cadastrar_conta = str_replace('true','1',$permissao_editar_servico_cadastrar_conta);
			$permissao_editar_servico_cadastrar_conta = str_replace('false','0',$permissao_editar_servico_cadastrar_conta);

			$permissao_excluir_servico_cadastrar_conta = str_replace('true','1',$permissao_excluir_servico_cadastrar_conta);
			$permissao_excluir_servico_cadastrar_conta = str_replace('false','0',$permissao_excluir_servico_cadastrar_conta);

			// ------------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_produto_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_produto_cadastrar_conta);
			$permissao_cadastrar_produto_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_produto_cadastrar_conta);

			$permissao_visualizar_produto_cadastrar_conta = str_replace('true','1',$permissao_visualizar_produto_cadastrar_conta);
			$permissao_visualizar_produto_cadastrar_conta = str_replace('false','0',$permissao_visualizar_produto_cadastrar_conta);

			$permissao_editar_produto_cadastrar_conta = str_replace('true','1',$permissao_editar_produto_cadastrar_conta);
			$permissao_editar_produto_cadastrar_conta = str_replace('false','0',$permissao_editar_produto_cadastrar_conta);

			$permissao_excluir_produto_cadastrar_conta = str_replace('true','1',$permissao_excluir_produto_cadastrar_conta);
			$permissao_excluir_produto_cadastrar_conta = str_replace('false','0',$permissao_excluir_produto_cadastrar_conta);

			// ------------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_fornecedor_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_fornecedor_cadastrar_conta);
			$permissao_cadastrar_fornecedor_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_fornecedor_cadastrar_conta);

			$permissao_visualizar_fornecedor_cadastrar_conta = str_replace('true','1',$permissao_visualizar_fornecedor_cadastrar_conta);
			$permissao_visualizar_fornecedor_cadastrar_conta = str_replace('false','0',$permissao_visualizar_fornecedor_cadastrar_conta);

			$permissao_editar_fornecedor_cadastrar_conta = str_replace('true','1',$permissao_editar_fornecedor_cadastrar_conta);
			$permissao_editar_fornecedor_cadastrar_conta = str_replace('false','0',$permissao_editar_fornecedor_cadastrar_conta);

			$permissao_excluir_fornecedor_cadastrar_conta = str_replace('true','1',$permissao_excluir_fornecedor_cadastrar_conta);
			$permissao_excluir_fornecedor_cadastrar_conta = str_replace('false','0',$permissao_excluir_fornecedor_cadastrar_conta);

			// ------------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_estoque_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_estoque_cadastrar_conta);
			$permissao_cadastrar_estoque_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_estoque_cadastrar_conta);

			$permissao_visualizar_estoque_cadastrar_conta = str_replace('true','1',$permissao_visualizar_estoque_cadastrar_conta);
			$permissao_visualizar_estoque_cadastrar_conta = str_replace('false','0',$permissao_visualizar_estoque_cadastrar_conta);

			$permissao_editar_estoque_cadastrar_conta = str_replace('true','1',$permissao_editar_estoque_cadastrar_conta);
			$permissao_editar_estoque_cadastrar_conta = str_replace('false','0',$permissao_editar_estoque_cadastrar_conta);

			$permissao_excluir_estoque_cadastrar_conta = str_replace('true','1',$permissao_excluir_estoque_cadastrar_conta);
			$permissao_excluir_estoque_cadastrar_conta = str_replace('false','0',$permissao_excluir_estoque_cadastrar_conta);

			// ----------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_centro_custo_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_centro_custo_cadastrar_conta);
			$permissao_cadastrar_centro_custo_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_centro_custo_cadastrar_conta);

			$permissao_visualizar_centro_custo_cadastrar_conta = str_replace('true','1',$permissao_visualizar_centro_custo_cadastrar_conta);
			$permissao_visualizar_centro_custo_cadastrar_conta = str_replace('false','0',$permissao_visualizar_centro_custo_cadastrar_conta);

			$permissao_editar_centro_custo_cadastrar_conta = str_replace('true','1',$permissao_editar_centro_custo_cadastrar_conta);
			$permissao_editar_centro_custo_cadastrar_conta = str_replace('false','0',$permissao_editar_centro_custo_cadastrar_conta);

			$permissao_excluir_centro_custo_cadastrar_conta = str_replace('true','1',$permissao_excluir_centro_custo_cadastrar_conta);
			$permissao_excluir_centro_custo_cadastrar_conta = str_replace('false','0',$permissao_excluir_centro_custo_cadastrar_conta);

			// ----------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_banco_cadastrar_conta = str_replace('true','1',$permissao_cadastrar_banco_cadastrar_conta);
			$permissao_cadastrar_banco_cadastrar_conta = str_replace('false','0',$permissao_cadastrar_banco_cadastrar_conta);

			$permissao_visualizar_banco_cadastrar_conta = str_replace('true','1',$permissao_visualizar_banco_cadastrar_conta);
			$permissao_visualizar_banco_cadastrar_conta = str_replace('false','0',$permissao_visualizar_banco_cadastrar_conta);

			$permissao_editar_banco_cadastrar_conta = str_replace('true','1',$permissao_editar_banco_cadastrar_conta);
			$permissao_editar_banco_cadastrar_conta = str_replace('false','0',$permissao_editar_banco_cadastrar_conta);

			$permissao_excluir_banco_cadastrar_conta = str_replace('true','1',$permissao_excluir_banco_cadastrar_conta);
			$permissao_excluir_banco_cadastrar_conta = str_replace('false','0',$permissao_excluir_banco_cadastrar_conta);

			// ----------------------------------------------------------------------------------------------------------------

			$usuario = $dados['usuario_cadastrar_conta'];
			$senha = $dados['senha_cadastrar_conta'];

			$usuario = strtolower($usuario);
			$senha = strtolower($senha);

			$senha = $this->crypt->getEncrypt($senha,$this->config->config['encryption_key']);
			$senha = base64_encode($senha);

			$this->conta_model->setCodigoEmpresa((int)$dados['codigo_empresa_cadastrar_conta']);
			$this->conta_model->setUsuario($usuario);
			$this->conta_model->setSenha($senha);
			$this->conta_model->setPermissaoAdministrador(0);

			$this->conta_model->setPermissaoCadastrarConta($permissao_cadastrar_conta_cadastrar_conta);
			$this->conta_model->setPermissaoEditarConta($permissao_editar_conta_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirConta($permissao_excluir_conta_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarConta($permissao_visualizar_conta_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarCliente($permissao_cadastrar_cliente_cadastrar_conta);
			$this->conta_model->setPermissaoEditarCliente($permissao_editar_cliente_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirCliente($permissao_excluir_cliente_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarCliente($permissao_visualizar_cliente_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarColaborador($permissao_cadastrar_colaborador_cadastrar_conta);
			$this->conta_model->setPermissaoEditarColaborador($permissao_editar_colaborador_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirColaborador($permissao_excluir_colaborador_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarColaborador($permissao_visualizar_colaborador_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarFinanceiro($permissao_cadastrar_financeiro_cadastrar_conta);
			$this->conta_model->setPermissaoEditarFinanceiro($permissao_editar_financeiro_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirFinanceiro($permissao_excluir_financeiro_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarFinanceiro($permissao_visualizar_financeiro_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarServico($permissao_cadastrar_servico_cadastrar_conta);
			$this->conta_model->setPermissaoEditarServico($permissao_editar_servico_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirServico($permissao_excluir_servico_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarServico($permissao_visualizar_servico_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarProduto($permissao_cadastrar_produto_cadastrar_conta);
			$this->conta_model->setPermissaoEditarProduto($permissao_editar_produto_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirProduto($permissao_excluir_produto_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarProduto($permissao_visualizar_produto_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarFornecedor($permissao_cadastrar_fornecedor_cadastrar_conta);
			$this->conta_model->setPermissaoEditarFornecedor($permissao_editar_fornecedor_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirFornecedor($permissao_excluir_fornecedor_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarFornecedor($permissao_visualizar_fornecedor_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarEstoque($permissao_cadastrar_estoque_cadastrar_conta);
			$this->conta_model->setPermissaoEditarEstoque($permissao_editar_estoque_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirEstoque($permissao_excluir_estoque_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarEstoque($permissao_visualizar_estoque_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarCentroCusto($permissao_cadastrar_centro_custo_cadastrar_conta);
			$this->conta_model->setPermissaoEditarCentroCusto($permissao_editar_centro_custo_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirCentroCusto($permissao_excluir_centro_custo_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarCentroCusto($permissao_visualizar_centro_custo_cadastrar_conta);

			$this->conta_model->setPermissaoCadastrarBanco($permissao_cadastrar_banco_cadastrar_conta);
			$this->conta_model->setPermissaoEditarBanco($permissao_editar_banco_cadastrar_conta);
			$this->conta_model->setPermissaoExcluirBanco($permissao_excluir_banco_cadastrar_conta);
			$this->conta_model->setPermissaoVisualizarBanco($permissao_visualizar_banco_cadastrar_conta);

			try{
				$sql_conta = $this->conta_model->getCadastrar();

				if($sql_conta){
					$this->json_collection->assign('sucesso','true');
					$this->json_collection->display();
					exit;
				}else{
					$this->json_collection->assign('alerta','Não foi possível cadastrar essa conta no sistema, por favor tente novamente.');
					$this->json_collection->display();
					exit;
				}
			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível cadastrar essa conta no sistema, por favor tente novamente. Copie a informação a seguir e entre em contato com o desenvolvedor responsável: '.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}
	}

	public function editar($codigo_conta){
		
		if($this->session->userdata('permissao_editar_conta_conta') === '1'){
			$this->db->select('
				codigo_empresa,
				nome_empresa,
				permissao_cliente_empresa,
				permissao_colaborador_empresa,
				permissao_conta_empresa,
				permissao_estoque_empresa,
				permissao_financeiro_empresa,
				permissao_fornecedor_empresa,
				permissao_produto_empresa,
				permissao_servico_empresa,
				permissao_centro_custo_empresa,
				permissao_banco_empresa,

				codigo_empresa_conta,
				usuario_conta
			');

			$this->db->from('empresa');
			$this->db->join('conta','conta.codigo_empresa_conta = empresa.codigo_empresa');
			$this->db->where('conta.usuario_conta',$this->session->userdata('usuario_conta'));
			$this->db->order_by('nome_empresa','ASC');
			$sql_consulta_tabelas = $this->db->get();

			if($sql_consulta_tabelas->num_rows === 1){
				foreach($sql_consulta_tabelas->result_array() as $dados_consulta_tabelas){
					$codigo_empresa = $dados_consulta_tabelas['codigo_empresa'];
					$permissao_cliente_empresa = $dados_consulta_tabelas['permissao_cliente_empresa'];
					$permissao_colaborador_empresa = $dados_consulta_tabelas['permissao_colaborador_empresa'];
					$permissao_conta_empresa = $dados_consulta_tabelas['permissao_conta_empresa'];
					$permissao_estoque_empresa = $dados_consulta_tabelas['permissao_estoque_empresa'];		
					$permissao_financeiro_empresa = $dados_consulta_tabelas['permissao_financeiro_empresa'];	
					$permissao_fornecedor_empresa = $dados_consulta_tabelas['permissao_fornecedor_empresa'];
					$permissao_produto_empresa = $dados_consulta_tabelas['permissao_produto_empresa'];	
					$permissao_servico_empresa = $dados_consulta_tabelas['permissao_servico_empresa'];
					$permissao_centro_custo_empresa = $dados_consulta_tabelas['permissao_centro_custo_empresa'];
					$permissao_banco_empresa = $dados_consulta_tabelas['permissao_banco_empresa'];
				}						
			}else{
				header("Location:" . base_url() . 'inicio');
			}

			$this->db->select('
				codigo_conta,
				usuario_conta,
				senha_conta,

				permissao_cadastrar_cliente_conta,
				permissao_visualizar_cliente_conta,
				permissao_editar_cliente_conta,
				permissao_excluir_cliente_conta,

				permissao_cadastrar_colaborador_conta,
				permissao_visualizar_colaborador_conta,
				permissao_editar_colaborador_conta,
				permissao_excluir_colaborador_conta,

				permissao_cadastrar_conta_conta,
				permissao_visualizar_conta_conta,
				permissao_editar_conta_conta,
				permissao_excluir_conta_conta,

				permissao_cadastrar_estoque_conta,
				permissao_visualizar_estoque_conta,
				permissao_editar_estoque_conta,
				permissao_excluir_estoque_conta,

				permissao_cadastrar_fornecedor_conta,
				permissao_visualizar_fornecedor_conta,
				permissao_editar_fornecedor_conta,
				permissao_excluir_fornecedor_conta,

				permissao_cadastrar_produto_conta,
				permissao_visualizar_produto_conta,
				permissao_editar_produto_conta,
				permissao_excluir_produto_conta,

				permissao_cadastrar_servico_conta,
				permissao_visualizar_servico_conta,
				permissao_editar_servico_conta,
				permissao_excluir_servico_conta,

				permissao_cadastrar_financeiro_conta,
				permissao_visualizar_financeiro_conta,
				permissao_editar_financeiro_conta,
				permissao_excluir_financeiro_conta,

				permissao_cadastrar_centro_custo_conta,
				permissao_visualizar_centro_custo_conta,
				permissao_editar_centro_custo_conta,
				permissao_excluir_centro_custo_conta,

				permissao_cadastrar_banco_conta,
				permissao_visualizar_banco_conta,
				permissao_editar_banco_conta,
				permissao_excluir_banco_conta
			');

			$this->db->from('conta');
			$this->db->where('codigo_conta',(int)$codigo_conta);
			$sql_consulta_conta = $this->db->get();

			if($sql_consulta_conta->num_rows === 1){
				foreach($sql_consulta_conta->result_array() as $dados_consulta_conta){
					$usuario_conta = $dados_consulta_conta['usuario_conta'];
					$senha_conta = $dados_consulta_conta['senha_conta'];

					$permissao_cadastrar_cliente_conta = $dados_consulta_conta['permissao_cadastrar_cliente_conta'];
					$permissao_visualizar_cliente_conta = $dados_consulta_conta['permissao_visualizar_cliente_conta'];
					$permissao_editar_cliente_conta = $dados_consulta_conta['permissao_editar_cliente_conta'];
					$permissao_excluir_cliente_conta = $dados_consulta_conta['permissao_excluir_cliente_conta'];

					$permissao_cadastrar_colaborador_conta = $dados_consulta_conta['permissao_cadastrar_colaborador_conta'];
					$permissao_visualizar_colaborador_conta = $dados_consulta_conta['permissao_visualizar_colaborador_conta'];
					$permissao_editar_colaborador_conta = $dados_consulta_conta['permissao_editar_colaborador_conta'];
					$permissao_excluir_colaborador_conta = $dados_consulta_conta['permissao_excluir_colaborador_conta'];

					$permissao_cadastrar_conta_conta = $dados_consulta_conta['permissao_cadastrar_conta_conta'];
					$permissao_visualizar_conta_conta = $dados_consulta_conta['permissao_visualizar_conta_conta'];
					$permissao_editar_conta_conta = $dados_consulta_conta['permissao_editar_conta_conta'];
					$permissao_excluir_conta_conta = $dados_consulta_conta['permissao_excluir_conta_conta'];

					$permissao_cadastrar_estoque_conta = $dados_consulta_conta['permissao_cadastrar_estoque_conta'];
					$permissao_visualizar_estoque_conta = $dados_consulta_conta['permissao_visualizar_estoque_conta'];
					$permissao_editar_estoque_conta = $dados_consulta_conta['permissao_editar_estoque_conta'];
					$permissao_excluir_estoque_conta = $dados_consulta_conta['permissao_excluir_estoque_conta'];

					$permissao_cadastrar_fornecedor_conta = $dados_consulta_conta['permissao_cadastrar_fornecedor_conta'];
					$permissao_visualizar_fornecedor_conta = $dados_consulta_conta['permissao_visualizar_fornecedor_conta'];
					$permissao_editar_fornecedor_conta = $dados_consulta_conta['permissao_editar_fornecedor_conta'];
					$permissao_excluir_fornecedor_conta = $dados_consulta_conta['permissao_excluir_fornecedor_conta'];

					$permissao_cadastrar_produto_conta = $dados_consulta_conta['permissao_cadastrar_produto_conta'];
					$permissao_visualizar_produto_conta = $dados_consulta_conta['permissao_visualizar_produto_conta'];
					$permissao_editar_produto_conta = $dados_consulta_conta['permissao_editar_produto_conta'];
					$permissao_excluir_produto_conta = $dados_consulta_conta['permissao_excluir_produto_conta'];

					$permissao_cadastrar_servico_conta = $dados_consulta_conta['permissao_cadastrar_servico_conta'];
					$permissao_visualizar_servico_conta = $dados_consulta_conta['permissao_visualizar_servico_conta'];
					$permissao_editar_servico_conta = $dados_consulta_conta['permissao_editar_servico_conta'];
					$permissao_excluir_servico_conta = $dados_consulta_conta['permissao_excluir_servico_conta'];

					$permissao_cadastrar_financeiro_conta = $dados_consulta_conta['permissao_cadastrar_financeiro_conta'];
					$permissao_visualizar_financeiro_conta = $dados_consulta_conta['permissao_visualizar_financeiro_conta'];
					$permissao_editar_financeiro_conta = $dados_consulta_conta['permissao_editar_financeiro_conta'];
					$permissao_excluir_financeiro_conta = $dados_consulta_conta['permissao_excluir_financeiro_conta'];

					$permissao_cadastrar_centro_custo_conta = $dados_consulta_conta['permissao_cadastrar_centro_custo_conta'];
					$permissao_visualizar_centro_custo_conta = $dados_consulta_conta['permissao_visualizar_centro_custo_conta'];
					$permissao_editar_centro_custo_conta = $dados_consulta_conta['permissao_editar_centro_custo_conta'];
					$permissao_excluir_centro_custo_conta = $dados_consulta_conta['permissao_excluir_centro_custo_conta'];

					$permissao_cadastrar_banco_conta = $dados_consulta_conta['permissao_cadastrar_banco_conta'];
					$permissao_visualizar_banco_conta = $dados_consulta_conta['permissao_visualizar_banco_conta'];
					$permissao_editar_banco_conta = $dados_consulta_conta['permissao_editar_banco_conta'];
					$permissao_excluir_banco_conta = $dados_consulta_conta['permissao_excluir_banco_conta'];
				}

				$senha_conta = base64_decode($senha_conta);
				$senha_conta = $this->crypt->getDecrypt($senha_conta,$this->config->config['encryption_key']);
				$senha_conta = strip_tags($senha_conta);


				$this->smarty_ci->assign('codigo_conta',(int)$codigo_conta);
				$this->smarty_ci->assign('usuario_conta',$usuario_conta);
				$this->smarty_ci->assign('senha_conta',$senha_conta);

				$this->smarty_ci->assign('permissao_cadastrar_cliente_conta',$permissao_cadastrar_cliente_conta);
				$this->smarty_ci->assign('permissao_visualizar_cliente_conta',$permissao_visualizar_cliente_conta);
				$this->smarty_ci->assign('permissao_editar_cliente_conta',$permissao_editar_cliente_conta);
				$this->smarty_ci->assign('permissao_excluir_cliente_conta',$permissao_excluir_cliente_conta);

				$this->smarty_ci->assign('permissao_cadastrar_colaborador_conta',$permissao_cadastrar_colaborador_conta);
				$this->smarty_ci->assign('permissao_visualizar_colaborador_conta',$permissao_visualizar_colaborador_conta);
				$this->smarty_ci->assign('permissao_editar_colaborador_conta',$permissao_editar_colaborador_conta);
				$this->smarty_ci->assign('permissao_excluir_colaborador_conta',$permissao_excluir_colaborador_conta);

				$this->smarty_ci->assign('permissao_cadastrar_conta_conta',$permissao_cadastrar_conta_conta);
				$this->smarty_ci->assign('permissao_visualizar_conta_conta',$permissao_visualizar_conta_conta);
				$this->smarty_ci->assign('permissao_editar_conta_conta',$permissao_editar_conta_conta);
				$this->smarty_ci->assign('permissao_excluir_conta_conta',$permissao_excluir_conta_conta);

				$this->smarty_ci->assign('permissao_cadastrar_estoque_conta',$permissao_cadastrar_estoque_conta);
				$this->smarty_ci->assign('permissao_visualizar_estoque_conta',$permissao_visualizar_estoque_conta);
				$this->smarty_ci->assign('permissao_editar_estoque_conta',$permissao_editar_estoque_conta);
				$this->smarty_ci->assign('permissao_excluir_estoque_conta',$permissao_excluir_estoque_conta);

				$this->smarty_ci->assign('permissao_cadastrar_fornecedor_conta',$permissao_cadastrar_fornecedor_conta);
				$this->smarty_ci->assign('permissao_visualizar_fornecedor_conta',$permissao_visualizar_fornecedor_conta);
				$this->smarty_ci->assign('permissao_editar_fornecedor_conta',$permissao_editar_fornecedor_conta);
				$this->smarty_ci->assign('permissao_excluir_fornecedor_conta',$permissao_excluir_fornecedor_conta);

				$this->smarty_ci->assign('permissao_cadastrar_produto_conta',$permissao_cadastrar_produto_conta);
				$this->smarty_ci->assign('permissao_visualizar_produto_conta',$permissao_visualizar_produto_conta);
				$this->smarty_ci->assign('permissao_editar_produto_conta',$permissao_editar_produto_conta);
				$this->smarty_ci->assign('permissao_excluir_produto_conta',$permissao_excluir_produto_conta);

				$this->smarty_ci->assign('permissao_cadastrar_servico_conta',$permissao_cadastrar_servico_conta);
				$this->smarty_ci->assign('permissao_visualizar_servico_conta',$permissao_visualizar_servico_conta);
				$this->smarty_ci->assign('permissao_editar_servico_conta',$permissao_editar_servico_conta);
				$this->smarty_ci->assign('permissao_excluir_servico_conta',$permissao_excluir_servico_conta);

				$this->smarty_ci->assign('permissao_cadastrar_financeiro_conta',$permissao_cadastrar_financeiro_conta);
				$this->smarty_ci->assign('permissao_visualizar_financeiro_conta',$permissao_visualizar_financeiro_conta);
				$this->smarty_ci->assign('permissao_editar_financeiro_conta',$permissao_editar_financeiro_conta);
				$this->smarty_ci->assign('permissao_excluir_financeiro_conta',$permissao_excluir_financeiro_conta);

				$this->smarty_ci->assign('permissao_cadastrar_centro_custo_conta',$permissao_cadastrar_centro_custo_conta);
				$this->smarty_ci->assign('permissao_visualizar_centro_custo_conta',$permissao_visualizar_centro_custo_conta);
				$this->smarty_ci->assign('permissao_editar_centro_custo_conta',$permissao_editar_centro_custo_conta);
				$this->smarty_ci->assign('permissao_excluir_centro_custo_conta',$permissao_excluir_centro_custo_conta);

				$this->smarty_ci->assign('permissao_cadastrar_banco_conta',$permissao_cadastrar_banco_conta);
				$this->smarty_ci->assign('permissao_visualizar_banco_conta',$permissao_visualizar_banco_conta);
				$this->smarty_ci->assign('permissao_editar_banco_conta',$permissao_editar_banco_conta);
				$this->smarty_ci->assign('permissao_excluir_banco_conta',$permissao_excluir_banco_conta);

				$this->smarty_ci->assign('codigo_empresa',$codigo_empresa);
				$this->smarty_ci->assign('permissao_cliente_empresa',$permissao_cliente_empresa);
				$this->smarty_ci->assign('permissao_colaborador_empresa',$permissao_colaborador_empresa);
				$this->smarty_ci->assign('permissao_conta_empresa',$permissao_conta_empresa);
				$this->smarty_ci->assign('permissao_estoque_empresa',$permissao_estoque_empresa);
				$this->smarty_ci->assign('permissao_financeiro_empresa',$permissao_financeiro_empresa);
				$this->smarty_ci->assign('permissao_fornecedor_empresa',$permissao_fornecedor_empresa);
				$this->smarty_ci->assign('permissao_produto_empresa',$permissao_produto_empresa);
				$this->smarty_ci->assign('permissao_servico_empresa',$permissao_servico_empresa);
				$this->smarty_ci->assign('permissao_centro_custo_empresa',$permissao_centro_custo_empresa);
				$this->smarty_ci->assign('permissao_banco_empresa',$permissao_banco_empresa);
				$this->smarty_ci->display('editar-conta.tpl');
			}else{
				header("Location:" .base_url() . 'inicio');
			}
		}else{
			header("Location:" . base_url() . 'inicio');
		}	
	}

	public function fazerEdicao(){
		$this->load->model('conta_model','conta_model');

		$this->form_validation->set_message('is_unique','O campo %s ja foi cadastrado.');

		$this->form_validation->set_rules('usuario_editar_conta','usuário','max_length[20]|min_length[6]|required|alpha_dash');
		$this->form_validation->set_rules('senha_editar_conta','senha','senha|alpha_dash|required');

		if(!$this->form_validation->run()){
			$this->form_validation->set_error_delimiters("", "");
			
			$this->json_collection->assign('alerta',form_error('usuario_editar_conta'));
			$this->json_collection->assign('alerta',form_error('senha_editar_conta'));
			$this->json_collection->display();

			exit;
		}else{
			$dados = elements(
				array(
					'codigo_conta_editar_conta',
					'usuario_editar_conta',
					'senha_editar_conta',

					'permissao_cadastrar_conta_editar_conta',
					'permissao_visualizar_conta_editar_conta',
					'permissao_editar_conta_editar_conta',
					'permissao_excluir_conta_editar_conta',

					'permissao_cadastrar_cliente_editar_conta',
					'permissao_visualizar_cliente_editar_conta',
					'permissao_editar_cliente_editar_conta',
					'permissao_excluir_cliente_editar_conta',

					'permissao_cadastrar_colaborador_editar_conta',
					'permissao_visualizar_colaborador_editar_conta',
					'permissao_editar_colaborador_editar_conta',
					'permissao_excluir_colaborador_editar_conta',

					'permissao_cadastrar_financeiro_editar_conta',
					'permissao_visualizar_financeiro_editar_conta',
					'permissao_editar_financeiro_editar_conta',
					'permissao_excluir_financeiro_editar_conta',

					'permissao_cadastrar_servico_editar_conta',
					'permissao_visualizar_servico_editar_conta',
					'permissao_editar_servico_editar_conta',
					'permissao_excluir_servico_editar_conta',

					'permissao_cadastrar_produto_editar_conta',
					'permissao_visualizar_produto_editar_conta',
					'permissao_editar_produto_editar_conta',
					'permissao_excluir_produto_editar_conta',

					'permissao_cadastrar_fornecedor_editar_conta',
					'permissao_visualizar_fornecedor_editar_conta',
					'permissao_editar_fornecedor_editar_conta',
					'permissao_excluir_fornecedor_editar_conta',

					'permissao_cadastrar_estoque_editar_conta',
					'permissao_visualizar_estoque_editar_conta',
					'permissao_editar_estoque_editar_conta',
					'permissao_excluir_estoque_editar_conta',

					'permissao_cadastrar_centro_custo_editar_conta',
					'permissao_visualizar_centro_custo_editar_conta',
					'permissao_editar_centro_custo_editar_conta',
					'permissao_excluir_centro_custo_editar_conta',

					'permissao_cadastrar_banco_editar_conta',
					'permissao_visualizar_banco_editar_conta',
					'permissao_editar_banco_editar_conta',
					'permissao_excluir_banco_editar_conta'
				), $this->input->post()
			);

			// verifica se existe uma conta com o mesmo nome de usuario digitado
			$this->db->select('
				codigo_conta,
				usuario_conta
			');

			$this->db->from('conta');
			$this->db->where('codigo_conta !=',(int)$dados['codigo_conta_editar_conta']);
			$this->db->where('usuario_conta',$dados['usuario_editar_conta']);
			$sql_consulta_conta = $this->db->get();

			if($sql_consulta_conta->num_rows !== 0){
				$this->json_collection->assign('alerta','O campo usuário ja foi cadastrado.');
				$this->json_collection->display();
				exit;
			}

			$permissao_cadastrar_conta_editar_conta = $dados['permissao_cadastrar_conta_editar_conta'];
			$permissao_visualizar_conta_editar_conta = $dados['permissao_visualizar_conta_editar_conta'];
			$permissao_editar_conta_editar_conta = $dados['permissao_editar_conta_editar_conta'];
			$permissao_excluir_conta_editar_conta = $dados['permissao_excluir_conta_editar_conta'];

			$permissao_cadastrar_cliente_editar_conta = $dados['permissao_cadastrar_cliente_editar_conta'];
			$permissao_visualizar_cliente_editar_conta = $dados['permissao_visualizar_cliente_editar_conta'];
			$permissao_editar_cliente_editar_conta = $dados['permissao_editar_cliente_editar_conta'];
			$permissao_excluir_cliente_editar_conta = $dados['permissao_excluir_cliente_editar_conta'];

			$permissao_cadastrar_colaborador_editar_conta = $dados['permissao_cadastrar_colaborador_editar_conta'];
			$permissao_visualizar_colaborador_editar_conta = $dados['permissao_visualizar_colaborador_editar_conta'];
			$permissao_editar_colaborador_editar_conta = $dados['permissao_editar_colaborador_editar_conta'];
			$permissao_excluir_colaborador_editar_conta = $dados['permissao_excluir_colaborador_editar_conta'];

			$permissao_cadastrar_financeiro_editar_conta = $dados['permissao_cadastrar_financeiro_editar_conta'];
			$permissao_visualizar_financeiro_editar_conta = $dados['permissao_visualizar_financeiro_editar_conta'];
			$permissao_editar_financeiro_editar_conta = $dados['permissao_editar_financeiro_editar_conta'];
			$permissao_excluir_financeiro_editar_conta = $dados['permissao_excluir_financeiro_editar_conta'];

			$permissao_cadastrar_servico_editar_conta = $dados['permissao_cadastrar_servico_editar_conta'];
			$permissao_visualizar_servico_editar_conta = $dados['permissao_visualizar_servico_editar_conta'];
			$permissao_editar_servico_editar_conta = $dados['permissao_editar_servico_editar_conta'];
			$permissao_excluir_servico_editar_conta = $dados['permissao_excluir_servico_editar_conta'];

			$permissao_cadastrar_produto_editar_conta = $dados['permissao_cadastrar_produto_editar_conta'];
			$permissao_visualizar_produto_editar_conta = $dados['permissao_visualizar_produto_editar_conta'];
			$permissao_editar_produto_editar_conta = $dados['permissao_editar_produto_editar_conta'];
			$permissao_excluir_produto_editar_conta = $dados['permissao_excluir_produto_editar_conta'];

			$permissao_cadastrar_fornecedor_editar_conta = $dados['permissao_cadastrar_fornecedor_editar_conta'];
			$permissao_visualizar_fornecedor_editar_conta = $dados['permissao_visualizar_fornecedor_editar_conta'];
			$permissao_editar_fornecedor_editar_conta = $dados['permissao_editar_fornecedor_editar_conta'];
			$permissao_excluir_fornecedor_editar_conta = $dados['permissao_excluir_fornecedor_editar_conta'];

			$permissao_cadastrar_estoque_editar_conta = $dados['permissao_cadastrar_estoque_editar_conta'];
			$permissao_visualizar_estoque_editar_conta = $dados['permissao_visualizar_estoque_editar_conta'];
			$permissao_editar_estoque_editar_conta = $dados['permissao_editar_estoque_editar_conta'];
			$permissao_excluir_estoque_editar_conta = $dados['permissao_excluir_estoque_editar_conta'];

			$permissao_cadastrar_centro_custo_editar_conta = $dados['permissao_cadastrar_centro_custo_editar_conta'];
			$permissao_visualizar_centro_custo_editar_conta = $dados['permissao_visualizar_centro_custo_editar_conta'];
			$permissao_editar_centro_custo_editar_conta = $dados['permissao_editar_centro_custo_editar_conta'];
			$permissao_excluir_centro_custo_editar_conta = $dados['permissao_excluir_centro_custo_editar_conta'];

			$permissao_cadastrar_banco_editar_conta = $dados['permissao_cadastrar_banco_editar_conta'];
			$permissao_visualizar_banco_editar_conta = $dados['permissao_visualizar_banco_editar_conta'];
			$permissao_editar_banco_editar_conta = $dados['permissao_editar_banco_editar_conta'];
			$permissao_excluir_banco_editar_conta = $dados['permissao_excluir_banco_editar_conta'];

			$permissao_cadastrar_conta_editar_conta = str_replace('true','1',$permissao_cadastrar_conta_editar_conta);
			$permissao_cadastrar_conta_editar_conta = str_replace('false','0',$permissao_cadastrar_conta_editar_conta);

			$permissao_visualizar_conta_editar_conta = str_replace('true','1',$permissao_visualizar_conta_editar_conta);
			$permissao_visualizar_conta_editar_conta = str_replace('false','0',$permissao_visualizar_conta_editar_conta);

			$permissao_editar_conta_editar_conta = str_replace('true','1',$permissao_editar_conta_editar_conta);
			$permissao_editar_conta_editar_conta = str_replace('false','0',$permissao_editar_conta_editar_conta);

			$permissao_excluir_conta_editar_conta = str_replace('true','1',$permissao_excluir_conta_editar_conta);
			$permissao_excluir_conta_editar_conta = str_replace('false','0',$permissao_excluir_conta_editar_conta);

			// ---------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_cliente_editar_conta = str_replace('true','1',$permissao_cadastrar_cliente_editar_conta);
			$permissao_cadastrar_cliente_editar_conta = str_replace('false','0',$permissao_cadastrar_cliente_editar_conta);

			$permissao_visualizar_cliente_editar_conta = str_replace('true','1',$permissao_visualizar_cliente_editar_conta);
			$permissao_visualizar_cliente_editar_conta = str_replace('false','0',$permissao_visualizar_cliente_editar_conta);

			$permissao_editar_cliente_editar_conta = str_replace('true','1',$permissao_editar_cliente_editar_conta);
			$permissao_editar_cliente_editar_conta = str_replace('false','0',$permissao_editar_cliente_editar_conta);

			$permissao_excluir_cliente_editar_conta = str_replace('true','1',$permissao_excluir_cliente_editar_conta);
			$permissao_excluir_cliente_editar_conta = str_replace('false','0',$permissao_excluir_cliente_editar_conta);

			// ----------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_colaborador_editar_conta = str_replace('true','1',$permissao_cadastrar_colaborador_editar_conta);
			$permissao_cadastrar_colaborador_editar_conta = str_replace('false','0',$permissao_cadastrar_colaborador_editar_conta);

			$permissao_visualizar_colaborador_editar_conta = str_replace('true','1',$permissao_visualizar_colaborador_editar_conta);
			$permissao_visualizar_colaborador_editar_conta = str_replace('false','0',$permissao_visualizar_colaborador_editar_conta);

			$permissao_editar_colaborador_editar_conta = str_replace('true','1',$permissao_editar_colaborador_editar_conta);
			$permissao_editar_colaborador_editar_conta = str_replace('false','0',$permissao_editar_colaborador_editar_conta);

			$permissao_excluir_colaborador_editar_conta = str_replace('true','1',$permissao_excluir_colaborador_editar_conta);
			$permissao_excluir_colaborador_editar_conta = str_replace('false','0',$permissao_excluir_colaborador_editar_conta);

			// -----------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_financeiro_editar_conta = str_replace('true','1',$permissao_cadastrar_financeiro_editar_conta);
			$permissao_cadastrar_financeiro_editar_conta = str_replace('false','0',$permissao_cadastrar_financeiro_editar_conta);

			$permissao_visualizar_financeiro_editar_conta = str_replace('true','1',$permissao_visualizar_financeiro_editar_conta);
			$permissao_visualizar_financeiro_editar_conta = str_replace('false','0',$permissao_visualizar_financeiro_editar_conta);

			$permissao_editar_financeiro_editar_conta = str_replace('true','1',$permissao_editar_financeiro_editar_conta);
			$permissao_editar_financeiro_editar_conta = str_replace('false','0',$permissao_editar_financeiro_editar_conta);

			$permissao_excluir_financeiro_editar_conta = str_replace('true','1',$permissao_excluir_financeiro_editar_conta);
			$permissao_excluir_financeiro_editar_conta = str_replace('false','0',$permissao_excluir_financeiro_editar_conta);

			// -------------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_servico_editar_conta = str_replace('true','1',$permissao_cadastrar_servico_editar_conta);
			$permissao_cadastrar_servico_editar_conta = str_replace('false','0',$permissao_cadastrar_servico_editar_conta);

			$permissao_visualizar_servico_editar_conta = str_replace('true','1',$permissao_visualizar_servico_editar_conta);
			$permissao_visualizar_servico_editar_conta = str_replace('false','0',$permissao_visualizar_servico_editar_conta);

			$permissao_editar_servico_editar_conta = str_replace('true','1',$permissao_editar_servico_editar_conta);
			$permissao_editar_servico_editar_conta = str_replace('false','0',$permissao_editar_servico_editar_conta);

			$permissao_excluir_servico_editar_conta = str_replace('true','1',$permissao_excluir_servico_editar_conta);
			$permissao_excluir_servico_editar_conta = str_replace('false','0',$permissao_excluir_servico_editar_conta);

			// ------------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_produto_editar_conta = str_replace('true','1',$permissao_cadastrar_produto_editar_conta);
			$permissao_cadastrar_produto_editar_conta = str_replace('false','0',$permissao_cadastrar_produto_editar_conta);

			$permissao_visualizar_produto_editar_conta = str_replace('true','1',$permissao_visualizar_produto_editar_conta);
			$permissao_visualizar_produto_editar_conta = str_replace('false','0',$permissao_visualizar_produto_editar_conta);

			$permissao_editar_produto_editar_conta = str_replace('true','1',$permissao_editar_produto_editar_conta);
			$permissao_editar_produto_editar_conta = str_replace('false','0',$permissao_editar_produto_editar_conta);

			$permissao_excluir_produto_editar_conta = str_replace('true','1',$permissao_excluir_produto_editar_conta);
			$permissao_excluir_produto_editar_conta = str_replace('false','0',$permissao_excluir_produto_editar_conta);

			// ------------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_fornecedor_editar_conta = str_replace('true','1',$permissao_cadastrar_fornecedor_editar_conta);
			$permissao_cadastrar_fornecedor_editar_conta = str_replace('false','0',$permissao_cadastrar_fornecedor_editar_conta);

			$permissao_visualizar_fornecedor_editar_conta = str_replace('true','1',$permissao_visualizar_fornecedor_editar_conta);
			$permissao_visualizar_fornecedor_editar_conta = str_replace('false','0',$permissao_visualizar_fornecedor_editar_conta);

			$permissao_editar_fornecedor_editar_conta = str_replace('true','1',$permissao_editar_fornecedor_editar_conta);
			$permissao_editar_fornecedor_editar_conta = str_replace('false','0',$permissao_editar_fornecedor_editar_conta);

			$permissao_excluir_fornecedor_editar_conta = str_replace('true','1',$permissao_excluir_fornecedor_editar_conta);
			$permissao_excluir_fornecedor_editar_conta = str_replace('false','0',$permissao_excluir_fornecedor_editar_conta);

			// ------------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_estoque_editar_conta = str_replace('true','1',$permissao_cadastrar_estoque_editar_conta);
			$permissao_cadastrar_estoque_editar_conta = str_replace('false','0',$permissao_cadastrar_estoque_editar_conta);

			$permissao_visualizar_estoque_editar_conta = str_replace('true','1',$permissao_visualizar_estoque_editar_conta);
			$permissao_visualizar_estoque_editar_conta = str_replace('false','0',$permissao_visualizar_estoque_editar_conta);

			$permissao_editar_estoque_editar_conta = str_replace('true','1',$permissao_editar_estoque_editar_conta);
			$permissao_editar_estoque_editar_conta = str_replace('false','0',$permissao_editar_estoque_editar_conta);

			$permissao_excluir_estoque_editar_conta = str_replace('true','1',$permissao_excluir_estoque_editar_conta);
			$permissao_excluir_estoque_editar_conta = str_replace('false','0',$permissao_excluir_estoque_editar_conta);

			// ----------------------------------------------------------------------------------------------------------------


			$permissao_cadastrar_centro_custo_editar_conta = str_replace('true','1',$permissao_cadastrar_centro_custo_editar_conta);
			$permissao_cadastrar_centro_custo_editar_conta = str_replace('false','0',$permissao_cadastrar_centro_custo_editar_conta);

			$permissao_visualizar_centro_custo_editar_conta = str_replace('true','1',$permissao_visualizar_centro_custo_editar_conta);
			$permissao_visualizar_centro_custo_editar_conta = str_replace('false','0',$permissao_visualizar_centro_custo_editar_conta);

			$permissao_editar_centro_custo_editar_conta = str_replace('true','1',$permissao_editar_centro_custo_editar_conta);
			$permissao_editar_centro_custo_editar_conta = str_replace('false','0',$permissao_editar_centro_custo_editar_conta);

			$permissao_excluir_centro_custo_editar_conta = str_replace('true','1',$permissao_excluir_centro_custo_editar_conta);
			$permissao_excluir_centro_custo_editar_conta = str_replace('false','0',$permissao_excluir_centro_custo_editar_conta);

			// ----------------------------------------------------------------------------------------------------------------

			$permissao_cadastrar_banco_editar_conta = str_replace('true','1',$permissao_cadastrar_banco_editar_conta);
			$permissao_cadastrar_banco_editar_conta = str_replace('false','0',$permissao_cadastrar_banco_editar_conta);

			$permissao_visualizar_banco_editar_conta = str_replace('true','1',$permissao_visualizar_banco_editar_conta);
			$permissao_visualizar_banco_editar_conta = str_replace('false','0',$permissao_visualizar_banco_editar_conta);

			$permissao_editar_banco_editar_conta = str_replace('true','1',$permissao_editar_banco_editar_conta);
			$permissao_editar_banco_editar_conta = str_replace('false','0',$permissao_editar_banco_editar_conta);

			$permissao_excluir_banco_editar_conta = str_replace('true','1',$permissao_excluir_banco_editar_conta);
			$permissao_excluir_banco_editar_conta = str_replace('false','0',$permissao_excluir_banco_editar_conta);

			// ----------------------------------------------------------------------------------------------------------------

			$usuario = $dados['usuario_editar_conta'];
			$senha = $dados['senha_editar_conta'];

			$usuario = strtolower($usuario);
			$senha = strtolower($senha);

			$senha = $this->crypt->getEncrypt($senha,$this->config->config['encryption_key']);
			$senha = base64_encode($senha);

			$this->conta_model->setCodigo((int)$dados['codigo_conta_editar_conta']);
			$this->conta_model->setUsuario($usuario);
			$this->conta_model->setSenha($senha);

			$this->conta_model->setPermissaoCadastrarConta($permissao_cadastrar_conta_editar_conta);
			$this->conta_model->setPermissaoEditarConta($permissao_editar_conta_editar_conta);
			$this->conta_model->setPermissaoExcluirConta($permissao_excluir_conta_editar_conta);
			$this->conta_model->setPermissaoVisualizarConta($permissao_visualizar_conta_editar_conta);

			$this->conta_model->setPermissaoCadastrarCliente($permissao_cadastrar_cliente_editar_conta);
			$this->conta_model->setPermissaoEditarCliente($permissao_editar_cliente_editar_conta);
			$this->conta_model->setPermissaoExcluirCliente($permissao_excluir_cliente_editar_conta);
			$this->conta_model->setPermissaoVisualizarCliente($permissao_visualizar_cliente_editar_conta);

			$this->conta_model->setPermissaoCadastrarColaborador($permissao_cadastrar_colaborador_editar_conta);
			$this->conta_model->setPermissaoEditarColaborador($permissao_editar_colaborador_editar_conta);
			$this->conta_model->setPermissaoExcluirColaborador($permissao_excluir_colaborador_editar_conta);
			$this->conta_model->setPermissaoVisualizarColaborador($permissao_visualizar_colaborador_editar_conta);

			$this->conta_model->setPermissaoCadastrarFinanceiro($permissao_cadastrar_financeiro_editar_conta);
			$this->conta_model->setPermissaoEditarFinanceiro($permissao_editar_financeiro_editar_conta);
			$this->conta_model->setPermissaoExcluirFinanceiro($permissao_excluir_financeiro_editar_conta);
			$this->conta_model->setPermissaoVisualizarFinanceiro($permissao_visualizar_financeiro_editar_conta);

			$this->conta_model->setPermissaoCadastrarServico($permissao_cadastrar_servico_editar_conta);
			$this->conta_model->setPermissaoEditarServico($permissao_editar_servico_editar_conta);
			$this->conta_model->setPermissaoExcluirServico($permissao_excluir_servico_editar_conta);
			$this->conta_model->setPermissaoVisualizarServico($permissao_visualizar_servico_editar_conta);

			$this->conta_model->setPermissaoCadastrarProduto($permissao_cadastrar_produto_editar_conta);
			$this->conta_model->setPermissaoEditarProduto($permissao_editar_produto_editar_conta);
			$this->conta_model->setPermissaoExcluirProduto($permissao_excluir_produto_editar_conta);
			$this->conta_model->setPermissaoVisualizarProduto($permissao_visualizar_produto_editar_conta);

			$this->conta_model->setPermissaoCadastrarFornecedor($permissao_cadastrar_fornecedor_editar_conta);
			$this->conta_model->setPermissaoEditarFornecedor($permissao_editar_fornecedor_editar_conta);
			$this->conta_model->setPermissaoExcluirFornecedor($permissao_excluir_fornecedor_editar_conta);
			$this->conta_model->setPermissaoVisualizarFornecedor($permissao_visualizar_fornecedor_editar_conta);

			$this->conta_model->setPermissaoCadastrarEstoque($permissao_cadastrar_estoque_editar_conta);
			$this->conta_model->setPermissaoEditarEstoque($permissao_editar_estoque_editar_conta);
			$this->conta_model->setPermissaoExcluirEstoque($permissao_excluir_estoque_editar_conta);
			$this->conta_model->setPermissaoVisualizarEstoque($permissao_visualizar_estoque_editar_conta);

			$this->conta_model->setPermissaoCadastrarCentroCusto($permissao_cadastrar_centro_custo_editar_conta);
			$this->conta_model->setPermissaoEditarCentroCusto($permissao_editar_centro_custo_editar_conta);
			$this->conta_model->setPermissaoExcluirCentroCusto($permissao_excluir_centro_custo_editar_conta);
			$this->conta_model->setPermissaoVisualizarCentroCusto($permissao_visualizar_centro_custo_editar_conta);

			$this->conta_model->setPermissaoCadastrarBanco($permissao_cadastrar_banco_editar_conta);
			$this->conta_model->setPermissaoEditarBanco($permissao_editar_banco_editar_conta);
			$this->conta_model->setPermissaoExcluirBanco($permissao_excluir_banco_editar_conta);
			$this->conta_model->setPermissaoVisualizarBanco($permissao_visualizar_banco_editar_conta);

			try{
				$sql_conta = $this->conta_model->getEditar();

				if($sql_conta){
					$this->json_collection->assign('sucesso','true');
					$this->json_collection->display();
					exit;
				}else{
					$this->json_collection->assign('alerta','Não foi possível salvar as alterações dessa conta no sistema, por favor tente novamente.');
					$this->json_collection->display();
					exit;
				}
			}catch(Exception $e){
				$this->json_collection->assign('alerta','Não foi possível salvar as alterações dessa conta no sistema, por favor tente novamente. Copie a informação a seguir e entre em contato com o desenvolvedor responsável: '.$e->getMessage());
				$this->json_collection->display();
				exit;
			}
		}
	}

	public function configuracoes(){
		$this->db->select('
			nome_empresa,
			status_empresa,
			permissao_cliente_empresa,
			permissao_colaborador_empresa,
			permissao_conta_empresa,
			permissao_estoque_empresa,
			permissao_financeiro_empresa,
			permissao_fornecedor_empresa,
			permissao_produto_empresa,
			permissao_servico_empresa,
			permissao_centro_custo_empresa,
			permissao_banco_empresa,

			codigo_conta,
			codigo_empresa_conta,
			usuario_conta
		');

		$this->db->from('empresa');
		$this->db->join('conta','conta.codigo_empresa_conta = empresa.codigo_empresa');
		$this->db->where('usuario_conta',$this->session->userdata('usuario_conta'));
		$sql_consulta_tabelas = $this->db->get();

		if($sql_consulta_tabelas->num_rows === 1){
			foreach($sql_consulta_tabelas->result_array() as $dados_consulta_tabelas){
				$nome_empresa = $dados_consulta_tabelas['nome_empresa'];
				$permissao_cliente_empresa = $dados_consulta_tabelas['permissao_cliente_empresa'];
				$permissao_colaborador_empresa = $dados_consulta_tabelas['permissao_colaborador_empresa'];
				$permissao_conta_empresa = $dados_consulta_tabelas['permissao_conta_empresa'];
				$permissao_estoque_empresa = $dados_consulta_tabelas['permissao_estoque_empresa'];
				$permissao_financeiro_empresa = $dados_consulta_tabelas['permissao_financeiro_empresa'];
				$permissao_fornecedor_empresa = $dados_consulta_tabelas['permissao_fornecedor_empresa'];
				$permissao_produto_empresa = $dados_consulta_tabelas['permissao_produto_empresa'];
				$permissao_servico_empresa = $dados_consulta_tabelas['permissao_servico_empresa'];
				$permissao_centro_custo_empresa = $dados_consulta_tabelas['permissao_centro_custo_empresa'];
				$permissao_banco_empresa = $dados_consulta_tabelas['permissao_banco_empresa'];

				$codigo_conta = $dados_consulta_tabelas['codigo_conta'];
			}

			$this->smarty_ci->assign('nome_empresa',$nome_empresa);
			$this->smarty_ci->assign('permissao_cliente_empresa',$permissao_cliente_empresa);
			$this->smarty_ci->assign('permissao_colaborador_empresa',$permissao_colaborador_empresa);
			$this->smarty_ci->assign('permissao_conta_empresa',$permissao_conta_empresa);
			$this->smarty_ci->assign('permissao_estoque_empresa',$permissao_estoque_empresa);
			$this->smarty_ci->assign('permissao_financeiro_empresa',$permissao_financeiro_empresa);
			$this->smarty_ci->assign('permissao_fornecedor_empresa',$permissao_fornecedor_empresa);
			$this->smarty_ci->assign('permissao_produto_empresa',$permissao_produto_empresa);
			$this->smarty_ci->assign('permissao_servico_empresa',$permissao_servico_empresa);
			$this->smarty_ci->assign('permissao_centro_custo_empresa',$permissao_centro_custo_empresa);
			$this->smarty_ci->assign('permissao_banco_empresa',$permissao_banco_empresa);
			$this->smarty_ci->assign('codigo_conta',$codigo_conta);
			$this->smarty_ci->display('configuracoes-conta.tpl');
		}
	}

	public function sair(){
		$this->load->model('conta_model','conta_model');
		$this->conta_model->setUsuario($this->db->escape_str($this->session->userdata('usuario_conta')));
		$this->conta_model->getSair();

		header("Location:".base_url());
	}

	public function excluir($codigo_conta){
		$this->load->model('conta_model','conta_model');

		$this->conta_model->setCodigo((int)$codigo_conta);
		$this->conta_model->getDesativar();
		
		header("Location:" . base_url() . 'conta/listarContas');
	}

	public function instalar(){
		$num_rows = $this->db->count_all_results('empresa');

		if ($num_rows === 0 ){
			$this->load->model('empresa_model','empresa');

			$this->empresa->setNome('Globalnetsis');
			$this->empresa->setStatus('1');
			$this->empresa->setPermissaoCliente('1');
			$this->empresa->setPermissaoColaborador('1');
			$this->empresa->setPermissaoConta('1');
			$this->empresa->setPermissaoEstoque('1');
			$this->empresa->setPermissaoFinanceiro('1');
			$this->empresa->setPermissaoFornecedor('1');
			$this->empresa->setPermissaoProduto('1');
			$this->empresa->setPermissaoServico('1');
		
			try{
				$retorno = $this->empresa->getCadastrar();

				if($retorno){
					$codigo_empresa = $this->db->insert_id();

					$senha = 'gsuper4582';
					$senha = $this->crypt->getEncrypt($senha,$this->config->config['encryption_key']);
					$senha = base64_encode($senha);

					$this->load->model('conta_model','conta');
					$this->conta->setCodigoEmpresa($codigo_empresa);
					$this->conta->setStatus('1');
					$this->conta->setUsuario('gsuper');
					$this->conta->setSenha($senha);
					
					$this->conta->setPermissaoAdministrador('1');
					
					$this->conta->setPermissaoCadastrarCliente('1');
					$this->conta->setPermissaoVisualizarCliente('1');
					$this->conta->setPermissaoEditarCliente('1');
					$this->conta->setPermissaoExcluirCliente('1');
					
					$this->conta->setPermissaoCadastrarColaborador('1');
					$this->conta->setPermissaoVisualizarColaborador('1');
					$this->conta->setPermissaoEditarColaborador('1');
					$this->conta->setPermissaoExcluirColaborador('1');

					$this->conta->setPermissaoCadastrarConta('1');
					$this->conta->setPermissaoVisualizarConta('1');
					$this->conta->setPermissaoEditarConta('1');
					$this->conta->setPermissaoExcluirConta('1');
					
					$this->conta->setPermissaoCadastrarEstoque('1');
					$this->conta->setPermissaoVisualizarEstoque('1');
					$this->conta->setPermissaoEditarEstoque('1');
					$this->conta->setPermissaoExcluirEstoque('1');
					
					$this->conta->setPermissaoCadastrarFinanceiro('1');
					$this->conta->setPermissaoVisualizarFinanceiro('1');
					$this->conta->setPermissaoEditarFinanceiro('1');
					$this->conta->setPermissaoExcluirFinanceiro('1');

					$this->conta->setPermissaoCadastrarFornecedor('1');
					$this->conta->setPermissaoVisualizarFornecedor('1');
					$this->conta->setPermissaoEditarFornecedor('1');
					$this->conta->setPermissaoExcluirFornecedor('1');

					$this->conta->setPermissaoCadastrarProduto('1');
					$this->conta->setPermissaoVisualizarProduto('1');
					$this->conta->setPermissaoEditarProduto('1');
					$this->conta->setPermissaoExcluirProduto('1');
					
					$this->conta->setPermissaoCadastrarServico('1');
					$this->conta->setPermissaoVisualizarServico('1');
					$this->conta->setPermissaoEditarServico('1');
					$this->conta->setPermissaoExcluirServico('1');

					$this->conta->setPermissaoCadastrarCentroCusto('1');
					$this->conta->setPermissaoEditarCentroCusto('1');
					$this->conta->setPermissaoExcluirCentroCusto('1');
					$this->conta->setPermissaoVisualizarCentroCusto('1');

					$this->conta->setPermissaoCadastrarBanco('1');
					$this->conta->setPermissaoEditarBanco('1');
					$this->conta->setPermissaoExcluirBanco('1');
					$this->conta->setPermissaoVisualizarBanco('1');

					
					try{
						$retorno = $this->conta->getCadastrar();

						if($retorno){
							header ('location:' . base_url() );
						}else{
							print("Teste - contaCadastrar - Falhou");
						}
					}catch(Exception $e){
						print($e->getMessage());
					}

				}else{
					print("Teste - empresaCadastrar - Falhou");
				}
			}catch(Exception $e){
				print($e->getMessage());
			}
		}else{
			header('location:' . base_url() );
		}
	}

} // fecha class conta