{include file="cabecalho.tpl" titulo="Administradores"}
{include file="menu-1.tpl"}
{include file="alertas.tpl"}

	<script src="{$base_url}/assets/js/administrador.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Administradores</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="{$base_url}administrador/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">Cadastrar</a>
				</div>
			</div>
		</header>

		<div class="table-responsive">
			<table class='table table-hover table-condensed'>
				<thead>
					<th width="96%">Empresa</th>
					<th width="2%"></th>
					<th width="2%"></th>
				</thead>

				<tbody>
					{if $codigo_empresa !== '0'}
						{section name=i loop=$codigo_empresa}
							<tr>
								<td>{$nome_empresa[i]|capitalize}</td>
								
								<td>
									<a href="{$base_url}administrador/editar/{$codigo_empresa[i]}"><span class="glyphicon glyphicon glyphicon-edit"></span></a>
								</td>

								<td>
									<a href="{$base_url}administrador/excluir/{$codigo_empresa[i]}" class="deletar_administrador"><span class="glyphicon glyphicon-trash"></span></a>
								</td>
							</tr>
						{/section}
					{/if}
				</tbody>
			</table>
		</div>
	</section>
{include file="rodape.tpl"}