<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="menu-principal">
	<div class="container-fluid">  <!-- style="background-color:#E6E6FA;"-->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#ul-menu-principal">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		     </button>

			<a href="{$base_url}inicio" class="navbar-brand" title="início">
				FINXI
				&nbsp;&nbsp;
			</a>
		</div> 

		<div class="collapse navbar-collapse" id="ul-menu-principal">
			<ul class="nav navbar-nav navbar-left">
				<li>
					<li>
						{if $usuario_conta_sessao != ''}
						<a href="{$base_url}imovel" title="Listar / Filtrar">Imóveis</a>
						{else}
						<a href="javascript:history.go(-1);" title="Voltar">Voltar</a>
						{/if}
					</li>
				</li>
			</ul> <!-- menu esquerdo -->

			<ul class="nav navbar-nav navbar-right">
				{if $usuario_conta_sessao != ''}
					<li><a href="{$base_url}conta/sair" title="Sair">Sair</a></li>
				{/if}	
			</ul> <!-- menu direito -->
			
		</div>
	</div> <!-- conteudo do menu -->
</nav>