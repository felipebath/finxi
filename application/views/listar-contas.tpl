{include file="cabecalho.tpl" titulo="Contas"}
{include file="menu-2.tpl"}
{include file="alertas.tpl"}

	<script src="{$base_url}/assets/js/conta.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Contas</h1>
				</div>

				{if $permissao_cadastrar_conta_conta === '1'}
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<a href="{$base_url}conta/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">Cadastrar</a>
					</div>
				{/if}
			</div>
		</header>

		<div class="table-responsive">
			<table class='table table-hover table-condensed'>
				<thead>
					<th width="48%">Usuário</th>
					<th width="48%">Empresa</th>
					<th width="2%"></th>
					<th width="2%"></th>
				</thead>

				<tbody>
					{if $codigo_conta !== '0'}
						{section name=i loop=$codigo_conta}
							<tr>
								<td>{$usuario_conta[i]}</td>
								<td>{$nome_empresa[i]|capitalize}</td>
								
								<td>
									{if $permissao_editar_conta_conta === '1'}
										<a href="{$base_url}conta/editar/{$codigo_conta[i]}"><span class="glyphicon glyphicon glyphicon-edit"></span></a>
									{/if}
								</td>

								<td>
									{if $permissao_excluir_conta_conta === '1'}
										<a href="{$base_url}conta/excluir/{$codigo_conta[i]}" class="deletar_conta"><span class="glyphicon glyphicon-trash"></span></a>
									{/if}
								</td>
							</tr>
						{/section}
					{else}
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					{/if}
				</tbody>
			</table>
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							{$links_paginacao}
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
{include file="rodape.tpl"}