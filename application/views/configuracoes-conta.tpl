{include file="cabecalho.tpl" titulo="Configurações de conta"}
{include file="menu-2.tpl"}

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1>Configurações de conta</h1>
				</div>
			</div>
		</header>

		<div class='row'>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				<ul class='list-group'>
					<li class='list-group-item list-group-divisor'>Empresa</li>
					<li class='list-group-item'>{$nome_empresa|capitalize}</li>

					<li class='list-group-item list-group-divisor'>Conta</li>
					<li class='list-group-item'><a href="{$base_url}conta/editar/{$codigo_conta}">Editar minha conta</a></li>
					
					<li class='list-group-item list-group-divisor'>Módulos</li>

					{if $permissao_cliente_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Cliente</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Cliente</li>
					{/if}

					{if $permissao_conta_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Conta</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Conta</li>
					{/if}

					{if $permissao_financeiro_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Financeiro</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Financeiro</li>
					{/if}

					{if $permissao_estoque_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Estoque</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Estoque</li>
					{/if}

					{if $permissao_colaborador_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Colaborador</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Colaborador</li>
					{/if}

					{if $permissao_produto_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Produto</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Produto</li>
					{/if}

					{if $permissao_servico_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Serviço</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Serviço</li>
					{/if}

					{if $permissao_fornecedor_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Fornecedor</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Fornecedor</li>
					{/if}

					{if $permissao_centro_custo_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Centro de Custo</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Centro de Custo</li>
					{/if}

					{if $permissao_banco_empresa === '1'}
						<li class='list-group-item'><span class='glyphicon glyphicon-ok'></span> Banco</li>
					{else}
						<li class='list-group-item'><span class='glyphicon glyphicon-remove'></span> Banco</li>
					{/if}
				</ul>
			</div>
		</div>
	
	</section>
{include file="rodape.tpl"}