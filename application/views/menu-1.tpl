<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<a href="{$base_url}" class="navbar-brand" title="Finxi">Finxi</a>
		</div> <!-- cabecalho do menu -->

		<ul class="nav navbar-nav navbar-right">
			<li><a href="{$base_url}conta/sair" title="Sair">Sair</a></li>
		</ul> <!-- menu direito -->
	</div> <!-- conteudo do menu -->
</nav>