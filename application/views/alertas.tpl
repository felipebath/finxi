<section class="modal fade" id="alerta">
  <div class="modal-dialog">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="alerta_titulo"></h4>
      </header>

      <div class="modal-body" id="alerta_conteudo"></div>
    </div>
  </div>
</section>

<section class="modal fade" id="alerta_confirmacao_redirecionamento">
  <div class="modal-dialog">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="alerta_confirmacao_redirecionamento_titulo"></h4>
      </header>

      <div class='modal-body' id='alerta_confirmacao_redirecionamento_conteudo'></div>

      <footer class="modal-footer">
      		<a href='#' class='btn btn-primary' id="alerta_confirmacao_redirecionamento_url_redirecionamento" title='Sim'>Sim</a>
      		<a href='#' class='btn' id="alerta_confirmacao_redirecionamento_url_desistencia" title='Não'>Não</a>
      </footer>
    </div>
  </div>
</section>