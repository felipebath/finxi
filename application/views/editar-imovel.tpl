{include file="cabecalho.tpl" titulo="Editar Imóvel"}
{include file="menu-2.tpl"}
{include file="alertas.tpl"}

<script src="{$base_url}/assets/js/imovel.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando novo Imóvel</h1>
				</div>

				{if $usuario_conta_sessao != ''}
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="{$base_url}imovel" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
				{/if}
			</div>
		</header>

		<form action="{$base_url}imovel/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="{$id}" id="id">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes" value="{$nome}">
                    		</div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Valor</label>
                                <input type="text" placeholder="Valor" id="valor" class="form-control" value="{$valor}">
                            </div>
                    	</div>

                    	<br>
                    	
                    	<div class="row">	
                       		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        	    <label>Descrição</label>
                                <input type="text" placeholder="Descrição" class="form-control" id="descricao" value="{$descricao}">
	                        </div>
                        		
                        	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                           	    <label>Endereço</label>
	                            <input type="text" placeholder="Endereço" class="form-control" id="endereco" value="{$endereco}">
	                        </div>

	                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Cep</label>
	                            <input type="text" placeholder="Cep" class="form-control" id="cep" maxlength="10" value="{$cep}">
	                        </div>
	                    </div>
	                        
	                    <br>

	                    <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                              <label>Imagem</label>
                              <img src="{$imagem}" width="400" style="border: 1px solid gray">
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                             	  <label></label>  
                                <input type="text" placeholder="Imagem" class="form-control" id="imagem" value="{$imagem}">
                            </div>
                        </div>                                                            
					</div>
				</div>
			<br>
			<br>
			{if $usuario_conta_sessao != ''}
			{include file="botoes-submit.tpl"}
			{/if}
		</form>
	</section>
{include file="rodape.tpl"}