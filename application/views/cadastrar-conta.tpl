{include file="cabecalho.tpl" titulo="Cadastrar conta"}
{include file="menu-2.tpl"}
{include file="alertas.tpl"}

	<script src="{$base_url}/assets/js/conta.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando conta</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="{$base_url}conta/listarContas" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<nav>
			<ul class="nav nav-tabs">
				<li class="active"><a href="#entrada" title="Entrada" data-toggle="tab">Entrada</a></li>
				<li><a href="#permissoes" title="Permissões" data-toggle="tab">Permissões</a></li>
			</ul>
		</nav>

		<form action="{$base_url}conta/fazerCadastro" method="post" id="formulario_cadastrar_conta">

		<div class="tab-content">
			<div class="tab-pane active" id="entrada">
				<br>
				
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<input type="hidden" value="{$codigo_empresa}" id="codigo_empresa_cadastrar_conta">

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Usuário *</label>
								<input type="text" id="usuario_cadastrar_conta" placeholder="Digite seu nome de usuário" class="form-control" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_usuario_cadastrar_conta"></span>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Senha *</label>
								<input type="password" id="senha_cadastrar_conta" placeholder="Digite sua senha" class="form-control" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_senha_cadastrar_conta"></span>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- entrada -->

			<div class="tab-pane" id="permissoes">
				<br>

				<input type="checkbox" id="selecionar_todos_permissao_cadastrar_conta">
				<span>Selecionar todos</span><br>

				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

						{if $permissao_conta_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Conta</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_conta" id="permissao_cadastrar_conta_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_conta" id="permissao_editar_conta_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_conta" id="permissao_excluir_conta_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_conta" id="permissao_visualizar_conta_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- conta -->
						{/if}

						<br>

						{if $permissao_cliente_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<span>Cliente</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_cliente" id="permissao_cadastrar_cliente_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_cliente" id="permissao_editar_cliente_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_cliente" id="permissao_excluir_cliente_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_cliente" id="permissao_visualizar_cliente_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- cliente -->
						{/if}

						<br>

						{if $permissao_colaborador_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Colaborador</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_colaborador" id="permissao_cadastrar_colaborador_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_colaborador" id="permissao_editar_colaborador_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_colaborador" id="permissao_excluir_colaborador_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_colaborador" id="permissao_visualizar_colaborador_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- colaborador -->
						{/if}

						<br>

						{if $permissao_financeiro_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Finânceiro</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_financeiro" id="permissao_cadastrar_financeiro_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_financeiro" id="permissao_editar_financeiro_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_financeiro" id="permissao_excluir_financeiro_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_financeiro" id="permissao_visualizar_financeiro_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- financeiro -->
						{/if}

						<br>

						{if $permissao_servico_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Serviço</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_servico" id="permissao_cadastrar_servico_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_servico" id="permissao_editar_servico_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_servico" id="permissao_excluir_servico_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_servico" id="permissao_visualizar_servico_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- servico -->
						{/if}

						<br>

						{if $permissao_produto_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Produto</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_produto" id="permissao_cadastrar_produto_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_produto" id="permissao_editar_produto_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_produto" id="permissao_excluir_produto_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_produto" id="permissao_visualizar_produto_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- produto -->
						{/if}

						<br>

						{if $permissao_fornecedor_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Fornecedor</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_fornecedor" id="permissao_cadastrar_fornecedor_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_fornecedor" id="permissao_editar_fornecedor_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_fornecedor" id="permissao_excluir_fornecedor_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_fornecedor" id="permissao_visualizar_fornecedor_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- fornecedor -->
						{/if}

						<br>

						{if $permissao_estoque_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Estoque</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_estoque" id="permissao_cadastrar_estoque_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_estoque" id="permissao_editar_estoque_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_estoque" id="permissao_excluir_estoque_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_estoque" id="permissao_visualizar_estoque_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- estoque -->
						{/if}

						<br>

						{if $permissao_centro_custo_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Centro de custo</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_centro_custo" id="permissao_cadastrar_centro_custo_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_centro_custo" id="permissao_editar_centro_custo_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_centro_custo" id="permissao_excluir_centro_custo_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_centro_custo" id="permissao_visualizar_centro_custo_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- centro de custo -->
						{/if}

						<br>

						{if $permissao_banco_empresa === '1'}
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Banco</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="checkbox" name="permissao_cadastrar_banco" id="permissao_cadastrar_banco_cadastrar_conta">
											<span>Cadastrar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_editar_banco" id="permissao_editar_banco_cadastrar_conta">
											<span>Editar</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_excluir_banco" id="permissao_excluir_banco_cadastrar_conta">
											<span>Excluir</span>
										&nbsp;&nbsp;
											<input type="checkbox" name="permissao_visualizar_banco" id="permissao_visualizar_banco_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- estoque -->
						{/if}

					</div>
				</div>
			</div> <!-- permissoes -->
		</div>

		<br>
		<br>
		<input type="submit" value="&nbsp;&nbsp;Salvar&nbsp;&nbsp;" class="btn btn-primary">
		&nbsp;&nbsp;
		<a href="javascript:history.go(-1)" class="btn btn-primary">Cancelar</a>

		</form>
	</section>
{include file="rodape.tpl"}