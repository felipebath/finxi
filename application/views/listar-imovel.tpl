{include file="cabecalho.tpl" titulo="Imóvel"}
{include file="menu-2.tpl"}
{include file="alertas.tpl"}

	<script src="{$base_url}/assets/js/imovel.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Imóvel</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					{if $usuario_conta_sessao != ''}
					<a href="{$base_url}imovel/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">
						Novo
					</a>
					{/if}
				</div>
			</div>
		</header>

		<div class="table-responsive">
			<form method="get" action="{$base_url}imovel/filtrar">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
								<input type="search" name="valor_filtro" class="form-control" placeholder="Digite o endereço e pressione enter." autofocus="yes" required>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<input type="search" name="raio_filtro" class="form-control" placeholder="Digite a distância" maxlength="5">
							</div>

							<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
								<input type='submit' value='Filtrar' class='pull-right btn btn-primary'>
							</div>
						</div>
					</div>
				</div>	
			</form>
			
			<br>
		
			<div class="table-responsive">
				<table class='table table-hover table-striped'>
					<thead>
						<th>Id</th>
						<th>Nome</th>
						<th>Endereço</th>
						<th>Valor</th>
						<th>Editar</th>
						<th>Ação</th>
					</thead>

					<tbody>
						{if $id !== '0'}
							{section name=i loop=$id}
								<tr>
									<td>{$id[i]}</td>
									
									<td>{$nome[i]|capitalize}</td>
									
									<td>{$endereco[i]}</td>

									<td>{$valor[i]}</td>

									<td>
										<a href="{$base_url}imovel/editar/{$id[i]}">
											<span class="glyphicon glyphicon glyphicon-edit"></span>
										</a>
									</td>
									
									<td>
										<a href="{$base_url}imovel/inativar/{$id[i]}" class="inativar_item">
											<span class="glyphicon glyphicon-trash"></span>
										</a>
									</td>
								</tr>
							{/section}
						{/if}
					</tbody>
				</table>
				
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							{$links_paginacao}
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
{include file="rodape.tpl"}