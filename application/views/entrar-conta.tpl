{include file="cabecalho.tpl" titulo="Entrar"}
{include file="alertas.tpl"}

	<script src="{$base_url}/assets/js/conta.js" defer></script>

	<section class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-offset-4">
				<div class="panel panel-default">
					
					<header class="panel-heading">
						<h1 class="panel-title">Pesquisar</h1>
					</header>

					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<form action="{$base_url}imovel/filtrar" method="get">
									<label></label>
									<input type="text" name="valor_filtro" placeholder="Digite o endereço a ser pesquisado" class="form-control" autofocus="yes" maxlength="100">
							</div>
						</div>

						<br>			

						<div class="row">
							<div class='col-xs-12 col-sm-5 col-md-5 col-lg-5'>
								<input type="search" name="raio_filtro" class="form-control" placeholder="Digite a distância" maxlength="5">
							</div>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<input type="submit" class="btn btn-primary" value="Pesquisar">
								</form>
							</div>
						</div>
					</div>

					<header class="panel-heading">
						<h1 class="panel-title">Login</h1>
					</header>

					<div class="panel-body">
						<form action="{$base_url}conta/entrar" method="post" id="formulario_entrar_conta">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label>Usuário</label>
									<input type="text" id="usuario_entrar_conta" placeholder="Digite seu nome de usuário" class="form-control" autofocus="yes" autocomplete="yes" maxlength="">
									<span id="alerta_usuario_entrar_conta" class="alerta_formulario"></span>
								</div>
							</div>

							<br>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label>Senha</label>
									<input type="password" id="senha_entrar_conta" placeholder="Digite sua senha" class="form-control" autocomplete="yes">
									<span id="alerta_senha_entrar_conta" class="alerta_formulario"></span>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<br>
									<input type="submit" class="btn btn-primary" value="Entrar">
								</div>
							</div>
						</form>		
					</div>
				</div>
			</div>
		</div>
	</section>
{include file="rodape.tpl"}