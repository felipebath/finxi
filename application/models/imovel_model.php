<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Imovel_model extends CI_Model{

	private $id;
	private $nome;
	private $descricao;
	private $endereco;
	private $cep;
	private $imagem;
	private $valor;
	private $latitude;
	private $longitude;
	private $codigo_empresa;	
	
	public function __construct(){
		parent:: __construct();
	}
	
	public function setId($id)
	{
	    $this->id = $id;
	}
	
	public function getId()
	{
	    return $this->id;
	}

	public function setNome($nome)
	{
	    $this->nome = $nome;
	}
	
	public function getNome()
	{
	    return $this->nome;
	}

	public function setDescricao($descricao)
	{
	    $this->descricao = $descricao;
	}
	
	public function getDescricao()
	{
	    return $this->descricao;
	}

	public function setEndereco($endereco)
	{
	    $this->endereco = $endereco;
	}
	
	public function getEndereco()
	{
	    return $this->endereco;
	}

	public function setCep($cep)
	{
	    $this->cep = $cep;
	}
	
	public function getCep()
	{
	    return $this->cep;
	}

	public function setImagem($imagem)
	{
	    $this->imagem = $imagem;
	}
	
	public function getImagem()
	{
	    return $this->imagem;
	}

	public function setValor($valor)
	{
	    $this->valor = $valor;
	}
	
	public function getValor()
	{
	    return $this->valor;
	}

	public function setLatitude($latitude)
	{
	    $this->latitude = $latitude;
	}
	
	public function getLatitude()
	{
	    return $this->latitude;
	}

	public function setLongitude($longitude)
	{
	    $this->longitude = $longitude;
	}
	
	public function getLongitude()
	{
	    return $this->longitude;
	}

	public function setCodigoEmpresa($codigo_empresa)
	{
	    $this->codigo_empresa = $codigo_empresa;
	}
	
	public function getCodigoEmpresa()
	{
	    return $this->codigo_empresa;
	}
	
	public function getCadastrar(){
		return $this->cadastro();
	}

	public function cadastro(){
		$dados = array(
				'nome'					=> $this->getNome(),
				'descricao' 			=> $this->getDescricao(),
				'endereco' 				=> $this->getEndereco(),
				'cep' 					=> $this->getCep(),
				'latitude' 				=> $this->getLatitude(),
				'longitude' 			=> $this->getLongitude(),
				'imagem' 				=> $this->getImagem(),
				'valor' 				=> $this->getValor(),
				'codigo_empresa' 		=> $this->getCodigoEmpresa()
			);
		$sql = $this->db->insert('imovel',$dados);

		if ($sql) {
			return true;
		} else {
			return false;
		}		
	}

	public function getEditar(){
		return $this->editar();
	}

	public function editar(){
		$dados = array(
				'id_imovel' 			=> $this->getId(),
				'nome'					=> $this->getNome(),
				'descricao' 			=> $this->getDescricao(),
				'endereco' 				=> $this->getEndereco(),
				'cep' 					=> $this->getCep(),
				'latitude' 				=> $this->getLatitude(),
				'longitude' 			=> $this->getLongitude(),
				'imagem' 				=> $this->getImagem(),
				'valor' 				=> $this->getValor(),
				'codigo_empresa' 		=> $this->getCodigoEmpresa()			
		 	);

		$this->db->where('id_imovel',$this->getId());
		$sql = $this->db->update('imovel',$dados);

		if ($sql) {
			return true;
		} else {
			return false;
		}		
	}

	public function getInativar(){
		return $this->inativar();
	}

	private function inativar(){
		$dados = array(
			'id_imovel'=>$this->getId()
		);

		$this->db->where('id_imovel', $this->getId());

		$sql = $this->db->delete('imovel',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}
}