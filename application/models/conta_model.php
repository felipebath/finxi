<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Conta_model extends CI_Model{
	private $codigo;
	private $codigo_empresa;
	private $status;
	private $usuario;
	private $senha;
	private $permissao_administrador;
	
	private $permissao_cadastrar_cliente;
	private $permissao_visualizar_cliente;
	private $permissao_editar_cliente;
	private $permissao_excluir_cliente;

	private $permissao_cadastrar_colaborador;
	private $permissao_visualizar_colaborador;
	private $permissao_editar_colaborador;
	private $permissao_excluir_colaborador;

	private $permissao_cadastrar_conta;
	private $permissao_visualizar_conta;
	private $permissao_editar_conta;
	private $permissao_excluir_conta;

	private $permissao_cadastrar_estoque;
	private $permissao_visualizar_estoque;
	private $permissao_editar_estoque;
	private $permissao_excluir_estoque;
	
	private $permissao_cadastrar_financeiro;
	private $permissao_visualizar_financeiro;
	private $permissao_editar_financeiro;
	private $permissao_excluir_financeiro;
	
	private $permissao_cadastrar_fornecedor;
	private $permissao_visualizar_fornecedor;
	private $permissao_editar_fornecedor;
	private $permissao_excluir_fornecedor;
	
	private $permissao_cadastrar_produto;
	private $permissao_visualizar_produto;
	private $permissao_editar_produto;
	private $permissao_excluir_produto;
	
	private $permissao_cadastrar_servico;
	private $permissao_visualizar_servico;
	private $permissao_editar_servico;
	private $permissao_excluir_servico;

	private $permissao_cadastrar_centro_custo;
	private $permissao_visualizar_centro_custo;
	private $permissao_editar_centro_custo;
	private $permissao_excluir_centro_custo;
	
	private $permissao_cadastrar_banco;
	private $permissao_visualizar_banco;
	private $permissao_editar_banco;
	private $permissao_excluir_banco;

	public function __construct(){
		parent::__construct();

		$this->load->database(); 
		$this->load->library('session');
	}

	public function setCodigo($codigo){
		$this->codigo = $codigo;
	}

	public function setCodigoEmpresa($codigo_empresa){
		$this->codigo_empresa = $codigo_empresa;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}

	public function setSenha($senha){
		$this->senha = $senha;
	}

	public function setPermissaoAdministrador($permissao_administrador){
		$this->permissao_administrador = $permissao_administrador;
	}
	
    // cliente
	public function setPermissaoCadastrarCliente($permissao_cadastrar_cliente){
		$this->permissao_cadastrar_cliente = $permissao_cadastrar_cliente;
	}

	public function setPermissaoVisualizarCliente($permissao_visualizar_cliente){
		$this->permissao_visualizar_cliente = $permissao_visualizar_cliente;
	}

	public function setPermissaoEditarCliente($permissao_editar_cliente){
		$this->permissao_editar_cliente = $permissao_editar_cliente;
	}

	public function setPermissaoExcluirCliente($permissao_excluir_cliente){
		$this->permissao_excluir_cliente = $permissao_excluir_cliente;
	}


    // colaborador
 	public function setPermissaoCadastrarColaborador($permissao_cadastrar_colaborador){
		$this->permissao_cadastrar_colaborador = $permissao_cadastrar_colaborador;
	}

	public function setPermissaoVisualizarColaborador($permissao_visualizar_colaborador){
		$this->permissao_visualizar_colaborador = $permissao_visualizar_colaborador;
	}

	public function setPermissaoEditarColaborador($permissao_editar_colaborador){
		$this->permissao_editar_colaborador = $permissao_editar_colaborador;
	}

	public function setPermissaoExcluirColaborador($permissao_excluir_colaborador){
		$this->permissao_excluir_colaborador = $permissao_excluir_colaborador;
	}


	// conta
 	public function setPermissaoCadastrarConta($permissao_cadastrar_conta){
		$this->permissao_cadastrar_conta = $permissao_cadastrar_conta;
	}

	public function setPermissaoVisualizarConta($permissao_visualizar_conta){
		$this->permissao_visualizar_conta = $permissao_visualizar_conta;
	}

	public function setPermissaoEditarConta($permissao_editar_conta){
		$this->permissao_editar_conta = $permissao_editar_conta;
	}

	public function setPermissaoExcluirConta($permissao_excluir_conta){
		$this->permissao_excluir_conta = $permissao_excluir_conta;
	}


	// estoque
 	public function setPermissaoCadastrarEstoque($permissao_cadastrar_estoque){
		$this->permissao_cadastrar_estoque = $permissao_cadastrar_estoque;
	}

	public function setPermissaoVisualizarEstoque($permissao_visualizar_estoque){
		$this->permissao_visualizar_estoque = $permissao_visualizar_estoque;
	}

	public function setPermissaoEditarEstoque($permissao_editar_estoque){
		$this->permissao_editar_estoque = $permissao_editar_estoque;
	}

	public function setPermissaoExcluirEstoque($permissao_excluir_estoque){
		$this->permissao_excluir_estoque = $permissao_excluir_estoque;
	}


	// financeiro
 	public function setPermissaoCadastrarFinanceiro($permissao_cadastrar_financeiro){
		$this->permissao_cadastrar_financeiro = $permissao_cadastrar_financeiro;
	}

	public function setPermissaoVisualizarFinanceiro($permissao_visualizar_financeiro){
		$this->permissao_visualizar_financeiro = $permissao_visualizar_financeiro;
	}

	public function setPermissaoEditarFinanceiro($permissao_editar_financeiro){
		$this->permissao_editar_financeiro = $permissao_editar_financeiro;
	}

	public function setPermissaoExcluirFinanceiro($permissao_excluir_financeiro){
		$this->permissao_excluir_financeiro = $permissao_excluir_financeiro;
	}


	// fornecedor
 	public function setPermissaoCadastrarFornecedor($permissao_cadastrar_fornecedor){
		$this->permissao_cadastrar_fornecedor = $permissao_cadastrar_fornecedor;
	}

	public function setPermissaoVisualizarFornecedor($permissao_visualizar_fornecedor){
		$this->permissao_visualizar_fornecedor = $permissao_visualizar_fornecedor;
	}

	public function setPermissaoEditarFornecedor($permissao_editar_fornecedor){
		$this->permissao_editar_fornecedor = $permissao_editar_fornecedor;
	}

	public function setPermissaoExcluirFornecedor($permissao_excluir_fornecedor){
		$this->permissao_excluir_fornecedor = $permissao_excluir_fornecedor;
	}


	// produto
 	public function setPermissaoCadastrarProduto($permissao_cadastrar_produto){
		$this->permissao_cadastrar_produto = $permissao_cadastrar_produto;
	}

	public function setPermissaoVisualizarProduto($permissao_visualizar_produto){
		$this->permissao_visualizar_produto = $permissao_visualizar_produto;
	}

	public function setPermissaoEditarProduto($permissao_editar_produto){
		$this->permissao_editar_produto = $permissao_editar_produto;
	}

	public function setPermissaoExcluirProduto($permissao_excluir_produto){
		$this->permissao_excluir_produto = $permissao_excluir_produto;
	}


	// servico
 	public function setPermissaoCadastrarServico($permissao_cadastrar_servico){
		$this->permissao_cadastrar_servico = $permissao_cadastrar_servico;
	}

	public function setPermissaoVisualizarServico($permissao_visualizar_servico){
		$this->permissao_visualizar_servico = $permissao_visualizar_servico;
	}

	public function setPermissaoEditarServico($permissao_editar_servico){
		$this->permissao_editar_servico = $permissao_editar_servico;
	}

	public function setPermissaoExcluirServico($permissao_excluir_servico){
		$this->permissao_excluir_servico = $permissao_excluir_servico;
	}

	//centro de custo
	public function setPermissaoCadastrarCentroCusto($permissao_cadastrar_centro_custo){
		$this->permissao_cadastrar_centro_custo = $permissao_cadastrar_centro_custo;
	}

	public function setPermissaoVisualizarCentroCusto($permissao_visualizar_centro_custo){
		$this->permissao_visualizar_centro_custo = $permissao_visualizar_centro_custo;
	}

	public function setPermissaoEditarCentroCusto($permissao_editar_centro_custo){
		$this->permissao_editar_centro_custo = $permissao_editar_centro_custo;
	}

	public function setPermissaoExcluirCentroCusto($permissao_excluir_centro_custo){
		$this->permissao_excluir_centro_custo = $permissao_excluir_centro_custo;
	}

	//banco
	public function setPermissaoCadastrarBanco($permissao_cadastrar_banco){
		$this->permissao_cadastrar_banco = $permissao_cadastrar_banco;
	}

	public function setPermissaoVisualizarBanco($permissao_visualizar_banco){
		$this->permissao_visualizar_banco = $permissao_visualizar_banco;
	}

	public function setPermissaoEditarBanco($permissao_editar_banco){
		$this->permissao_editar_banco = $permissao_editar_banco;
	}

	public function setPermissaoExcluirBanco($permissao_excluir_banco){
		$this->permissao_excluir_banco = $permissao_excluir_banco;
	}

	public function getCodigo(){
		return $this->codigo;
	}

	public function getCodigoEmpresa(){
		return $this->codigo_empresa;
	}

	public function getStatus(){
		return $this->status;
	}

	public function getUsuario(){
		return $this->usuario;
	}

	public function getSenha(){
		return $this->senha;
	}

	public function getPermissaoAdministrador(){
		return $this->permissao_administrador;
	}

	// cliente
	public function getPermissaoCadastrarCliente(){
		return $this->permissao_cadastrar_cliente;
	}

	public function getPermissaoVisualizarCliente(){
		return $this->permissao_visualizar_cliente;
	}

	public function getPermissaoEditarCliente(){
		return $this->permissao_editar_cliente;
	}

	public function getPermissaoExcluirCliente(){
		return $this->permissao_excluir_cliente;
	}

	// colaborador
	public function getPermissaoCadastrarColaborador(){
		return $this->permissao_cadastrar_colaborador;
	}

	public function getPermissaoVisualizarColaborador(){
		return $this->permissao_visualizar_colaborador;
	}

	public function getPermissaoEditarColaborador(){
		return $this->permissao_editar_colaborador;
	}

	public function getPermissaoExcluirColaborador(){
		return $this->permissao_excluir_colaborador;
	}

	// conta
	public function getPermissaoCadastrarConta(){
		return $this->permissao_cadastrar_conta;
	}

	public function getPermissaoVisualizarConta(){
		return $this->permissao_visualizar_conta;
	}

	public function getPermissaoEditarConta(){
		return $this->permissao_editar_conta;
	}

	public function getPermissaoExcluirConta(){
		return $this->permissao_excluir_conta;
	}

	// estoque
	public function getPermissaoCadastrarEstoque(){
		return $this->permissao_cadastrar_estoque;
	}

	public function getPermissaoVisualizarEstoque(){
		return $this->permissao_visualizar_estoque;
	}

	public function getPermissaoEditarEstoque(){
		return $this->permissao_editar_estoque;
	}

	public function getPermissaoExcluirEstoque(){
		return $this->permissao_excluir_estoque;
	}

	// financeiro
	public function getPermissaoCadastrarFinanceiro(){
		return $this->permissao_cadastrar_financeiro;
	}

	public function getPermissaoVisualizarFinanceiro(){
		return $this->permissao_visualizar_financeiro;
	}

	public function getPermissaoEditarFinanceiro(){
		return $this->permissao_editar_financeiro;
	}

	public function getPermissaoExcluirFinanceiro(){
		return $this->permissao_excluir_financeiro;
	}

	// fornecedor
	public function getPermissaoCadastrarFornecedor(){
		return $this->permissao_cadastrar_fornecedor;
	}

	public function getPermissaoVisualizarFornecedor(){
		return $this->permissao_visualizar_fornecedor;
	}

	public function getPermissaoEditarFornecedor(){
		return $this->permissao_editar_fornecedor;
	}

	public function getPermissaoExcluirFornecedor(){
		return $this->permissao_excluir_fornecedor;
	}

	// produto
	public function getPermissaoCadastrarProduto(){
		return $this->permissao_cadastrar_produto;
	}

	public function getPermissaoVisualizarProduto(){
		return $this->permissao_visualizar_produto;
	}

	public function getPermissaoEditarProduto(){
		return $this->permissao_editar_produto;
	}

	public function getPermissaoExcluirProduto(){
		return $this->permissao_excluir_produto;
	}

	// servico
	public function getPermissaoCadastrarServico(){
		return $this->permissao_cadastrar_servico;
	}

	public function getPermissaoVisualizarServico(){
		return $this->permissao_visualizar_servico;
	}

	public function getPermissaoEditarServico(){
		return $this->permissao_editar_servico;
	}

	public function getPermissaoExcluirServico(){
		return $this->permissao_excluir_servico;
	}

	//centro de custo
	public function getPermissaoCadastrarCentroCusto(){
		return $this->permissao_cadastrar_centro_custo;
	}

	public function getPermissaoVisualizarCentroCusto(){
		return $this->permissao_visualizar_centro_custo;
	}

	public function getPermissaoEditarCentroCusto(){
		return $this->permissao_editar_centro_custo;
	}

	public function getPermissaoExcluirCentroCusto(){
		return $this->permissao_excluir_centro_custo;
	}

	//banco
	public function getPermissaoCadastrarBanco(){
		return $this->permissao_cadastrar_banco;
	}

	public function getPermissaoVisualizarBanco(){
		return $this->permissao_visualizar_banco;
	}

	public function getPermissaoEditarBanco(){
		return $this->permissao_editar_banco;
	}

	public function getPermissaoExcluirBanco(){
		return $this->permissao_excluir_banco;
	}


	public function getCadastrar(){
		return $this->cadastrar();
	}

	public function getEditar(){
		return $this->editar();
	}

	public function getAtivar(){
		return $this->ativar();
	}

	public function getDesativar(){
		return $this->desativar();
	}
	
	public function getEntrar(){
		return $this->entrar();
	}
	
	public function getValidarSessao(){
		return $this->validarSessao();
	}

	public function getSair(){
		return $this->sair();
	}

	private function cadastrar(){
		$dados = array(
			'codigo_empresa_conta'=>$this->getCodigoEmpresa(),
			'status_conta'=>'1',
			'usuario_conta'=>$this->getUsuario(),
			'senha_conta'=>$this->getSenha(),
			'permissao_administrador_conta'=>$this->getPermissaoAdministrador(),
			
			'permissao_cadastrar_cliente_conta'=>$this->getPermissaoCadastrarCliente(),
			'permissao_visualizar_cliente_conta'=>$this->getPermissaoVisualizarCliente(),
			'permissao_editar_cliente_conta'=>$this->getPermissaoEditarCliente(),
			'permissao_excluir_cliente_conta'=>$this->getPermissaoExcluirCliente(),

			'permissao_cadastrar_colaborador_conta'=>$this->getPermissaoCadastrarColaborador(),
			'permissao_visualizar_colaborador_conta'=>$this->getPermissaoVisualizarColaborador(),
			'permissao_editar_colaborador_conta'=>$this->getPermissaoEditarColaborador(),
			'permissao_excluir_colaborador_conta'=>$this->getPermissaoExcluirColaborador(),

			'permissao_cadastrar_conta_conta'=>$this->getPermissaoCadastrarConta(),
			'permissao_visualizar_conta_conta'=>$this->getPermissaoVisualizarConta(),
			'permissao_editar_conta_conta'=>$this->getPermissaoEditarConta(),
			'permissao_excluir_conta_conta'=>$this->getPermissaoExcluirConta(),

			'permissao_cadastrar_estoque_conta'=>$this->getPermissaoCadastrarEstoque(),
			'permissao_visualizar_estoque_conta'=>$this->getPermissaoVisualizarEstoque(),
			'permissao_editar_estoque_conta'=>$this->getPermissaoEditarEstoque(),
			'permissao_excluir_estoque_conta'=>$this->getPermissaoExcluirEstoque(),

			'permissao_cadastrar_financeiro_conta'=>$this->getPermissaoCadastrarFinanceiro(),
			'permissao_visualizar_financeiro_conta'=>$this->getPermissaoVisualizarFinanceiro(),
			'permissao_editar_financeiro_conta'=>$this->getPermissaoEditarFinanceiro(),
			'permissao_excluir_financeiro_conta'=>$this->getPermissaoExcluirFinanceiro(),

			'permissao_cadastrar_fornecedor_conta'=>$this->getPermissaoCadastrarFornecedor(),
			'permissao_visualizar_fornecedor_conta'=>$this->getPermissaoVisualizarFornecedor(),
			'permissao_editar_fornecedor_conta'=>$this->getPermissaoEditarFornecedor(),
			'permissao_excluir_fornecedor_conta'=>$this->getPermissaoExcluirFornecedor(),

			'permissao_cadastrar_produto_conta'=>$this->getPermissaoCadastrarProduto(),
			'permissao_visualizar_produto_conta'=>$this->getPermissaoVisualizarProduto(),
			'permissao_editar_produto_conta'=>$this->getPermissaoEditarProduto(),
			'permissao_excluir_produto_conta'=>$this->getPermissaoExcluirProduto(),
			
			'permissao_cadastrar_servico_conta'=>$this->getPermissaoCadastrarServico(),
			'permissao_visualizar_servico_conta'=>$this->getPermissaoVisualizarServico(),
			'permissao_editar_servico_conta'=>$this->getPermissaoEditarServico(),
			'permissao_excluir_servico_conta'=>$this->getPermissaoExcluirServico(),

			'permissao_cadastrar_centro_custo_conta' =>$this->getPermissaoCadastrarCentroCusto(),
			'permissao_visualizar_centro_custo_conta' =>$this->getPermissaoVisualizarCentroCusto(),
			'permissao_editar_centro_custo_conta' =>$this->getPermissaoEditarCentroCusto(),
			'permissao_excluir_centro_custo_conta'=>$this->getPermissaoExcluirCentroCusto(),
			
			'permissao_cadastrar_banco_conta' =>$this->getPermissaoCadastrarBanco(),
			'permissao_visualizar_banco_conta' =>$this->getPermissaoVisualizarBanco(),
			'permissao_editar_banco_conta' =>$this->getPermissaoEditarBanco(),
			'permissao_excluir_banco_conta' =>$this->getPermissaoExcluirBanco()
		);

		$sql = $this->db->insert('conta',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}

	private function editar(){
		$dados = array(
			'codigo_conta'=>$this->getCodigo(),
			'usuario_conta'=>$this->getUsuario(),
			'senha_conta'=>$this->getSenha(),
			
			'permissao_cadastrar_cliente_conta'=>$this->getPermissaoCadastrarCliente(),
			'permissao_visualizar_cliente_conta'=>$this->getPermissaoVisualizarCliente(),
			'permissao_editar_cliente_conta'=>$this->getPermissaoEditarCliente(),
			'permissao_excluir_cliente_conta'=>$this->getPermissaoExcluirCliente(),

			'permissao_cadastrar_colaborador_conta'=>$this->getPermissaoCadastrarColaborador(),
			'permissao_visualizar_colaborador_conta'=>$this->getPermissaoVisualizarColaborador(),
			'permissao_editar_colaborador_conta'=>$this->getPermissaoEditarColaborador(),
			'permissao_excluir_colaborador_conta'=>$this->getPermissaoExcluirColaborador(),

			'permissao_cadastrar_conta_conta'=>$this->getPermissaoCadastrarConta(),
			'permissao_visualizar_conta_conta'=>$this->getPermissaoVisualizarConta(),
			'permissao_editar_conta_conta'=>$this->getPermissaoEditarConta(),
			'permissao_excluir_conta_conta'=>$this->getPermissaoExcluirConta(),

			'permissao_cadastrar_estoque_conta'=>$this->getPermissaoCadastrarEstoque(),
			'permissao_visualizar_estoque_conta'=>$this->getPermissaoVisualizarEstoque(),
			'permissao_editar_estoque_conta'=>$this->getPermissaoEditarEstoque(),
			'permissao_excluir_estoque_conta'=>$this->getPermissaoExcluirEstoque(),

			'permissao_cadastrar_financeiro_conta'=>$this->getPermissaoCadastrarFinanceiro(),
			'permissao_visualizar_financeiro_conta'=>$this->getPermissaoVisualizarFinanceiro(),
			'permissao_editar_financeiro_conta'=>$this->getPermissaoEditarFinanceiro(),
			'permissao_excluir_financeiro_conta'=>$this->getPermissaoExcluirFinanceiro(),

			'permissao_cadastrar_fornecedor_conta'=>$this->getPermissaoCadastrarFornecedor(),
			'permissao_visualizar_fornecedor_conta'=>$this->getPermissaoVisualizarFornecedor(),
			'permissao_editar_fornecedor_conta'=>$this->getPermissaoEditarFornecedor(),
			'permissao_excluir_fornecedor_conta'=>$this->getPermissaoExcluirFornecedor(),

			'permissao_cadastrar_produto_conta'=>$this->getPermissaoCadastrarProduto(),
			'permissao_visualizar_produto_conta'=>$this->getPermissaoVisualizarProduto(),
			'permissao_editar_produto_conta'=>$this->getPermissaoEditarProduto(),
			'permissao_excluir_produto_conta'=>$this->getPermissaoExcluirProduto(),
			
			'permissao_cadastrar_servico_conta'=>$this->getPermissaoCadastrarServico(),
			'permissao_visualizar_servico_conta'=>$this->getPermissaoVisualizarServico(),
			'permissao_editar_servico_conta'=>$this->getPermissaoEditarServico(),
			'permissao_excluir_servico_conta'=>$this->getPermissaoExcluirServico(),

			'permissao_cadastrar_centro_custo_conta' =>$this->getPermissaoCadastrarCentroCusto(),
			'permissao_visualizar_centro_custo_conta' =>$this->getPermissaoVisualizarCentroCusto(),
			'permissao_editar_centro_custo_conta' =>$this->getPermissaoEditarCentroCusto(),
			'permissao_excluir_centro_custo_conta'=>$this->getPermissaoExcluirCentroCusto(),
			
			'permissao_cadastrar_banco_conta' =>$this->getPermissaoCadastrarBanco(),
			'permissao_visualizar_banco_conta' =>$this->getPermissaoVisualizarBanco(),
			'permissao_editar_banco_conta' =>$this->getPermissaoEditarBanco(),
			'permissao_excluir_banco_conta' =>$this->getPermissaoExcluirBanco()
		);

		$this->db->where('codigo_conta',$this->getCodigo());
		$sql = $this->db->update('conta',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}

	private function desativar(){
		$dados = array(
			'codigo_conta'=>$this->getCodigo(),
			'status_conta'=>'0'
		);

		$this->db->where('codigo_conta',$this->getCodigo());
		$sql = $this->db->update('conta',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}

	private function ativar(){
		$dados = array(
			'codigo_conta'=>$this->getCodigo(),
			'status_conta'=>'1'
		);

		$this->db->where('codigo_conta',$this->getCodigo());
		$sql = $this->db->update('conta',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}

	private function entrar(){
		$this->db->select('
			codigo_empresa,
			status_empresa,
			nome_empresa,

			codigo_conta,
			usuario_conta,
			senha_conta,
			status_conta,
			permissao_administrador_conta,

			permissao_cadastrar_cliente_conta,
			permissao_visualizar_cliente_conta,
			permissao_editar_cliente_conta,
			permissao_excluir_cliente_conta,

			permissao_cadastrar_colaborador_conta,
			permissao_visualizar_colaborador_conta,
			permissao_editar_colaborador_conta,
			permissao_excluir_colaborador_conta,

			permissao_cadastrar_conta_conta,
			permissao_visualizar_conta_conta,
			permissao_editar_conta_conta,
			permissao_excluir_conta_conta,

			permissao_cadastrar_estoque_conta,
			permissao_visualizar_estoque_conta,
			permissao_editar_estoque_conta,
			permissao_excluir_estoque_conta,

			permissao_cadastrar_fornecedor_conta,
			permissao_visualizar_fornecedor_conta,
			permissao_editar_fornecedor_conta,
			permissao_excluir_fornecedor_conta,

			permissao_cadastrar_produto_conta,
			permissao_visualizar_produto_conta,
			permissao_editar_produto_conta,
			permissao_excluir_produto_conta,

			permissao_cadastrar_servico_conta,
			permissao_visualizar_servico_conta,
			permissao_editar_servico_conta,
			permissao_excluir_servico_conta,

			permissao_cadastrar_financeiro_conta,
			permissao_visualizar_financeiro_conta,
			permissao_editar_financeiro_conta,
			permissao_excluir_financeiro_conta,

			permissao_cadastrar_centro_custo_conta,
			permissao_visualizar_centro_custo_conta,
			permissao_editar_centro_custo_conta,
			permissao_excluir_centro_custo_conta,
			
			permissao_cadastrar_banco_conta,
			permissao_visualizar_banco_conta,
			permissao_editar_banco_conta,
			permissao_excluir_banco_conta
		');

		$this->db->from('empresa');
		$this->db->join('conta','conta.codigo_empresa_conta = empresa.codigo_empresa');
		$this->db->where('conta.status_conta','1');
		$this->db->where('empresa.status_empresa','1');
		$this->db->where('conta.usuario_conta',$this->getUsuario());
		$this->db->where('conta.senha_conta',$this->getSenha());
		$sql = $this->db->get();

		if($sql->num_rows() === 1){
			$sessao['usuario_conta'] = $this->getUsuario();

			foreach($sql->result_array() as $dados){
				$sessao['codigo_empresa'] = $dados['codigo_empresa'];
				$sessao['nome_empresa'] = $dados['nome_empresa'];

				$sessao['permissao_administrador_conta'] = $dados['permissao_administrador_conta'];

				$sessao['permissao_cadastrar_cliente_conta'] = $dados['permissao_cadastrar_cliente_conta'];
				$sessao['permissao_visualizar_cliente_conta'] = $dados['permissao_visualizar_cliente_conta'];
				$sessao['permissao_editar_cliente_conta'] = $dados['permissao_editar_cliente_conta'];
				$sessao['permissao_excluir_cliente_conta'] = $dados['permissao_excluir_cliente_conta'];

				$sessao['permissao_cadastrar_colaborador_conta'] = $dados['permissao_cadastrar_colaborador_conta'];
				$sessao['permissao_visualizar_colaborador_conta'] = $dados['permissao_visualizar_colaborador_conta'];
				$sessao['permissao_editar_colaborador_conta'] = $dados['permissao_editar_colaborador_conta'];
				$sessao['permissao_excluir_colaborador_conta'] = $dados['permissao_excluir_colaborador_conta'];

				$sessao['permissao_cadastrar_conta_conta'] = $dados['permissao_cadastrar_conta_conta'];
				$sessao['permissao_visualizar_conta_conta'] = $dados['permissao_visualizar_conta_conta'];
				$sessao['permissao_editar_conta_conta'] = $dados['permissao_editar_conta_conta'];
				$sessao['permissao_excluir_conta_conta'] = $dados['permissao_excluir_conta_conta'];

				$sessao['permissao_cadastrar_estoque_conta'] = $dados['permissao_cadastrar_estoque_conta'];
				$sessao['permissao_visualizar_estoque_conta'] = $dados['permissao_visualizar_estoque_conta'];
				$sessao['permissao_editar_estoque_conta'] = $dados['permissao_editar_estoque_conta'];
				$sessao['permissao_excluir_estoque_conta'] = $dados['permissao_excluir_estoque_conta'];

				$sessao['permissao_cadastrar_financeiro_conta'] = $dados['permissao_cadastrar_financeiro_conta'];
				$sessao['permissao_visualizar_financeiro_conta'] = $dados['permissao_visualizar_financeiro_conta'];
				$sessao['permissao_editar_financeiro_conta'] = $dados['permissao_editar_financeiro_conta'];
				$sessao['permissao_excluir_financeiro_conta'] = $dados['permissao_excluir_financeiro_conta'];

				$sessao['permissao_cadastrar_fornecedor_conta'] = $dados['permissao_cadastrar_fornecedor_conta'];
				$sessao['permissao_visualizar_fornecedor_conta'] = $dados['permissao_visualizar_fornecedor_conta'];
				$sessao['permissao_editar_fornecedor_conta'] = $dados['permissao_editar_fornecedor_conta'];
				$sessao['permissao_excluir_fornecedor_conta'] = $dados['permissao_excluir_fornecedor_conta'];

				$sessao['permissao_cadastrar_produto_conta'] = $dados['permissao_cadastrar_produto_conta'];
				$sessao['permissao_visualizar_produto_conta'] = $dados['permissao_visualizar_produto_conta'];
				$sessao['permissao_editar_produto_conta'] = $dados['permissao_editar_produto_conta'];
				$sessao['permissao_excluir_produto_conta'] = $dados['permissao_excluir_produto_conta'];

				$sessao['permissao_cadastrar_servico_conta'] = $dados['permissao_cadastrar_servico_conta'];
				$sessao['permissao_visualizar_servico_conta'] = $dados['permissao_visualizar_servico_conta'];
				$sessao['permissao_editar_servico_conta'] = $dados['permissao_editar_servico_conta'];
				$sessao['permissao_excluir_servico_conta'] = $dados['permissao_excluir_servico_conta'];

				$sessao['permissao_cadastrar_centro_custo_conta'] = $dados['permissao_cadastrar_centro_custo_conta'];
				$sessao['permissao_visualizar_centro_custo_conta'] = $dados['permissao_visualizar_centro_custo_conta'];
				$sessao['permissao_editar_centro_custo_conta'] = $dados['permissao_editar_centro_custo_conta'];
				$sessao['permissao_excluir_centro_custo_conta'] = $dados['permissao_excluir_centro_custo_conta'];

				$sessao['permissao_cadastrar_banco_conta'] = $dados['permissao_cadastrar_banco_conta'];
				$sessao['permissao_visualizar_banco_conta'] = $dados['permissao_visualizar_banco_conta'];
				$sessao['permissao_editar_banco_conta'] = $dados['permissao_editar_banco_conta'];
				$sessao['permissao_excluir_banco_conta'] = $dados['permissao_excluir_banco_conta'];
			}

			$this->session->set_userdata($sessao);
 
			return true;
		}else{
			return false;
		}
	}

	public function validarSessao(){
		if($this->getUsuario() === 'gsuper'){
			return true;
		}else{
			$sql = $this->db->get_where('conta',array('usuario_conta'=>$this->getUsuario()));

			if($sql->num_rows === 1){
				return true;
			}else{
				return false;
			}
		}
	}

	public function sair(){
		$this->session->unset_userdata($this->getUsuario());
		$this->session->sess_destroy();

		return true;
	}
}