<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Empresa_model extends CI_Model{
	private $codigo;
	private $nome;
	private $status;
	private $permissao_cliente;
	private $permissao_colaborador;
	private $permissao_conta;
	private $permissao_estoque;
	private $permissao_financeiro;
	private $permissao_fornecedor;
	private $permissao_produto;
	private $permissao_servico;
	private $permissao_centro_custo;
	private $permissao_banco;
	
	public function __construct(){
		parent::__construct();

		$this->load->database(); 
		$this->load->library('session');
	}

	public function setCodigo($codigo){
		$this->codigo = $codigo;
	}

	public function setNome($nome){
		$this->nome = $nome;
	}

	public function setStatus($status){
		$this->status = $status;
	}
	
    public function setPermissaoCliente($permissao_cliente){
		$this->permissao_cliente = $permissao_cliente;
	}

    public function setPermissaoColaborador($permissao_colaborador){
		$this->permissao_colaborador = $permissao_colaborador;
	}
	
	public function setPermissaoConta($permissao_conta){
		$this->permissao_conta = $permissao_conta;
	}

	public function setPermissaoEstoque($permissao_estoque){
		$this->permissao_estoque = $permissao_estoque;
	}

	public function setPermissaoFinanceiro($permissao_financeiro){
		$this->permissao_financeiro = $permissao_financeiro;
	}

	public function setPermissaoFornecedor($permissao_fornecedor){
		$this->permissao_fornecedor = $permissao_fornecedor;
	}
	
	public function setPermissaoProduto($permissao_produto){
		$this->permissao_produto = $permissao_produto;
	}

	public function setPermissaoServico($permissao_servico){
		$this->permissao_servico = $permissao_servico;
	}

	public function setPermissaoCentroCusto($permissao_centro_custo){
		$this->permissao_centro_custo = $permissao_centro_custo;
	}

	public function setPermissaoBanco($permissao_banco){
		$this->permissao_banco = $permissao_banco;
	}

	public function getCodigo(){
		return $this->codigo;
	}

	public function getNome(){
		return $this->nome;
	}

	public function getStatus(){
		return $this->status;
	}
	
	public function getPermissaoCliente(){
		return $this->permissao_cliente;
	}

	public function getPermissaoColaborador(){
		return $this->permissao_colaborador;
	}

	public function getPermissaoConta(){
		return $this->permissao_conta;
	}

	public function getPermissaoEstoque(){
		return $this->permissao_estoque;
	}

	public function getPermissaoFinanceiro(){
		return $this->permissao_financeiro;
	}

	public function getPermissaoFornecedor(){
		return $this->permissao_fornecedor;
	}

	public function getPermissaoProduto(){
		return $this->permissao_produto;
	}

	public function getPermissaoServico(){
		return $this->permissao_servico;
	}

	public function getPermissaoCentroCusto(){
		return $this->permissao_centro_custo;
	}

	public function getPermissaoBanco(){
		return $this->permissao_banco;
	}
	
	public function getCadastrar(){
		return $this->cadastrar();
	}

	public function getEditar(){
		return $this->editar();
	}

	public function getAtivar(){
		return $this->ativar();
	}

	public function getDesativar(){
		return $this->desativar();
	}

	private function cadastrar(){
		$dados = array(
			'nome_empresa'=>$this->getNome(),
			'status_empresa'=>'1',
			'permissao_cliente_empresa'=>$this->getPermissaoCliente(),
			'permissao_colaborador_empresa'=>$this->getPermissaoColaborador(),
			'permissao_conta_empresa'=>$this->getPermissaoConta(),
			'permissao_estoque_empresa'=>$this->getPermissaoEstoque(),
			'permissao_financeiro_empresa'=>$this->getPermissaoFinanceiro(),
			'permissao_fornecedor_empresa'=>$this->getPermissaoFornecedor(),
			'permissao_produto_empresa'=>$this->getPermissaoProduto(),
			'permissao_servico_empresa'=>$this->getPermissaoServico(),
			'permissao_centro_custo_empresa'=>$this->getPermissaoCentroCusto(),
			'permissao_banco_empresa'=>$this->getPermissaoBanco()
		);

		$sql = $this->db->insert('empresa',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}

	private function editar(){
		$dados = array(
			'codigo_empresa'=>$this->getCodigo(),
			'nome_empresa'=>$this->getNome(),
			'permissao_cliente_empresa'=>$this->getPermissaoCliente(),
			'permissao_colaborador_empresa'=>$this->getPermissaoColaborador(),
			'permissao_conta_empresa'=>$this->getPermissaoConta(),
			'permissao_estoque_empresa'=>$this->getPermissaoEstoque(),
			'permissao_financeiro_empresa'=>$this->getPermissaoFinanceiro(),
			'permissao_fornecedor_empresa'=>$this->getPermissaoFornecedor(),
			'permissao_produto_empresa'=>$this->getPermissaoProduto(),
			'permissao_servico_empresa'=>$this->getPermissaoServico(),
			'permissao_centro_custo_empresa'=>$this->getPermissaoCentroCusto(),
			'permissao_banco_empresa'=>$this->getPermissaoBanco()
		);

		$this->db->where('codigo_empresa',$this->getCodigo());
		$sql = $this->db->update('empresa',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}

	private function desativar(){
		$dados = array(
			'codigo_empresa'=>$this->getCodigo(),
			'status_empresa'=>'0'
		);

		$this->db->where('codigo_empresa',$this->getCodigo());
		$sql = $this->db->update('empresa',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}

	private function ativar(){
		$dados = array(
			'codigo_empresa'=>$this->getCodigo(),
			'status_empresa'=>'1'
		);

		$this->db->where('codigo_empresa',$this->getCodigo());
		$sql = $this->db->update('empresa',$dados);

		if($sql){
			return true;
		}else{
			return false;
		}
	}
	
} // fecha a class