<?php
require("libs/Smarty.class.php");

/*
 * @author rodolfobarretoweb@gmail.com
 * @version 2.0
 */

class Smarty_ci extends Smarty{
    
    /* 
     * @var $add_cache
     * @acess private
     */
    private $time_cache;
    
    /*
     * @acess public
     * @param time_cache
     */
    public function setTimeCache($time_cache){
        $this->time_cache = $time_cache;
    }
    
    public function __construct() {     
        parent::__construct();
        
        # get configs of server
        $config =& get_config();
        
        parent::setTemplateDir ($config['application_dir'].'views');
        parent::setCompileDir  ($config['application_dir'].'compile');
        parent::setCacheDir    ($config['application_dir'].'cache');
        
        if($this->time_cache > 0){
            $this->caching = true;
        }else{
            $this->caching = false;
        }
        
        # time of cache
        $this->cache_lifetime = $this->time_cache;
        
        $this->compile_check = true;
    }
}
?>