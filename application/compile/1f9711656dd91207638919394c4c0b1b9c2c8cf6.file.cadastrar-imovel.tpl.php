<?php /* Smarty version Smarty-3.1.16, created on 2016-04-20 00:57:15
         compiled from "/opt/lampp/htdocs/wmanager/application/views/cadastrar-imovel.tpl" */ ?>
<?php /*%%SmartyHeaderCode:167037728757158f9b6869a4-01626527%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f9711656dd91207638919394c4c0b1b9c2c8cf6' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/cadastrar-imovel.tpl',
      1 => 1461106625,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '167037728757158f9b6869a4-01626527',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_57158f9b6d48e0_35944396',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57158f9b6d48e0_35944396')) {function content_57158f9b6d48e0_35944396($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar Imóvel"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/imovel.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo Imóvel</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
imovel" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
imovel/fazerCadastro" method="post" id="formulario">
			
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes">
                    		</div>
                    	</div>

                    	<br>
                    	
                    	<div class="row">	
                       		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        	    <label>Descrição</label>
                                <input type="text" placeholder="Descrição" class="form-control" id="descricao">
	                        </div>
                        		
                        	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                           	    <label>Endereço</label>
	                            <input type="text" placeholder="Endereço" class="form-control" id="endereco">
	                        </div>

	                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Cep</label>
	                            <input type="text" placeholder="Cep" class="form-control" id="cep" maxlength="10">
	                        </div>
	                    </div>
	                        
	                    <br>

	                    <div class="row">
	                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                           	    <label>Imagem</label>
	                            <input type="text" placeholder="Imagem" class="form-control" id="imagem">
	                        </div>
	                    
	                    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Valor</label>
	                            <input type="text" placeholder="Valor" id="valor" class="form-control">
	                        </div>	
	                	</div>                                                            
					</div>
				</div>
			<br>
			<br>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
