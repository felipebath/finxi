<?php /* Smarty version Smarty-3.1.16, created on 2014-09-10 20:10:50
         compiled from "/opt/lampp/htdocs/wmanager/application/views/listar-contas.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11952949925410942a4dbea9-54587419%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5380bb89a56d94850e429f668613c138f6ae6de1' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/listar-contas.tpl',
      1 => 1402925592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11952949925410942a4dbea9-54587419',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_conta_conta' => 0,
    'codigo_conta' => 0,
    'usuario_conta' => 0,
    'nome_empresa' => 0,
    'permissao_editar_conta_conta' => 0,
    'permissao_excluir_conta_conta' => 0,
    'links_paginacao' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5410942a5442b9_87800602',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5410942a5442b9_87800602')) {function content_5410942a5442b9_87800602($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/wmanager/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Contas"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/conta.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Contas</h1>
				</div>

				<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_conta_conta']->value==='1') {?>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">Cadastrar</a>
					</div>
				<?php }?>
			</div>
		</header>

		<div class="table-responsive">
			<table class='table table-hover table-condensed'>
				<thead>
					<th width="48%">Usuário</th>
					<th width="48%">Empresa</th>
					<th width="2%"></th>
					<th width="2%"></th>
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['codigo_conta']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['codigo_conta']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['usuario_conta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
								<td><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome_empresa']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]);?>
</td>
								
								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_editar_conta_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/editar/<?php echo $_smarty_tpl->tpl_vars['codigo_conta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><span class="glyphicon glyphicon glyphicon-edit"></span></a>
									<?php }?>
								</td>

								<td>
									<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_conta_conta']->value==='1') {?>
										<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/excluir/<?php echo $_smarty_tpl->tpl_vars['codigo_conta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" class="deletar_conta"><span class="glyphicon glyphicon-trash"></span></a>
									<?php }?>
								</td>
							</tr>
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>
			<div class='row'>
				<div class='col-md-offset-5 col-lg-offset-5'>	
					<nav>
						<ul class='pagination'>
							<?php echo $_smarty_tpl->tpl_vars['links_paginacao']->value;?>

						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
