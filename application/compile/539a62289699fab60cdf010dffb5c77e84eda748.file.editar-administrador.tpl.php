<?php /* Smarty version Smarty-3.1.16, created on 2014-09-10 20:09:20
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-administrador.tpl" */ ?>
<?php /*%%SmartyHeaderCode:538860868541093d0c692a9-08675281%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '539a62289699fab60cdf010dffb5c77e84eda748' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-administrador.tpl',
      1 => 1405712176,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '538860868541093d0c692a9-08675281',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
    'codigo_empresa' => 0,
    'nome_empresa' => 0,
    'usuario_conta' => 0,
    'senha_conta' => 0,
    'permissao_conta_empresa' => 0,
    'permissao_produto_empresa' => 0,
    'permissao_financeiro_empresa' => 0,
    'permissao_colaborador_empresa' => 0,
    'permissao_cliente_empresa' => 0,
    'permissao_fornecedor_empresa' => 0,
    'permissao_estoque_empresa' => 0,
    'permissao_servico_empresa' => 0,
    'permissao_banco_empresa' => 0,
    'permissao_centro_custo_empresa' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_541093d0cd5081_95670193',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_541093d0cd5081_95670193')) {function content_541093d0cd5081_95670193($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editando administrador"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-1.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/administrador.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando administrador</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div class="btn-group pull-right">
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
administrador" class="btn btn-primary" title="Visualizar todos">Visualizar todos</a>
						<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
administrador/cadastrar" class="btn btn-primary" title="Cadastrar">Cadastrar</a>
					</div>
				</div>
			</div>
		</header>

		<nav>
			<ul class="nav nav-tabs">
				<li class="active"><a href="#entrada" title="Entrada" data-toggle="tab">Entrada</a></li>
				<li><a href="#permissoes" title="Permissões" data-toggle="tab">Permissões</a></li>
			</ul>
		</nav>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
administrador/fazerEdicao" method="post" id="formulario_editar_administrador">
			<input type="hidden" id="codigo_empresa_editar_administrador" value="<?php echo $_smarty_tpl->tpl_vars['codigo_empresa']->value;?>
">

		<div class="tab-content">
			<div class="tab-pane active" id="entrada">
				<br>
				
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Nome da empresa *</label>
								<input type="text" id="nome_empresa_editar_administrador" placeholder="Digite o nome da empresa" class="form-control" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome_empresa']->value;?>
" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_nome_empresa_editar_administrador"></span>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Usuário *</label>
								<input type="text" id="usuario_editar_administrador" value="<?php echo $_smarty_tpl->tpl_vars['usuario_conta']->value;?>
" placeholder="Digite seu nome de usuário" class="form-control" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_usuario_editar_administrador"></span>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Senha *</label>
								<input type="password" id="senha_editar_administrador" value="<?php echo $_smarty_tpl->tpl_vars['senha_conta']->value;?>
" placeholder="Digite sua senha" class="form-control" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_senha_editar_administrador"></span>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- entrada -->

			<div class="tab-pane" id="permissoes">
				<br>

				<?php if ($_smarty_tpl->tpl_vars['permissao_conta_empresa']->value==='1'&&$_smarty_tpl->tpl_vars['permissao_produto_empresa']->value==='1'&&$_smarty_tpl->tpl_vars['permissao_financeiro_empresa']->value==='1'&&$_smarty_tpl->tpl_vars['permissao_colaborador_empresa']->value==='1'&&$_smarty_tpl->tpl_vars['permissao_cliente_empresa']->value==='1'&&$_smarty_tpl->tpl_vars['permissao_fornecedor_empresa']->value==='1'&&$_smarty_tpl->tpl_vars['permissao_estoque_empresa']->value==='1'&&$_smarty_tpl->tpl_vars['permissao_servico_empresa']->value==='1') {?>
					<input type="checkbox" id="selecionar_todos_permissao_editar_administrador" checked="checked">
				<?php } else { ?>
					<input type="checkbox" id="selecionar_todos_permissao_editar_administrador">
				<?php }?>
				<span>Selecionar todos</span><br>

				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php if ($_smarty_tpl->tpl_vars['permissao_conta_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_conta" id="permissao_conta_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_conta" id="permissao_conta_editar_administrador">
								<?php }?>
								<span>Conta</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

								<?php if ($_smarty_tpl->tpl_vars['permissao_produto_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_produto" id="permissao_produto_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_produto" id="permissao_produto_editar_administrador">
								<?php }?>
								<span>Produto</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

								<?php if ($_smarty_tpl->tpl_vars['permissao_financeiro_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_financeiro" id="permissao_financeiro_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_financeiro" id="permissao_financeiro_editar_administrador">
								<?php }?>
								<span>Finânceiro</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php if ($_smarty_tpl->tpl_vars['permissao_colaborador_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_colaborador" id="permissao_colaborador_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_colaborador" id="permissao_colaborador_editar_administrador">
								<?php }?>
								<span>Colaborador</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

								<?php if ($_smarty_tpl->tpl_vars['permissao_cliente_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_cliente" id="permissao_cliente_editar_administrador"checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_cliente" id="permissao_cliente_editar_administrador">
								<?php }?>
								
								<span>Cliente</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php if ($_smarty_tpl->tpl_vars['permissao_fornecedor_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_fornecedor" id="permissao_fornecedor_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_fornecedor" id="permissao_fornecedor_editar_administrador">
								<?php }?>
								<span>Fonecedor</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php if ($_smarty_tpl->tpl_vars['permissao_estoque_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_estoque" id="permissao_estoque_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_estoque" id="permissao_estoque_editar_administrador">	
								<?php }?>
								<span>Estoque</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php if ($_smarty_tpl->tpl_vars['permissao_servico_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_servico" id="permissao_servico_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_servico" id="permissao_servico_editar_administrador">
								<?php }?>
								<span>Serviço</span><br>
							</div>
						</div>
							
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php if ($_smarty_tpl->tpl_vars['permissao_banco_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_banco" id="permissao_banco_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_banco" id="permissao_banco_editar_administrador">
								<?php }?>
								<span>Banco</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php if ($_smarty_tpl->tpl_vars['permissao_centro_custo_empresa']->value==='1') {?>
									<input type="checkbox" name="permissao_centro_custo" id="permissao_centro_custo_editar_administrador" checked="checked">
								<?php } else { ?>
									<input type="checkbox" name="permissao_centro_custo" id="permissao_centro_custo_editar_administrador">
								<?php }?>
								<span>Centro de Custo</span><br>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- permissoes -->
		</div>

			<br>
			<input type="submit" value="Salvar alterações" class="btn btn-primary">

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
