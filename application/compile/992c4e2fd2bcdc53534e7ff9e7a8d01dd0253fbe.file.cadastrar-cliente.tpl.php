<?php /* Smarty version Smarty-3.1.16, created on 2014-07-11 15:15:11
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/cadastrar-cliente.tpl" */ ?>
<?php /*%%SmartyHeaderCode:536158569537f5b7e79ae78-43120935%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '992c4e2fd2bcdc53534e7ff9e7a8d01dd0253fbe' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/cadastrar-cliente.tpl',
      1 => 1402921522,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '536158569537f5b7e79ae78-43120935',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_537f5b7e7c7d92_41058051',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_537f5b7e7c7d92_41058051')) {function content_537f5b7e7c7d92_41058051($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar cliente"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/cliente.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo cliente</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/fazerCadastro" method="post" id="formulario_cadastrar_cliente">

		<div class="tab-content">
			<div class="tab-pane active" id="cadastro">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
	                            	<select class="form-control" id="status_cadastrar_cliente">
	                                	<option value="Ativo">Ativo</option>
	                                	<option value="Inativo">Inativo</option>
	                                	<option value="Bloqueado">Bloqueado</option>
	                                </select>
	                        </div>

							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Tipo pessoa</label>
	                            	<select class="form-control" id="tipo_pessoa_cadastrar_cliente">
	                                	<option value="Física">Física</option>
	                                	<option value="Jurídica">Jurídica</option>
	                                </select>
	                        </div>
	                    </div>

	                    <br>

                       	<div id="pessoa_juridica" style="display:none;">
                    		<div class="row"> 	
                       			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        			<label>Nome fantasia</label>
                                	<input type="text" placeholder="Nome Fantasia" class="form-control" id="nome_fantasia_cadastrar_cliente" maxlength="80" autofocus="yes" autocomplete="yes">
                                	<span class='alerta_formulario' id='alerta_nome_fantasia_cadastrar_cliente'></span>
                        		</div>
                        
                        		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                            	    <label>Razão Social</label>
		                                <input type="text" placeholder="Razão social" class="form-control" id="razao_social_cadastrar_cliente" autocomplete="yes">
		                                <span class='alerta_formulario' id='alerta_razao_social_cadastrar_cliente'></span>
	                        	</div>
	                        
	                        		
	                        	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                           	    <label>CNPJ</label>
		                            <input type="text" placeholder="CNPJ" class="form-control"  id="cnpj_cadastrar_cliente" data-mascara-campo="cnpj" maxlength="18" autocomplete="yes">
		                            <span class='alerta_formulario' id='alerta_cnpj_cadastrar_cliente'></span>
		                        </div>
		                	</div>
	                    </div>	
                            
                        <div id="pessoa_fisica">
	                    	<div class="row"> 	
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                            	<label>Nome</label>
	                            	<input type="text" placeholder="Nome Completo" id="nome_cadastrar_cliente" autocomplete="yes" class="form-control" autofocus="yes">
	                            	<span class='alerta_formulario' id='alerta_nome_cadastrar_cliente'></span>
	                            </div>
	                        	
	                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                            	<label>CPF</label>
	                            	<input type="text" id="cpf_cadastrar_cliente" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14">
	                            	<span class='alerta_formulario' id='alerta_cpf_cadastrar_cliente'></span>
	                            </div>
	                        </div>    
	                    </div>	
	                    <br>
	                    <div>
	                    	<div class="row"> 	
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	                            	<label>Telefone</label>
	                            	<input type="text" placeholder="Digite o Telefone" id="telefone_cadastrar_cliente" autocomplete="yes" class="form-control" autofocus="yes" data-mascara-campo='telefone' maxlength="15">
	                            	<span class='alerta_formulario' id='alerta_telefone_cadastrar_cliente'></span>
	                            </div>
	                        	
	                        	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                            	<label>Endereço</label>
	                            	<input type="text" id="endereco_cadastrar_cliente" placeholder="Digite o endereço" class="form-control"  autocomplete="yes">
	                            	<span class='alerta_endereco' id='alerta_endereco_cadastrar_cliente'></span>
	                            </div>

	                             <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                            	<label>Cidade</label>
	                            	<input type="text" placeholder="Digite a Cidade" id="cidade_cadastrar_cliente" autocomplete="yes" class="form-control" autofocus="yes">
	                            	<span class='alerta_formulario' id='alerta_cidade_cadastrar_cliente'></span>
	                            </div>
	                        </div>

	                    	<div class="row"> 	       
	                             <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	                            	<label>CEP</label>
	                            	<input type="text" placeholder="Digite o Cep" id="cep_cadastrar_cliente" autocomplete="yes" class="form-control" autofocus="yes" data-mascara-campo='cep' maxlength="9">
	                            	<span class='alerta_formulario' id='alerta_cep_cadastrar_cliente'></span>
	                            </div>

	                             <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	                            	<label>E-mail</label>
	                            	<input type="text" placeholder="Digite o E-mail" id="email_cadastrar_cliente" autocomplete="yes" class="form-control" autofocus="yes" maxlength="80">
	                            	<span class='alerta_formulario' id='alerta_email_cadastrar_cliente'></span>
	                            </div>
	                        	
	                        	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Estado</label>
								<select class="form-control" id="estado_cadastrar_cliente">
									<option value="AC">AC</option>
									<option value="AL">AL</option>
									<option value="AM">AM</option>
									<option value="AP">AP</option>
									<option value="BA">BA</option>
									<option value="CE">CE</option>
									<option value="ES">ES</option>
									<option value="GO">GO</option>
									<option value="MA">MA</option>
									<option value="MG">MG</option>
									<option value="MS">MS</option>
									<option value="MT">MT</option>
									<option value="PA">PA</option>
									<option value="PB">PB</option>
									<option value="PE">PE</option>
									<option value="PI">PI</option>
									<option value="PR">PR</option>
									<option value="RJ" selected="selected">RJ</option>
									<option value="RN">RN</option>
									<option value="RO">RO</option>
									<option value="RR">RR</option>
									<option value="RS">RS</option>
									<option value="SC">SC</option>
									<option value="SE">SE</option>
									<option value="SP">SP</option>
									<option value="TO">TO</option>
								 </select>
							</div>
	                    </div>				
					</div>	
			
					</div>
				</div>
			</div> <!-- cadastro -->
		</div>

			<br>
			<input type="submit" value="Cadastrar" class="btn btn-primary">

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
