<?php /* Smarty version Smarty-3.1.16, created on 2014-08-29 16:53:41
         compiled from "/opt/lampp/htdocs/rr/application/views/cadastrar-fornecedor-produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:107736632653ff29f58b4603-24394995%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cf057f81313c53b2f8129ffcf592a4e036f61ecf' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/cadastrar-fornecedor-produto.tpl',
      1 => 1409324007,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '107736632653ff29f58b4603-24394995',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53ff29f58f3983_52794006',
  'variables' => 
  array (
    'base_url' => 0,
    'fornecedor_id' => 0,
    'nome_fornecedor' => 0,
    'id_produto_list' => 0,
    'nome_produto_list' => 0,
    'id_grid' => 0,
    'nome_produto_grid' => 0,
    'permissao_excluir_fornecedor_conta' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ff29f58f3983_52794006')) {function content_53ff29f58f3983_52794006($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar fornecedor"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	
	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Adicionando produto ao fornecedor</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<nav>
			<ul class="nav nav-tabs" id="tab_fornecedor">
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/editar/<?php echo $_smarty_tpl->tpl_vars['fornecedor_id']->value;?>
" title="Fornecedor">Dados Cadastrais</a></li>
				<li class="active"><a href="#" title="Produtos">Produtos</a></li>
			</ul>
		</nav>
		
		<div class="tab-content">
			<div class="tab-pane active" id="fornecedor">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<br>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
									<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/fazerCadastroProduto?fornecedor_id=$fornecedor_id" method="post">
									<input type="hidden" name="fornecedor_id" value="<?php echo $_smarty_tpl->tpl_vars['fornecedor_id']->value;?>
">
										
										<div class="row">
											<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
												<label>Fornecedor</label>
												<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/editar/<?php echo $_smarty_tpl->tpl_vars['fornecedor_id']->value;?>
" title="editar fornecedor">
													<input type="text" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value;?>
" disabled="disabled">
												</a>							
											</div>
						                </div>

					                    <br>

										<div class="row">
											<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
												<label>Produtos</label>
												<select class="form-control" name="id_produto_list">
						                        	<?php if ($_smarty_tpl->tpl_vars['id_produto_list']->value!=='0') {?>
					                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_produto_list']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_produto_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
					                                        	<?php echo $_smarty_tpl->tpl_vars['nome_produto_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

					                                        </option>
					                                    <?php endfor; endif; ?>
					                                <?php } else { ?>
					                                    <option value="">Não foi possível localizar nenhum produto</option>
					                                <?php }?>
						                        </select>
						                        <span class='alerta_formulario' id='alerta_produto_nome'></span>
						                    </div>

						                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						                    	<br>
						                    	<input type="submit" class="btn btn-primary" title="Adicionar produto ao fornecedor" value="&nbsp;+&nbsp;">
						                    </div>	

					                    </div>
									</form>	
				                </div>
				            </div>
				        </div>

				        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<br>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<table class="table table-striped table-bordered table-hover">
				                                <thead>
				                                    <tr>
				                                        <th width="90%">Produto</th>
				                                        <th>Ação</th>
				                                    </tr>
				                                </thead>

				                                <tbody>
				                                	<?php if ($_smarty_tpl->tpl_vars['id_grid']->value!=='0') {?>
														<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_grid']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
															<tr>
																<td><?php echo $_smarty_tpl->tpl_vars['nome_produto_grid']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
																															
																<td>
																	<?php if ($_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value==='1') {?>
																		<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/excluirProduto/<?php echo $_smarty_tpl->tpl_vars['id_grid']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
?fornecedor_id=<?php echo $_smarty_tpl->tpl_vars['fornecedor_id']->value;?>
" title="excluir">
																			<span class="glyphicon glyphicon-trash"></span>
																		</a>
																	<?php }?>
																</td>
															</tr>
														<?php endfor; endif; ?>
													<?php } else { ?>
														<tr>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
													<?php }?>	
				                                </tbody>
			                            	</table>

			                            	<br>
					                    
											<div class="row">    
										    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/" class="btn btn-primary pull-right">Concluído</a>    
												</div>
											</div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>        
			</div>
 		</div>
				
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
