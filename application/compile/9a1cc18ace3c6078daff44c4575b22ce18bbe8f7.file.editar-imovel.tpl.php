<?php /* Smarty version Smarty-3.1.16, created on 2016-04-23 03:31:20
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-imovel.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2691264225716b4f83626c5-80626088%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9a1cc18ace3c6078daff44c4575b22ce18bbe8f7' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-imovel.tpl',
      1 => 1461375079,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2691264225716b4f83626c5-80626088',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5716b4f83aa9e2_72795047',
  'variables' => 
  array (
    'base_url' => 0,
    'usuario_conta_sessao' => 0,
    'id' => 0,
    'nome' => 0,
    'valor' => 0,
    'descricao' => 0,
    'endereco' => 0,
    'cep' => 0,
    'imagem' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5716b4f83aa9e2_72795047')) {function content_5716b4f83aa9e2_72795047($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar Imóvel"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/imovel.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando novo Imóvel</h1>
				</div>

				<?php if ($_smarty_tpl->tpl_vars['usuario_conta_sessao']->value!='') {?>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
imovel" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
				<?php }?>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
imovel/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome" class="form-control" id="nome" maxlength="80" autofocus="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
                    		</div>

                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label>Valor</label>
                                <input type="text" placeholder="Valor" id="valor" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['valor']->value;?>
">
                            </div>
                    	</div>

                    	<br>
                    	
                    	<div class="row">	
                       		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        	    <label>Descrição</label>
                                <input type="text" placeholder="Descrição" class="form-control" id="descricao" value="<?php echo $_smarty_tpl->tpl_vars['descricao']->value;?>
">
	                        </div>
                        		
                        	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                           	    <label>Endereço</label>
	                            <input type="text" placeholder="Endereço" class="form-control" id="endereco" value="<?php echo $_smarty_tpl->tpl_vars['endereco']->value;?>
">
	                        </div>

	                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Cep</label>
	                            <input type="text" placeholder="Cep" class="form-control" id="cep" maxlength="10" value="<?php echo $_smarty_tpl->tpl_vars['cep']->value;?>
">
	                        </div>
	                    </div>
	                        
	                    <br>

	                    <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                              <label>Imagem</label>
                              <img src="<?php echo $_smarty_tpl->tpl_vars['imagem']->value;?>
" width="400" style="border: 1px solid gray">
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                             	  <label></label>  
                                <input type="text" placeholder="Imagem" class="form-control" id="imagem" value="<?php echo $_smarty_tpl->tpl_vars['imagem']->value;?>
">
                            </div>
                        </div>                                                            
					</div>
				</div>
			<br>
			<br>
			<?php if ($_smarty_tpl->tpl_vars['usuario_conta_sessao']->value!='') {?>
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
