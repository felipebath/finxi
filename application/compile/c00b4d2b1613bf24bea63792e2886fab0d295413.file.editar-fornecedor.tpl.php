<?php /* Smarty version Smarty-3.1.16, created on 2014-09-15 17:23:01
         compiled from "/opt/lampp/htdocs/wmanager/application/views/editar-fornecedor.tpl" */ ?>
<?php /*%%SmartyHeaderCode:36305925554120c56ee6d68-38056649%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c00b4d2b1613bf24bea63792e2886fab0d295413' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/editar-fornecedor.tpl',
      1 => 1410794556,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '36305925554120c56ee6d68-38056649',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54120c57028611_87819307',
  'variables' => 
  array (
    'base_url' => 0,
    'id' => 0,
    'tipo_pessoa' => 0,
    'nome' => 0,
    'status' => 0,
    'razao_social' => 0,
    'inscricao_estadual' => 0,
    'cnpj' => 0,
    'cpf' => 0,
    'endereco' => 0,
    'complemento' => 0,
    'bairro' => 0,
    'cep' => 0,
    'estado' => 0,
    'cidade' => 0,
    'telefone' => 0,
    'celular' => 0,
    'fax' => 0,
    'email' => 0,
    'observacoes' => 0,
    'id_produto_list' => 0,
    'nome_produto_list' => 0,
    'id_fornecedor_produto' => 0,
    'nome_produto' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54120c57028611_87819307')) {function content_54120c57028611_87819307($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_mask')) include '/opt/lampp/htdocs/wmanager/application/libraries/Smarty-3.1.16/libs/plugins/modifier.mask.php';
?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar fornecedor"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/fornecedor.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando fornecedor</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/" class="pull-right btn btn-primary" title="Listar todos">
						Listar todos
					</a>
				</div>
			</div>
		</header>

		<nav>
            <ul class="nav nav-tabs" id="tab">
                <li class="active"><a href="#dados_cadastrais" title="dados cadastrais" data-toggle="tab">Dados cadastrais</a></li>
                <li><a href="#produtos" title="produtos desse fornecedor" data-toggle="tab">Produtos</a></li>
            </ul>
        </nav>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/fazerEdicao" method="post" id="formulario">
			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" id="id">	
			<div class="tab-content">
                <div class="tab-pane active" id="dados_cadastrais">
                <br>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="row">
								
								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<label>Tipo pessoa</label>
		                        	<select class="form-control" id="tipo_pessoa">
		                            	<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa']->value==='Física') {?>
		                                	<option value="Física">Física</option>
		                                	<option value="Jurídica">Jurídica</option>
		                                <?php } else { ?>	
		                                	<option value="Jurídica">Jurídica</option>
		                                	<option value="Física">Física</option>
		                                <?php }?>
									</select>
		                        </div>

		                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                           	<label>Nome</label>
		                           	<input type="text" placeholder="Nome" id="nome" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['nome']->value;?>
">
		                        </div>

								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<label>Status</label>
		                        	<select class="form-control" id="status">
		                            	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Ativo')) {?>
		                            		<option value="Ativo">Ativo</option>
		                                	<option value="Inativo">Inativo</option>
		                                <?php }?>
		                            	
		                            	<?php if (($_smarty_tpl->tpl_vars['status']->value==='Inativo')) {?>
		                            		<option value="Inativo">Inativo</option>
		                                	<option value="Ativo">Ativo</option>
		                                <?php }?>
		                            </select>
		                        </div>
		                    </div>

		                    <br>

		                	<?php if ($_smarty_tpl->tpl_vars['tipo_pessoa']->value==='Jurídica') {?>
		                       	<div id="pessoa_juridica">
		                    		<div class="row"> 	
		                       			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	    <label>Razão Social</label>
			                                <input type="text" placeholder="Razão social" class="form-control" id="razao_social" value="<?php echo $_smarty_tpl->tpl_vars['razao_social']->value;?>
">
			                        	</div>

			                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                        	    <label>Inscriçao Estadual</label>
			                                <input type="text" placeholder="Inscriçao Estadual" class="form-control" id="inscricao_estadual" value="<?php echo $_smarty_tpl->tpl_vars['inscricao_estadual']->value;?>
">
			                            </div>
			                        		                        		
			                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                           	    <label>CNPJ</label>
				                            <?php if ($_smarty_tpl->tpl_vars['cnpj']->value!='') {?>
												<input type="text" placeholder="CNPJ" class="form-control"  id="cnpj" data-mascara-campo="cnpj" maxlength="18" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj']->value,'##.###.###/####-##');?>
">
			                            	<?php } else { ?>
			                            		<input type="text" placeholder="CNPJ" class="form-control"  id="cnpj" data-mascara-campo="cnpj" maxlength="18">
			                            	<?php }?>
				                        </div>
				                	</div>
			                    </div>
			                
			                	<div id="pessoa_fisica" style="display:none;">
			                    	<div class="row"> 	
		                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                            	<label>CPF</label>
			                            	<?php if ($_smarty_tpl->tpl_vars['cpf']->value!=='') {?>
												<input type="text" id="cpf" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf']->value,'###.###.###-##');?>
">
			                            	<?php } else { ?>
			                            		<input type="text" id="cpf" placeholder="Digite o número do cpf" class="form-control"  autocomplete="yes" data-mascara-campo="cpf" maxlength="14">
			                            	<?php }?>
			                            </div>
			                        </div>    
			                    </div>	
		                	
		                	<?php } else { ?>
		                    
		                       	<div id="pessoa_fisica">
			                    	<div class="row"> 	
		                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                            	<label>CPF</label>
			                            	<?php if ($_smarty_tpl->tpl_vars['cpf']->value!=='') {?>
												<input type="text" id="cpf" placeholder="Digite o número do cpf" class="form-control"data-mascara-campo="cpf" maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cpf']->value,'###.###.###-##');?>
">
			                            	<?php } else { ?>
			                            		<input type="text" id="cpf" placeholder="Digite o número do cpf" class="form-control" data-mascara-campo="cpf" maxlength="14">
			                            	<?php }?>
			                            </div>
			                        </div>    
			                    </div>		
		                	
		                		<div id="pessoa_juridica" style="display:none;">
		                    		<div class="row"> 	
		                       			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	    <label>Razão Social</label>
			                                <input type="text" placeholder="Razão social" class="form-control" id="razao_social" value="<?php echo $_smarty_tpl->tpl_vars['razao_social']->value;?>
">
				                        </div>

				                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                        	    <label>Inscriçao Estadual</label>
			                                <input type="text" placeholder="Inscriçao Estadual" class="form-control" id="inscricao_estadual" value="<?php echo $_smarty_tpl->tpl_vars['inscricao_estadual']->value;?>
">
			                            </div>
			                        		
			                        	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			                           	    <label>CNPJ</label>
				                            <?php if ($_smarty_tpl->tpl_vars['cnpj']->value!='') {?>
												<input type="text" placeholder="CNPJ" class="form-control"  id="cnpj" data-mascara-campo="cnpj" maxlength="18" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cnpj']->value,'##.###.###/####-##');?>
">
			                            	<?php } else { ?>
			                            		<input type="text" placeholder="CNPJ" class="form-control"  id="cnpj" data-mascara-campo="cnpj" maxlength="18">
			                            	<?php }?>
				                        </div>
				                	</div>
			                    </div>
		                	<?php }?>
						
		                	<br>

		                    <div>
		                    	<div class="row"> 	
		                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		                            	<label>Endereço</label>
		                            	<input type="text" id="endereco" placeholder="Digite o endereço" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['endereco']->value;?>
">
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Complemento</label>
		                            	<input type="text" placeholder="Digite o complemento" id="complemento" class="form-control" maxlength="30" value="<?php echo $_smarty_tpl->tpl_vars['complemento']->value;?>
">
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Bairro</label>
		                            	<input type="text" placeholder="Digite o bairro" id="bairro" class="form-control" maxlength="30" value="<?php echo $_smarty_tpl->tpl_vars['bairro']->value;?>
">
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>CEP</label>
		                            	<input type="text" placeholder="Digite o Cep" id="cep" class="form-control" data-mascara-campo='cep' maxlength="9" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['cep']->value,'#####-###');?>
">
		                            </div>
		                    	</div>

		                    	<br>

		                    	<div class="row"> 	
		                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
										<label>Estado</label>
										<select class="form-control" id="estado">
											<option value="<?php echo $_smarty_tpl->tpl_vars['estado']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['estado']->value;?>
</option>
											<option value="AC">AC</option>
											<option value="AL">AL</option>
											<option value="AM">AM</option>
											<option value="AP">AP</option>
											<option value="BA">BA</option>
											<option value="CE">CE</option>
											<option value="ES">ES</option>
											<option value="GO">GO</option>
											<option value="MA">MA</option>
											<option value="MG">MG</option>
											<option value="MS">MS</option>
											<option value="MT">MT</option>
											<option value="PA">PA</option>
											<option value="PB">PB</option>
											<option value="PE">PE</option>
											<option value="PI">PI</option>
											<option value="PR">PR</option>
											<option value="RJ">RJ</option>
											<option value="RN">RN</option>
											<option value="RO">RO</option>
											<option value="RR">RR</option>
											<option value="RS">RS</option>
											<option value="SC">SC</option>
											<option value="SE">SE</option>
											<option value="SP">SP</option>
											<option value="TO">TO</option>
										 </select>
									</div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Cidade</label>
		                            	<input type="text" placeholder="Digite a Cidade" id="cidade" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['cidade']->value;?>
">
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Telefone</label>
		                            	<?php if ($_smarty_tpl->tpl_vars['telefone']->value!=='') {?>
		                            		<input type="text" placeholder="Digite o Telefone" id="telefone" class="form-control" data-mascara-campo='telefone' maxlength="14" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['telefone']->value,'(##) ####-####');?>
">
		                            	<?php } else { ?>	
		                            		<input type="text" placeholder="Digite o Telefone" id="telefone" class="form-control" data-mascara-campo='telefone' maxlength="14">
		                            	<?php }?>	
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Celular</label>
		                            	<?php if ($_smarty_tpl->tpl_vars['celular']->value!=='') {?>
		                            		<input type="text" placeholder="Digite o Telefone" id="celular" class="form-control" data-mascara-campo='celular' maxlength="15" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['celular']->value,'(##) #####-####');?>
">
		                            	<?php } else { ?>		
		                            		<input type="text" placeholder="Digite o Telefone" id="celular" class="form-control" data-mascara-campo='celular' maxlength="15">	                  
		                            	<?php }?>
		                            </div>

		                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		                            	<label>Fax</label>
		                            	<?php if ($_smarty_tpl->tpl_vars['fax']->value!=='') {?>
		                            		<input type="text" placeholder="Digite o Telefone" id="fax" class="form-control" data-mascara-campo='celular' maxlength="15" value="<?php echo smarty_modifier_mask($_smarty_tpl->tpl_vars['fax']->value,'(##) #####-####');?>
">
		                            	<?php } else { ?>		
		                            		<input type="text" placeholder="Digite o Telefone" id="fax" class="form-control" data-mascara-campo='celular' maxlength="15">	                  
		                            	<?php }?>
		                            </div>
		                        </div>

		                        <br>

		                    	<div class="row"> 	
		                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                            	<label>E-mail</label>
		                            	<input type="text" placeholder="Digite o e-mail" id="email" class="form-control" maxlength="80" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
">
		                            </div>

		                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
										<label>Observações</label>
			                            <textarea id="observacoes" class="form-control"><?php echo $_smarty_tpl->tpl_vars['observacoes']->value;?>
</textarea>
			                        </div>	
		            	        </div>
							</div>	                                                            
						</div>
					</div>
				</div><!--dados cadastrais-->
			
				<div class="tab-pane" id="produtos">
	                <br>    
	                <div class="row">    
	                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                            <div class="row">
	                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
	                                    <label>Produtos</label>
	                                    <select class="form-control" id="dados_item">
	                                        <?php if ($_smarty_tpl->tpl_vars['id_produto_list']->value!=='0') {?>
	                                            <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_produto_list']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
	                                                <option value="<?php echo $_smarty_tpl->tpl_vars['id_produto_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
|<?php echo $_smarty_tpl->tpl_vars['nome_produto_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
	                                                    <?php echo $_smarty_tpl->tpl_vars['nome_produto_list']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

	                                                </option>
	                                            <?php endfor; endif; ?>
	                                        <?php } else { ?>
	                                            <option value="">Não foi possível localizar nenhum produto</option>
	                                        <?php }?>
	                                    </select>
	                                </div>

	                                <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
	                                    <br>
	                                    <button class="btn btn-primary" id="adicionar_item" title="adicionar">
	                                        &nbsp;<b>+</b>&nbsp;
	                                    </button>
	                                </div>  
	                            </div>
	                        </div>
	                        
	                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                            <div class="row">
	                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                                    <br>
	                                    <table class="table table-striped table-bordered table-hover">
	                                        <thead>
	                                            <tr>
	                                                <th width="90%">Produto</th>
	                                                <th width="10%">Ação</th>
	                                            </tr>
	                                        </thead>

	                                        <tbody id="grid_itens">
	                                        	<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_fornecedor_produto']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
													<tr>
														<td><?php echo $_smarty_tpl->tpl_vars['nome_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
														<td>
															<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/excluirFornecedorProduto/<?php echo $_smarty_tpl->tpl_vars['id_fornecedor_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
?id_fornecedor=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="excluir-item-bd">
																<span class="glyphicon glyphicon-trash">
																	
																</span>
															</a>
														</td>
													</tr>
												<?php endfor; endif; ?>	
	                                        </tbody>
	                                    </table>
	                                </div> 
	                            </div>
	                        </div>     
	                    </div> <!--12-->   
	                </div> <!--row-->
            	</div>  <!-- produtos --> 			
		
			<br>
			<br>
			
			<?php echo $_smarty_tpl->getSubTemplate ("botoes-submit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
