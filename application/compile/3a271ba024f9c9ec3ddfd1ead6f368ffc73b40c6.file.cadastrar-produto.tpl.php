<?php /* Smarty version Smarty-3.1.16, created on 2014-08-26 22:05:23
         compiled from "/opt/lampp/htdocs/rr/application/views/cadastrar-produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:190255241653fcd5a2909a86-05660229%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3a271ba024f9c9ec3ddfd1ead6f368ffc73b40c6' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/cadastrar-produto.tpl',
      1 => 1409083521,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '190255241653fcd5a2909a86-05660229',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53fcd5a2935e57_94907698',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fcd5a2935e57_94907698')) {function content_53fcd5a2935e57_94907698($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar produto"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/produto.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo produto</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto/fazerCadastro" method="post" id="formulario">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Status</label>
                            	<select class="form-control" name="status">
                                	<option value="Ativo">Ativo</option>
                                	<option value="Inativo">Inativo</option>	                             
                                </select>
                        </div>
                    
                    	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                			<label>Nome</label>
                        	<input type="text" placeholder="Nome do produto" class="form-control" name="nome" maxlength="80" autofocus="yes" autocomplete="yes">
                        	<span class='alerta_formulario' name='alerta_nome'></span>
                		</div>

                		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                			<label>Quantidade Mínima</label>
                        	<input type="text" placeholder="Quantidade Mínima" class="form-control" name="quantidade_minima" maxlength="80" autofocus="yes" autocomplete="yes">
                        	<span class='alerta_formulario' name='alerta_quantidade_minima'></span>
                		</div>

                		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<label>Categoria</label>
                            	<select class="form-control" name="categoria">
                                	<option value="Outros">Outros</option>
                                	<option value="Arquitetônico">Arquitetônico</option>
                                	<option value="Estrutura">Estrutura</option>
                                	<option value="Instalações">Instalações</option>	                             
                                </select>
                        </div>
                    </div>
                    
                    <br>

                    <div class="row">    
                		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                			<label>Observações</label>
                        	<textarea name="observacoes" class="form-control"></textarea>
                		</div>
                	</div>                                                            
				</div>
			</div>
			<br>
			<br>
			<input type="submit" value="&nbsp;&nbsp;Salvar&nbsp;&nbsp;" class="btn btn-primary">
			&nbsp;&nbsp;
			<a href="javascript:history.go(-1)" class="btn btn-primary">Cancelar</a>
		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
