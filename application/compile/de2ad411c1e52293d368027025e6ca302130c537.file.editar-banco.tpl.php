<?php /* Smarty version Smarty-3.1.16, created on 2014-08-26 19:07:13
         compiled from "/opt/lampp/htdocs/rr/application/views/editar-banco.tpl" */ ?>
<?php /*%%SmartyHeaderCode:132823645553fcbec1797965-65616811%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'de2ad411c1e52293d368027025e6ca302130c537' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/editar-banco.tpl',
      1 => 1402921522,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '132823645553fcbec1797965-65616811',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
    'codigo_banco' => 0,
    'status_banco' => 0,
    'nome_banco' => 0,
    'agencia_banco' => 0,
    'conta_banco' => 0,
    'operacao_banco' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53fcbec17e0636_46863082',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fcbec17e0636_46863082')) {function content_53fcbec17e0636_46863082($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Editar banco"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/banco.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Editando banco</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco/fazerEdicao" method="post" id="formulario_editar_banco">
		<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['codigo_banco']->value;?>
" id="codigo_editar_banco">	
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
						<div class="row">
							<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Status</label>
	                            	<select class="form-control" id="status_editar_banco">
	                                	<?php if (($_smarty_tpl->tpl_vars['status_banco']->value==='Ativo')) {?>
	                                		<option value="Ativo">Ativo</option>
		                                	<option value="Inativo">Inativo</option>		                                	
	                                	<?php }?>
	                                	<?php if (($_smarty_tpl->tpl_vars['status_banco']->value==='Inativo')) {?>
	                                		<option value="Inativo">Inativo</option>
		                                	<option value="Ativo">Ativo</option>		                                	
	                                	<?php }?>	                             
	                                </select>
	                        </div>
	                    </div>    

	                    <br>
                		<div class="row"> 	
                   			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    			<label>Nome</label>
                            	<input type="text" placeholder="Nome do Banco" class="form-control" id="nome_editar_banco" maxlength="80" autofocus="yes" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['nome_banco']->value;?>
">
                            	<span class='alerta_formulario' id='alerta_nome_editar_banco'></span>
                    		</div>
                    
                    		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            	    <label>Agência</label>
	                                <input type="text" placeholder="Agência" class="form-control" id="agencia_editar_banco" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['agencia_banco']->value;?>
">
	                                <span class='alerta_formulario' id='alerta_agencia_editar_banco'></span>
                        	</div>
                        
                        		
                        	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                           	    <label>Conta</label>
	                            <input type="text" placeholder="Conta" class="form-control"  id="conta_editar_banco" maxlength="11" autocomplete="yes" value="<?php echo $_smarty_tpl->tpl_vars['conta_banco']->value;?>
">
	                            <span class='alerta_formulario' id='alerta_conta_editar_banco'></span>
	                        </div>

	                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
								<label>Operacao</label>
	                            	<select class="form-control" id="operacao_editar_banco">
	                                	<?php if (($_smarty_tpl->tpl_vars['operacao_banco']->value==='Físico')) {?>
	                                		<option value="Físico">Físico</option>
		                                	<option value="Jurídico">Jurídico</option>
		                                	<option value="Poupança">Poupança</option>		                         	
	                                	<?php }?>
	                                	<?php if (($_smarty_tpl->tpl_vars['operacao_banco']->value==='Jurídico')) {?>
		                                	<option value="Jurídico">Jurídico</option>   <option value="Físico">Físico</option>
		                                	<option value="Poupança">Poupança</option>		                                	
	                                	<?php }?>
	                                	<?php if (($_smarty_tpl->tpl_vars['operacao_banco']->value==='Poupança')) {?>
		                                	<option value="Poupança">Poupança</option>	
		                                	<option value="Jurídico">Jurídico</option>   <option value="Físico">Físico</option>
	                                	<?php }?>
	                                </select>
	                        </div>
	                	</div>                                                            
					</div>
				</div>
			<br>
			<input type="submit" value="Editar" class="btn btn-primary">
		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
