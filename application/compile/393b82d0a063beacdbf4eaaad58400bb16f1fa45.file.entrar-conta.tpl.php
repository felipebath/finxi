<?php /* Smarty version Smarty-3.1.16, created on 2016-04-23 03:03:51
         compiled from "/opt/lampp/htdocs/wmanager/application/views/entrar-conta.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1304048909540f36c216bbc2-33205885%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '393b82d0a063beacdbf4eaaad58400bb16f1fa45' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/entrar-conta.tpl',
      1 => 1461373430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1304048909540f36c216bbc2-33205885',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_540f36c21955e1_13950675',
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_540f36c21955e1_13950675')) {function content_540f36c21955e1_13950675($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Entrar"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/conta.js" defer></script>

	<section class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-offset-4">
				<div class="panel panel-default">
					
					<header class="panel-heading">
						<h1 class="panel-title">Pesquisar</h1>
					</header>

					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
imovel/filtrar" method="get">
									<label></label>
									<input type="text" name="valor_filtro" placeholder="Digite o endereço a ser pesquisado" class="form-control" autofocus="yes" maxlength="100">
							</div>
						</div>

						<br>			

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="submit" class="btn btn-primary" value="Pesquisar">
								</form>
							</div>
						</div>
					</div>

					<header class="panel-heading">
						<h1 class="panel-title">Login</h1>
					</header>

					<div class="panel-body">
						<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/entrar" method="post" id="formulario_entrar_conta">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label>Usuário</label>
									<input type="text" id="usuario_entrar_conta" placeholder="Digite seu nome de usuário" class="form-control" autofocus="yes" autocomplete="yes" maxlength="">
									<span id="alerta_usuario_entrar_conta" class="alerta_formulario"></span>
								</div>
							</div>

							<br>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label>Senha</label>
									<input type="password" id="senha_entrar_conta" placeholder="Digite sua senha" class="form-control" autocomplete="yes">
									<span id="alerta_senha_entrar_conta" class="alerta_formulario"></span>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<br>
									<input type="submit" class="btn btn-primary" value="Entrar">
								</div>
							</div>
						</form>		
					</div>
				</div>
			</div>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
