<?php /* Smarty version Smarty-3.1.16, created on 2014-09-18 17:10:35
         compiled from "/opt/lampp/htdocs/wmanager/application/views/relatorio-contas-a-pagar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:763820569541aed889eebd9-28419198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ecfbc05ddfcfd826866b9e5b61469eb199174b23' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/relatorio-contas-a-pagar.tpl',
      1 => 1411071025,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '763820569541aed889eebd9-28419198',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_541aed88a71c29_95659533',
  'variables' => 
  array (
    'status_filtro' => 0,
    'base_url' => 0,
    'data_criacao' => 0,
    'hora_criacao' => 0,
    'data_inicio_filtro' => 0,
    'data_final_filtro' => 0,
    'tipo_data_filtro' => 0,
    'id_contas_a_pagar' => 0,
    'fornecedor_checkbox' => 0,
    'nota_fiscal_checkbox' => 0,
    'data_emissao_checkbox' => 0,
    'banco_checkbox' => 0,
    'centro_de_custo_checkbox' => 0,
    'boleto_checkbox' => 0,
    'data_vencimento_checkbox' => 0,
    'status_checkbox' => 0,
    'data_pagamento_checkbox' => 0,
    'forma_checkbox' => 0,
    'numero_cheque_checkbox' => 0,
    'valor_checkbox' => 0,
    'juros_checkbox' => 0,
    'credito_checkbox' => 0,
    'valor_total_checkbox' => 0,
    'observacoes_checkbox' => 0,
    'nome_fornecedor' => 0,
    'nota_fiscal_contas_a_pagar' => 0,
    'data_emissao_contas_a_pagar' => 0,
    'nome_banco' => 0,
    'nome_centro_de_custo' => 0,
    'boleto_contas_a_pagar' => 0,
    'data_vencimento_contas_a_pagar' => 0,
    'status_contas_a_pagar' => 0,
    'data_pagamento_contas_a_pagar' => 0,
    'forma_contas_a_pagar' => 0,
    'numero_cheque_contas_a_pagar' => 0,
    'valor_contas_a_pagar' => 0,
    'juros_contas_a_pagar' => 0,
    'credito_contas_a_pagar' => 0,
    'valor_total_contas_a_pagar' => 0,
    'observacoes_contas_a_pagar' => 0,
    'id_contas_a_pagar_subtotal' => 0,
    'nome_centro_de_custo_subtotal' => 0,
    'valor_total_contas_a_pagar_subtotal' => 0,
    'somatorio_valor_total_contas_a_pagar' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_541aed88a71c29_95659533')) {function content_541aed88a71c29_95659533($_smarty_tpl) {?><!DOCTYPE html>
<html lang="pt-br">
<head>
<title>Alljob - Relatório - <?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
</title>
<meta charset="UTF-8">
<link href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/css/relatorio.css" rel="stylesheet">
</head>

<body>
    <header>
        <table class="table-header">
            <tr>
                <td class="td1-table-header"><h1>Relatório - <?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
</h1></td>
                <td class="td2-table-header"><span>Gerado em <b><?php echo $_smarty_tpl->tpl_vars['data_criacao']->value;?>
</b> as <?php echo $_smarty_tpl->tpl_vars['hora_criacao']->value;?>
 hs</span></td>
            </tr>
        </table>
    </header>
    
    <table>
        <tr>
            <td>
                <?php if ($_smarty_tpl->tpl_vars['data_inicio_filtro']->value===''&&$_smarty_tpl->tpl_vars['data_final_filtro']->value==='') {?>
                    Período: <b>Não informado</b>
                <?php } else { ?>
                    Período: <b><?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
</b> a <b><?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
</b>
                <?php }?>
                &nbsp;|&nbsp;       
            </td>

            <td>Orientação: <b><?php echo $_smarty_tpl->tpl_vars['tipo_data_filtro']->value;?>
</b></td>
        </tr>
    </table>
        
    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_contas_a_pagar']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
    
    <br>

        <table class="table-grid">
            <tr>
                <td class="col-line-divisor-center"><b>Cod</b></td>
                
                <?php if ($_smarty_tpl->tpl_vars['fornecedor_checkbox']->value==='on') {?>    
                    <td class="col-line-divisor-center"><b>Fornecedor</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['nota_fiscal_checkbox']->value==='on') {?>    
                    <td class="col-line-divisor-center"><b>Nota Fiscal</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['data_emissao_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Emissão</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['banco_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Banco</b></td>
                <?php }?>
                
                <?php if ($_smarty_tpl->tpl_vars['centro_de_custo_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Centro de Custo</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['boleto_checkbox']->value==='on') {?>    
                    <td class="col-line-divisor-center"><b>Boleto</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['data_vencimento_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Vencimento</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['status_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Status</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['data_pagamento_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Pagamento</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['forma_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Forma de Pgto.</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['numero_cheque_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Cheque</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['valor_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Valor</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['juros_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Juros</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['credito_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Crédito</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['valor_total_checkbox']->value==='on') {?>
                    <td class="col-line-divisor-center"><b>Valor Total</b></td>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['observacoes_checkbox']->value==='on') {?>
                    <td class="line-divisor-center"><b>Observações</b></td>
                <?php }?>
            </tr>

            <tr>
                <?php if ($_smarty_tpl->tpl_vars['id_contas_a_pagar']->value!=='0') {?>
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_contas_a_pagar']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                        <tr>
                            <td class="col-line-divisor-center">
                                <?php echo $_smarty_tpl->tpl_vars['id_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                            </td>    
                            
                            <?php if ($_smarty_tpl->tpl_vars['fornecedor_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-justify">
                                    <?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                </td>
                            <?php }?>
                            
                            <?php if ($_smarty_tpl->tpl_vars['nota_fiscal_checkbox']->value==='on') {?>    
                                <td class="col-line-divisor-center">
                                    <?php echo $_smarty_tpl->tpl_vars['nota_fiscal_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['data_emissao_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php if ($_smarty_tpl->tpl_vars['data_emissao_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='00/00/0000') {?>
                                        <?php echo $_smarty_tpl->tpl_vars['data_emissao_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                    <?php }?>   
                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['banco_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <div align="left">
                                        <?php echo $_smarty_tpl->tpl_vars['nome_banco']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                    </div>
                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['centro_de_custo_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                </td>
                            <?php }?>    

                            <?php if ($_smarty_tpl->tpl_vars['boleto_checkbox']->value==='on') {?>    
                                <td class="col-line-divisor-center">
                                    <?php echo $_smarty_tpl->tpl_vars['boleto_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['data_vencimento_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php if ($_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='00/00/0000') {?>
                                        <?php echo $_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                    <?php }?>
                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['status_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php echo $_smarty_tpl->tpl_vars['status_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['data_pagamento_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php if ($_smarty_tpl->tpl_vars['data_pagamento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='00/00/0000') {?>
                                        <?php echo $_smarty_tpl->tpl_vars['data_pagamento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                    <?php }?>   
                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['forma_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php echo $_smarty_tpl->tpl_vars['forma_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['numero_cheque_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php echo $_smarty_tpl->tpl_vars['numero_cheque_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                </td>
                            <?php }?>   

                            <?php if ($_smarty_tpl->tpl_vars['valor_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php echo number_format($_smarty_tpl->tpl_vars['valor_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>

                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['juros_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php echo number_format($_smarty_tpl->tpl_vars['juros_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>

                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['credito_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php echo number_format($_smarty_tpl->tpl_vars['credito_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>

                                </td>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['valor_total_checkbox']->value==='on') {?>
                                <td class="col-line-divisor-center">
                                    <?php echo number_format($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>

                                </td>
                            <?php }?>
                            
                            <?php if ($_smarty_tpl->tpl_vars['observacoes_checkbox']->value==='on') {?>
                                <td class="line-divisor-justify">
                                    <?php echo $_smarty_tpl->tpl_vars['observacoes_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                                </td>
                            <?php }?>
                                
                        </tr>
                    <?php endfor; endif; ?>
                <?php } else { ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
    
        <!--subtotal-->
        <table class="table-subtotal">
            <tr>
                <td></td>
                <td><b>Subtotal (por centro de custo)</b></td>
            </tr>

            <tr>
                <?php if ($_smarty_tpl->tpl_vars['id_contas_a_pagar_subtotal']->value!=='0') {?>
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_contas_a_pagar_subtotal']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                        <tr>
                            <td width="85%">
                                <?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo_subtotal']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

                            </td>

                            <td>
                                R$ <?php echo number_format($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar_subtotal']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']],2,',','.');?>

                            </td>


                        </tr>
                    <?php endfor; endif; ?>
                <?php } else { ?>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                <?php }?>
            </tr>
        </table>

        <!--total geral-->
        <table class="table-total-geral">
            <tr>
                <td>
                    <div align="right">
                        <b>Total Geral</b>
                    </div>
                </td>
            </tr>

            <tr>
                <tr>
                    <td align="right">R$ <?php echo $_smarty_tpl->tpl_vars['somatorio_valor_total_contas_a_pagar']->value;?>
</td>
                </tr>
            </tr>
        </table>
    <?php endfor; endif; ?>

</body>
</html>
<?php }} ?>
