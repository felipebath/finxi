<?php /* Smarty version Smarty-3.1.16, created on 2014-11-04 20:38:22
         compiled from "/opt/lampp/htdocs/wmanager/application/views/resultado-filtro-contas-a-pagar2.tpl" */ ?>
<?php /*%%SmartyHeaderCode:61699899954526ecf7ecfe8-32105589%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a322057ded7e46068f9d0e2072671e5e274ad7d0' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/resultado-filtro-contas-a-pagar2.tpl',
      1 => 1415129897,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '61699899954526ecf7ecfe8-32105589',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54526ecf903ac0_18661726',
  'variables' => 
  array (
    'base_url' => 0,
    'permissao_cadastrar_financeiro_conta' => 0,
    'id_fornecedor_filtro' => 0,
    'id_centro_de_custo_filtro' => 0,
    'status_filtro' => 0,
    'tipo_data_filtro' => 0,
    'data_inicio_filtro' => 0,
    'data_final_filtro' => 0,
    'nome_fornecedor_filtro' => 0,
    'id_fornecedor_list_filtro' => 0,
    'nome_fornecedor_list_filtro' => 0,
    'nome_centro_de_custo_filtro' => 0,
    'id_centro_de_custo_list_filtro' => 0,
    'nome_centro_de_custo_list_filtro' => 0,
    'id_contas_a_pagar' => 0,
    'data_vencimento_contas_a_pagar' => 0,
    'valor_total_contas_a_pagar' => 0,
    'nome_fornecedor' => 0,
    'boleto_contas_a_pagar' => 0,
    'data_emissao_contas_a_pagar' => 0,
    'nome_centro_de_custo' => 0,
    'observacoes_contas_a_pagar' => 0,
    'permissao_editar_financeiro_conta' => 0,
    'id_centro_de_custo' => 0,
    'subtotal_contas_a_pagar' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54526ecf903ac0_18661726')) {function content_54526ecf903ac0_18661726($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Contas a pagar"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/contas_a_pagar.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<h1>Contas a pagar</h1>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class='btn-group pull-right'>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">		
							<button id="exibir_checkboxes" class="btn btn-primary">
								Relatório
							</button>	
						</div>

						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/" class="pull-right btn btn-primary" title="Listar todos">
								&nbsp;Listar todos&nbsp;
							</a>
						</div>

						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_financeiro_conta']->value==='1') {?>
								<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/cadastrar" class="pull-right btn btn-primary" title="Cadastrar">		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Novo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</a>
							<?php }?>
						</div>
					</div>	
				</div>
			</div>

			<div id="checkboxes_relatorio" style="display:none;">
            	<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/gerarRelatorio2" target="blank">
							
	            	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
	               			<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
								<br>
									<div id="mensagem">
										Selecione a(s) coluna(s) a ser(em) exibida(s) no relatório:
									</div>
							</div>
						</div>

						<br>			

	               		<div class="row">
	               			<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Fornecedor &nbsp;<input type="checkbox" name="fornecedor" checked></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Nota fiscal	&nbsp;<input type="checkbox" name="nota_fiscal"></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Data de emissão &nbsp;<input type="checkbox" name="data_emissao" checked></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Banco &nbsp;<input type="checkbox" name="banco"></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Centro de Custo &nbsp;<input type="checkbox" name="centro_de_custo" checked></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Boleto &nbsp;<input type="checkbox" name="boleto" checked></label>
							</div>
						</div>
						
						<br>	

						<div class="row">
							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Dt. Vencim. &nbsp;<input type="checkbox" name="data_vencimento" checked></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Status &nbsp;<input type="checkbox" name="status"></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Dt. Pgto. &nbsp;<input type="checkbox" name="data_pagamento"></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Forma de pgto. &nbsp;<input type="checkbox" name="forma"></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Número Cheque &nbsp;<input type="checkbox" name="numero_cheque"></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Valor &nbsp;<input type="checkbox" name="valor" checked></label>
							</div>
						</div>

						<br>

						<div class="row">
							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Juros &nbsp;<input type="checkbox" name="juros" checked></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Crédito &nbsp;<input type="checkbox" name="credito" checked></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Observacoes &nbsp;<input type="checkbox" name="observacoes"></label>
							</div>

							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2'>
								<label>Valor total &nbsp;<input type="checkbox" name="valor_total" checked></label>
							</div>
						</div>

						<div class="row">
							<div class='col-xs-12 col-sm-2 col-md-2 col-lg-2 pull-right'>
								<input type="hidden" name="id_fornecedor_filtro" value="<?php echo $_smarty_tpl->tpl_vars['id_fornecedor_filtro']->value;?>
">
								<input type="hidden" name="id_centro_de_custo_filtro" value="<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo_filtro']->value;?>
">
								<input type="hidden" name="status_filtro" value="<?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
">
								<input type="hidden" name="tipo_data_filtro" value="<?php echo $_smarty_tpl->tpl_vars['tipo_data_filtro']->value;?>
">
								<input type="hidden" name="data_inicio_filtro" value="<?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
">
								<input type="hidden" name="data_final_filtro" value="<?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
">
								<input type="submit" value="Gerar Relatório" class="btn btn-primary pull-right">
							</div>	
						</div>

						<br>
						<br>

					</div>
				</form>	
			</div>

			
		</header>

		<div class="table-responsive">	
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/filtrar2" id="formulario_filtrar_contas_a_pagar">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="row">
								    
				                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			                    	<select class="form-control" name="id_fornecedor_filtro">
			                        	<option value="<?php echo $_smarty_tpl->tpl_vars['id_fornecedor_filtro']->value;?>
">
			                        		<?php if ($_smarty_tpl->tpl_vars['nome_fornecedor_filtro']->value==='Todos') {?>
			                        			Fornecedores (TODOS)
			                        		<?php } else { ?>
			                        			<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor_filtro']->value;?>

			                        			<option value="Todos">Fornecedores (TODOS)</option>
			                        		<?php }?>	
			                        	</option>
			                        	
			                        	<?php if ($_smarty_tpl->tpl_vars['id_fornecedor_list_filtro']->value!=='0') {?>
		                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_fornecedor_list_filtro']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_fornecedor_list_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">
		                                        	<?php echo $_smarty_tpl->tpl_vars['nome_fornecedor_list_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                        </option>
		                                    <?php endfor; endif; ?>
		                                <?php } else { ?>
		                                    <option value="">Não foi possível localizar nenhum fornecedor</option>
		                                <?php }?>
		                            </select>
		                        </div>

			                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			                    	<select class="form-control" name="id_centro_de_custo_filtro">
			                        	<option value="<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo_filtro']->value;?>
">
			                        		<?php if ($_smarty_tpl->tpl_vars['nome_centro_de_custo_filtro']->value==='Todos') {?>
			                        			Centro de Custo (TODOS)
			                        		<?php } else { ?>
			                        			<?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo_filtro']->value;?>

			                        			<option value="Todos">Centro de Custo (TODOS)</option>
			                        		<?php }?>	
			                        	</option>
			                        	
			                        	<?php if ($_smarty_tpl->tpl_vars['id_centro_de_custo_list_filtro']->value!=='0') {?>
		                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_centro_de_custo_list_filtro']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		                                        <option value="<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo_list_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
">	
		                                        	<?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo_list_filtro']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		                                        </option>
		                                    <?php endfor; endif; ?>
		                                <?php } else { ?>
		                                    <option value="">Não foi possível localizar nenhum centro de custo</option>
		                                <?php }?>
		                            </select>
		                        </div>
					            
					            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<select id='status_filtro' name='status_filtro' class="form-control">
			               				<?php if ($_smarty_tpl->tpl_vars['status_filtro']->value==='A pagar') {?>
			               					<option value="A pagar">A pagar</option>
			               					<option value="Pago">Pago</option>
			               				<?php } else { ?>
											<option value="Pago">Pago</option>
											<option value="A pagar">A pagar</option>
			               				<?php }?>
			               			</select>
								</div>

								<div id="status_pago_filtro" style="display:none;">
									<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<select name='tipo_data_filtro' class="form-control">
				               				<?php if ($_smarty_tpl->tpl_vars['tipo_data_filtro']->value==='vencimento') {?>
				               					<option value="vencimento">Vencimento</option>
				               					<option value="pagamento">Pagamento</option>
				               				<?php } else { ?>
												<option value="pagamento">Pagamento</option>
				               					<option value="vencimento">Vencimento</option>
				               				<?php }?>
				               			</select>
									</div>
								</div>	

								<div id="status_a_pagar_filtro">
									<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<select name='tipo_data_filtro' class="form-control" disabled="disabled">
				               				<option value="vencimento">Vencimento</option>
				               			</select>
									</div>
								</div>	

								<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<input type="text" class="form-control calendario" name="data_inicio_filtro" placeholder="Data inicial" maxlength="10" data-mascara-campo='data' value="<?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
">
								</div>

			               		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<input type="text" class="form-control calendario" name="data_final_filtro" placeholder="Data final" maxlength="10" data-mascara-campo='data' value="<?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
">
								</div>
							</div>
							
							<br>
							
							<div class="row">
								<div class='col-xs-12 col-sm-1 col-md-1 col-lg-1'>
									<input type='submit' value='Filtrar' class='pull-left btn btn-primary'>
								</div>
							</div>
						</div>
					</div>	
				</form>
			</div>
		
		<br>	

		<div class="table-responsive">
			
			<table class='table table-hover table-condensed'>
				<thead>
					<th>Código</th>
					<th>Fornecedor</th>
					<th>Boleto</th>
					<th>Data Emissão</th>
					<th>Vencimento</th>
					<th>Valor Total</th>
					<th>Centro de Custo</th>
					<th>Observações</th>
					<th>Editar</th>
				</thead>

				<tbody>
					<?php if ($_smarty_tpl->tpl_vars['id_contas_a_pagar']->value!=='0') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['id_contas_a_pagar']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							
							<?php if ($_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='01/01/1970') {?>
								<?php if ($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='0,00') {?>
									<tr>
										<td><?php echo $_smarty_tpl->tpl_vars['id_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>	
																		
										<td><?php echo $_smarty_tpl->tpl_vars['nome_fornecedor']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
										
										<td><?php echo $_smarty_tpl->tpl_vars['boleto_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
										
										<td><?php echo $_smarty_tpl->tpl_vars['data_emissao_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
										
										<td><?php echo $_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
										
										<td><?php echo $_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

										<td><?php echo $_smarty_tpl->tpl_vars['nome_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>

										<td><?php echo $_smarty_tpl->tpl_vars['observacoes_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
										
										<td>
											<?php if ($_smarty_tpl->tpl_vars['permissao_editar_financeiro_conta']->value==='1') {?>
												<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/editar/<?php echo $_smarty_tpl->tpl_vars['id_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
?id_fornecedor_filtro=<?php echo $_smarty_tpl->tpl_vars['id_fornecedor_filtro']->value;?>
&id_centro_de_custo_filtro=<?php echo $_smarty_tpl->tpl_vars['id_centro_de_custo_filtro']->value;?>
&status_filtro=<?php echo $_smarty_tpl->tpl_vars['status_filtro']->value;?>
&tipo_data_filtro=<?php echo $_smarty_tpl->tpl_vars['tipo_data_filtro']->value;?>
&data_inicio_filtro=<?php echo $_smarty_tpl->tpl_vars['data_inicio_filtro']->value;?>
&data_final_filtro=<?php echo $_smarty_tpl->tpl_vars['data_final_filtro']->value;?>
">
													<span class="glyphicon glyphicon glyphicon-edit" title="editar"></span>
												</a>
											<?php }?>
										</td>
									</tr>
								<?php }?>	
							<?php }?>
							
							<?php if ($_smarty_tpl->tpl_vars['data_vencimento_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='01/01/1970') {?> 
								<?php if ($_smarty_tpl->tpl_vars['valor_total_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]=='0,00') {?> 
									<tr style='font-weight:bold;'>
										<td></td>	
										<td></td>
										<td></td>
										<td></td>
										<td>
											<?php if ($_smarty_tpl->tpl_vars['id_centro_de_custo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]!='') {?>
												SUBTOTAL
											<?php } else { ?>
												TOTAL GERAL
											<?php }?>
										</td>
										<td><?php echo $_smarty_tpl->tpl_vars['subtotal_contas_a_pagar']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</td>
										<td></td>	
										<td></td>
										<td></td>
									</tr>

									<tr>
										<td>&nbsp;</td>	
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>	
										<td></td>
										<td></td>
									</tr>
								<?php }?>
							<?php }?>	
						<?php endfor; endif; ?>
					<?php } else { ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
