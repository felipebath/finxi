<?php /* Smarty version Smarty-3.1.16, created on 2014-08-26 19:56:47
         compiled from "/opt/lampp/htdocs/rr/application/views/menu-2.tpl" */ ?>
<?php /*%%SmartyHeaderCode:110062566653fc992b0a6d10-07132827%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '42d4f95b03673f83ef730d1b1cf9f8ba288eb851' => 
    array (
      0 => '/opt/lampp/htdocs/rr/application/views/menu-2.tpl',
      1 => 1409075806,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '110062566653fc992b0a6d10-07132827',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53fc992b0fb393_17770516',
  'variables' => 
  array (
    'base_url' => 0,
    'nome_empresa_sessao' => 0,
    'permissao_cadastrar_banco_conta' => 0,
    'permissao_visualizar_banco_conta' => 0,
    'permissao_editar_banco_conta' => 0,
    'permissao_excluir_banco_conta' => 0,
    'permissao_cadastrar_centro_custo_conta' => 0,
    'permissao_visualizar_centro_custo_conta' => 0,
    'permissao_editar_centro_custo_conta' => 0,
    'permissao_excluir_centro_custo_conta' => 0,
    'permissao_cadastrar_cliente_conta' => 0,
    'permissao_visualizar_cliente_conta' => 0,
    'permissao_editar_cliente_conta' => 0,
    'permissao_excluir_cliente_conta' => 0,
    'permissao_cadastrar_conta_conta' => 0,
    'permissao_visualizar_conta_conta' => 0,
    'permissao_editar_conta_conta' => 0,
    'permissao_excluir_conta_conta' => 0,
    'permissao_cadastrar_financeiro_conta' => 0,
    'permissao_visualizar_financeiro_conta' => 0,
    'permissao_editar_financeiro_conta' => 0,
    'permissao_excluir_financeiro_conta' => 0,
    'permissao_cadastrar_fornecedor_conta' => 0,
    'permissao_visualizar_fornecedor_conta' => 0,
    'permissao_editar_fornecedor_conta' => 0,
    'permissao_excluir_fornecedor_conta' => 0,
    'usuario_conta_sessao' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fc992b0fb393_17770516')) {function content_53fc992b0fb393_17770516($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include '/opt/lampp/htdocs/rr/application/libraries/Smarty-3.1.16/libs/plugins/modifier.capitalize.php';
?><nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="menu-principal">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#ul-menu-principal">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		     </button>

			<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
inicio" class="navbar-brand" title="Gadministrativo">G-Administrativo - <?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nome_empresa_sessao']->value);?>
</a>
		</div> <!-- cabecalho do menu -->

		<div class="collapse navbar-collapse" id="ul-menu-principal">
			<ul class="nav navbar-nav navbar-left">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Cadastros">Cadastros <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<!--<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_banco_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_visualizar_banco_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_editar_banco_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_excluir_banco_conta']->value!=='0') {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
banco" title="Listar / Filtrar">Banco</a></li>
						<?php }?>
						
						<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_centro_custo_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_visualizar_centro_custo_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_editar_centro_custo_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_excluir_centro_custo_conta']->value!=='0') {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
centro_de_custo/" title="Listar / Filtrar">Centro de custo</a></li>
						<?php }?>

						<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_cliente_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_visualizar_cliente_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_editar_cliente_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_excluir_cliente_conta']->value!=='0') {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cliente/" title="Listar / Filtrar">Cliente</a></li>
						<?php }?>

						<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_conta_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_visualizar_conta_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_editar_conta_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_excluir_conta_conta']->value!=='0') {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/listarContas" title="Listar / Filtrar">Contas</a></li>
						<?php }?>

						<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_financeiro_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_visualizar_financeiro_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_editar_financeiro_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value!=='0') {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_pagar/" title="Listar / Filtrar">Contas a pagar</a></li>
						<?php }?>

						<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_financeiro_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_visualizar_financeiro_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_editar_financeiro_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_excluir_financeiro_conta']->value!=='0') {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
contas_a_receber/" title="Listar / Filtrar">Contas a receber</a></li>
						<?php }?>
						
						<?php if ($_smarty_tpl->tpl_vars['permissao_cadastrar_fornecedor_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_visualizar_fornecedor_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_editar_fornecedor_conta']->value!=='0'||$_smarty_tpl->tpl_vars['permissao_excluir_fornecedor_conta']->value!=='0') {?>
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor/" title="Listar / Filtrar">Fornecedor</a></li>
						<?php }?>-->
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/listarContas" title="Listar / Filtrar">Contas</a></li>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
cotacao" title="Listar / Filtrar">Cotação</a></li>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
estoque" title="Listar / Filtrar">Estoque</a></li>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
fornecedor" title="Listar / Filtrar">Fornecedores</a></li>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
obra" title="Listar / Filtrar">Obras</a></li>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
produto" title="Listar / Filtrar">Produtos</a></li>
					</ul>
				</li>

			</ul> <!-- menu esquerdo -->

			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/listarContas" title="usuario">Olá <?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['usuario_conta_sessao']->value);?>
</a></li>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/configuracoes" title="Configurações">Configurações</a></li>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/sair" title="Sair">Sair</a></li>
			</ul> <!-- menu direito -->
		</div>
	</div> <!-- conteudo do menu -->
</nav><?php }} ?>
