<?php /* Smarty version Smarty-3.1.16, created on 2014-05-30 00:09:07
         compiled from "/opt/lampp/htdocs/gadministrativo/application/views/cadastrar-conta.tpl" */ ?>
<?php /*%%SmartyHeaderCode:67962446353721a20b6a237-71827539%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '65a059e73ec9cc78ebef01fb5e81d2be0060bca7' => 
    array (
      0 => '/opt/lampp/htdocs/gadministrativo/application/views/cadastrar-conta.tpl',
      1 => 1401386929,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '67962446353721a20b6a237-71827539',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53721a20bbdca1_16241096',
  'variables' => 
  array (
    'base_url' => 0,
    'codigo_empresa' => 0,
    'permissao_conta_empresa' => 0,
    'permissao_cliente_empresa' => 0,
    'permissao_colaborador_empresa' => 0,
    'permissao_financeiro_empresa' => 0,
    'permissao_servico_empresa' => 0,
    'permissao_produto_empresa' => 0,
    'permissao_fornecedor_empresa' => 0,
    'permissao_estoque_empresa' => 0,
    'permissao_centro_custo_empresa' => 0,
    'permissao_banco_empresa' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53721a20bbdca1_16241096')) {function content_53721a20bbdca1_16241096($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar conta"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/conta.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando conta</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/listarContas" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<nav>
			<ul class="nav nav-tabs">
				<li class="active"><a href="#entrada" title="Entrada" data-toggle="tab">Entrada</a></li>
				<li><a href="#permissoes" title="Permissões" data-toggle="tab">Permissões</a></li>
			</ul>
		</nav>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
conta/fazerCadastro" method="post" id="formulario_cadastrar_conta">

		<div class="tab-content">
			<div class="tab-pane active" id="entrada">
				<br>
				
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['codigo_empresa']->value;?>
" id="codigo_empresa_cadastrar_conta">

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Usuário *</label>
								<input type="text" id="usuario_cadastrar_conta" placeholder="Digite seu nome de usuário" class="form-control" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_usuario_cadastrar_conta"></span>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Senha *</label>
								<input type="password" id="senha_cadastrar_conta" placeholder="Digite sua senha" class="form-control" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_senha_cadastrar_conta"></span>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- entrada -->

			<div class="tab-pane" id="permissoes">
				<br>

				<input type="checkbox" id="selecionar_todos_permissao_cadastrar_conta">
				<span>Selecionar todos</span><br>

				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

						<?php if ($_smarty_tpl->tpl_vars['permissao_conta_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Conta</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_conta" id="permissao_cadastrar_conta_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_conta" id="permissao_editar_conta_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_conta" id="permissao_excluir_conta_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_conta" id="permissao_visualizar_conta_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- conta -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_cliente_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<span>Cliente</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_cliente" id="permissao_cadastrar_cliente_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_cliente" id="permissao_editar_cliente_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_cliente" id="permissao_excluir_cliente_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_cliente" id="permissao_visualizar_cliente_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- cliente -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_colaborador_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Colaborador</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_colaborador" id="permissao_cadastrar_colaborador_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_colaborador" id="permissao_editar_colaborador_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_colaborador" id="permissao_excluir_colaborador_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_colaborador" id="permissao_visualizar_colaborador_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- colaborador -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_financeiro_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Finânceiro</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_financeiro" id="permissao_cadastrar_financeiro_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_financeiro" id="permissao_editar_financeiro_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_financeiro" id="permissao_excluir_financeiro_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_financeiro" id="permissao_visualizar_financeiro_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- financeiro -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_servico_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Serviço</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_servico" id="permissao_cadastrar_servico_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_servico" id="permissao_editar_servico_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_servico" id="permissao_excluir_servico_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_servico" id="permissao_visualizar_servico_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- servico -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_produto_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Produto</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_produto" id="permissao_cadastrar_produto_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_produto" id="permissao_editar_produto_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_produto" id="permissao_excluir_produto_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_produto" id="permissao_visualizar_produto_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- produto -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_fornecedor_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Fornecedor</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_fornecedor" id="permissao_cadastrar_fornecedor_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_fornecedor" id="permissao_editar_fornecedor_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_fornecedor" id="permissao_excluir_fornecedor_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_fornecedor" id="permissao_visualizar_fornecedor_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- fornecedor -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_estoque_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Estoque</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_estoque" id="permissao_cadastrar_estoque_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_estoque" id="permissao_editar_estoque_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_estoque" id="permissao_excluir_estoque_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_estoque" id="permissao_visualizar_estoque_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- estoque -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_centro_custo_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Centro de custo</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_centro_custo" id="permissao_cadastrar_centro_custo_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_centro_custo" id="permissao_editar_centro_custo_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_centro_custo" id="permissao_excluir_centro_custo_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_centro_custo" id="permissao_visualizar_centro_custo_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- centro de custo -->
						<?php }?>

						<br>

						<?php if ($_smarty_tpl->tpl_vars['permissao_banco_empresa']->value==='1') {?>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<span>Banco</span>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_cadastrar_banco" id="permissao_cadastrar_banco_cadastrar_conta">
											<span>Cadastrar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_editar_banco" id="permissao_editar_banco_cadastrar_conta">
											<span>Editar</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_excluir_banco" id="permissao_excluir_banco_cadastrar_conta">
											<span>Excluir</span>
										</div>

										<div class="col-xs-12 col-sm-3 col-md-1 col-lg-1">
											<input type="checkbox" name="permissao_visualizar_banco" id="permissao_visualizar_banco_cadastrar_conta">
											<span>Visualizar</span>
										</div>
									</div>
								</div>
							</div> <!-- estoque -->
						<?php }?>

					</div>
				</div>
			</div> <!-- permissoes -->
		</div>

			<br>
			<input type="submit" value="Cadastrar" class="btn btn-primary">

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
