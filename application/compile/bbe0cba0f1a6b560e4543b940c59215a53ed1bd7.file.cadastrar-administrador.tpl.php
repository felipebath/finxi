<?php /* Smarty version Smarty-3.1.16, created on 2014-09-10 20:10:10
         compiled from "/opt/lampp/htdocs/wmanager/application/views/cadastrar-administrador.tpl" */ ?>
<?php /*%%SmartyHeaderCode:107137428354109402a676e4-30866699%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbe0cba0f1a6b560e4543b940c59215a53ed1bd7' => 
    array (
      0 => '/opt/lampp/htdocs/wmanager/application/views/cadastrar-administrador.tpl',
      1 => 1405712176,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '107137428354109402a676e4-30866699',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_54109402a93572_37116883',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54109402a93572_37116883')) {function content_54109402a93572_37116883($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("cabecalho.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('titulo'=>"Cadastrar administrador"), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("menu-1.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("alertas.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<script src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/assets/js/administrador.js" defer></script>

	<section class="container-fluid">
		<header class="page-header">
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<h1>Cadastrando novo administrador</h1>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
administrador" class="pull-right btn btn-primary" title="Visualizar todos">Visualizar todos</a>
				</div>
			</div>
		</header>

		<nav>
			<ul class="nav nav-tabs">
				<li class="active"><a href="#entrada" title="Entrada" data-toggle="tab">Entrada</a></li>
				<li><a href="#permissoes" title="Permissões" data-toggle="tab">Permissões</a></li>
			</ul>
		</nav>

		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
administrador/fazerCadastro" method="post" id="formulario_cadastrar_administrador">

		<div class="tab-content">
			<div class="tab-pane active" id="entrada">
				<br>
				
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Nome da empresa *</label>
								<input type="text" id="nome_empresa_cadastrar_administrador" placeholder="Digite o nome da empresa" class="form-control" autofocus="yes" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_nome_empresa_cadastrar_administrador"></span>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Usuário *</label>
								<input type="text" id="usuario_cadastrar_administrador" placeholder="Digite seu nome de usuário" class="form-control" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_usuario_cadastrar_administrador"></span>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<label>Senha *</label>
								<input type="password" id="senha_cadastrar_administrador" placeholder="Digite sua senha" class="form-control" autocomplete="yes">
								<span class="alerta_formulario" id="alerta_senha_cadastrar_administrador"></span>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- entrada -->

			<div class="tab-pane" id="permissoes">
				<br>

				<input type="checkbox" id="selecionar_todos_permissao_cadastrar_administrador">
				<span>Selecionar todos</span><br>

				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_conta" id="permissao_conta_cadastrar_administrador">
								<span>Conta</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_produto" id="permissao_produto_cadastrar_administrador">
								<span>Produto</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_financeiro" id="permissao_financeiro_cadastrar_administrador">
								<span>Finânceiro</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_colaborador" id="permissao_colaborador_cadastrar_administrador">
								<span>Colaborador</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_cliente" id="permissao_cliente_cadastrar_administrador">
								<span>Cliente</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_fornecedor" id="permissao_fornecedor_cadastrar_administrador">
								<span>Fonecedor</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_estoque" id="permissao_estoque_cadastrar_administrador">
								<span>Estoque</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_servico" id="permissao_servico_cadastrar_administrador">
								<span>Serviço</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_banco" id="permissao_banco_cadastrar_administrador">
								<span>Banco</span><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="checkbox" name="permissao_centro_custo" id="permissao_centro_custo_cadastrar_administrador">
								<span>Centro de Custo</span><br>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- permissoes -->
		</div>

			<br>
			<input type="submit" value="Cadastrar" class="btn btn-primary">

		</form>
	</section>
<?php echo $_smarty_tpl->getSubTemplate ("rodape.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
