-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 23/04/2016 às 04:28
-- Versão do servidor: 5.5.39
-- Versão do PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `finxi`
--

DELIMITER $$
--
-- Funções
--
CREATE DEFINER=`root`@`localhost` FUNCTION `Geo`(lat_ini FLOAT(18,10), lon_ini FLOAT(18,10),lat_fim FLOAT(18,10), lon_fim FLOAT(18,10)) RETURNS float(18,10)
BEGIN
DECLARE Theta FLOAT(18,10);
DECLARE Dist FLOAT(18,10);
DECLARE Miles FLOAT(18,10);
DECLARE kilometers FLOAT(18,10);

SET Theta = lon_ini - lon_fim;
SET Dist  = SIN(RADIANS(lat_ini)) * SIN(RADIANS(lat_fim)) +  COS(RADIANS(lat_ini)) * COS(RADIANS(lat_fim)) * COS(RADIANS(Theta));
SET Dist  = ACOS(Dist);
SET Dist  = DEGREES(Dist);
SET Miles = Dist * 60 * 1.1515;
SET kilometers = Miles * 1.609344;

RETURN kilometers;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `conta`
--

CREATE TABLE IF NOT EXISTS `conta` (
`codigo_conta` int(11) NOT NULL,
  `codigo_empresa_conta` int(11) NOT NULL,
  `status_conta` int(1) NOT NULL,
  `usuario_conta` text COLLATE utf8_unicode_ci NOT NULL,
  `senha_conta` text COLLATE utf8_unicode_ci NOT NULL,
  `permissao_administrador_conta` int(1) NOT NULL,
  `permissao_cadastrar_cliente_conta` int(1) NOT NULL,
  `permissao_visualizar_cliente_conta` int(1) NOT NULL,
  `permissao_editar_cliente_conta` int(1) NOT NULL,
  `permissao_excluir_cliente_conta` int(1) NOT NULL,
  `permissao_cadastrar_colaborador_conta` int(1) NOT NULL,
  `permissao_visualizar_colaborador_conta` int(1) NOT NULL,
  `permissao_editar_colaborador_conta` int(1) NOT NULL,
  `permissao_excluir_colaborador_conta` int(1) NOT NULL,
  `permissao_cadastrar_conta_conta` int(1) NOT NULL,
  `permissao_visualizar_conta_conta` int(1) NOT NULL,
  `permissao_editar_conta_conta` int(1) NOT NULL,
  `permissao_excluir_conta_conta` int(1) NOT NULL,
  `permissao_cadastrar_estoque_conta` int(1) NOT NULL,
  `permissao_visualizar_estoque_conta` int(1) NOT NULL,
  `permissao_editar_estoque_conta` int(1) NOT NULL,
  `permissao_excluir_estoque_conta` int(1) NOT NULL,
  `permissao_cadastrar_financeiro_conta` int(1) NOT NULL,
  `permissao_visualizar_financeiro_conta` int(1) NOT NULL,
  `permissao_editar_financeiro_conta` int(1) NOT NULL,
  `permissao_excluir_financeiro_conta` int(1) NOT NULL,
  `permissao_cadastrar_fornecedor_conta` int(1) NOT NULL,
  `permissao_visualizar_fornecedor_conta` int(1) NOT NULL,
  `permissao_editar_fornecedor_conta` int(1) NOT NULL,
  `permissao_excluir_fornecedor_conta` int(1) NOT NULL,
  `permissao_cadastrar_produto_conta` int(1) NOT NULL,
  `permissao_visualizar_produto_conta` int(1) NOT NULL,
  `permissao_editar_produto_conta` int(1) NOT NULL,
  `permissao_excluir_produto_conta` int(1) NOT NULL,
  `permissao_cadastrar_servico_conta` int(1) NOT NULL,
  `permissao_visualizar_servico_conta` int(1) NOT NULL,
  `permissao_editar_servico_conta` int(1) NOT NULL,
  `permissao_excluir_servico_conta` int(1) NOT NULL,
  `permissao_cadastrar_centro_custo_conta` int(1) NOT NULL,
  `permissao_visualizar_centro_custo_conta` int(1) NOT NULL,
  `permissao_editar_centro_custo_conta` int(1) NOT NULL,
  `permissao_excluir_centro_custo_conta` int(1) NOT NULL,
  `permissao_cadastrar_banco_conta` int(1) NOT NULL,
  `permissao_visualizar_banco_conta` int(1) NOT NULL,
  `permissao_editar_banco_conta` int(1) NOT NULL,
  `permissao_excluir_banco_conta` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Fazendo dump de dados para tabela `conta`
--

INSERT INTO `conta` (`codigo_conta`, `codigo_empresa_conta`, `status_conta`, `usuario_conta`, `senha_conta`, `permissao_administrador_conta`, `permissao_cadastrar_cliente_conta`, `permissao_visualizar_cliente_conta`, `permissao_editar_cliente_conta`, `permissao_excluir_cliente_conta`, `permissao_cadastrar_colaborador_conta`, `permissao_visualizar_colaborador_conta`, `permissao_editar_colaborador_conta`, `permissao_excluir_colaborador_conta`, `permissao_cadastrar_conta_conta`, `permissao_visualizar_conta_conta`, `permissao_editar_conta_conta`, `permissao_excluir_conta_conta`, `permissao_cadastrar_estoque_conta`, `permissao_visualizar_estoque_conta`, `permissao_editar_estoque_conta`, `permissao_excluir_estoque_conta`, `permissao_cadastrar_financeiro_conta`, `permissao_visualizar_financeiro_conta`, `permissao_editar_financeiro_conta`, `permissao_excluir_financeiro_conta`, `permissao_cadastrar_fornecedor_conta`, `permissao_visualizar_fornecedor_conta`, `permissao_editar_fornecedor_conta`, `permissao_excluir_fornecedor_conta`, `permissao_cadastrar_produto_conta`, `permissao_visualizar_produto_conta`, `permissao_editar_produto_conta`, `permissao_excluir_produto_conta`, `permissao_cadastrar_servico_conta`, `permissao_visualizar_servico_conta`, `permissao_editar_servico_conta`, `permissao_excluir_servico_conta`, `permissao_cadastrar_centro_custo_conta`, `permissao_visualizar_centro_custo_conta`, `permissao_editar_centro_custo_conta`, `permissao_excluir_centro_custo_conta`, `permissao_cadastrar_banco_conta`, `permissao_visualizar_banco_conta`, `permissao_editar_banco_conta`, `permissao_excluir_banco_conta`) VALUES
(1, 1, 1, 'felipe', 'yn6zFQrE1q27Iiaof6Ci5g==', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 1, 1, 'luciana', 'j5ZDE7kHviHJz049tIEysw==', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresa`
--

CREATE TABLE IF NOT EXISTS `empresa` (
`codigo_empresa` int(11) NOT NULL,
  `nome_empresa` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `status_empresa` int(1) NOT NULL,
  `permissao_cliente_empresa` int(1) NOT NULL,
  `permissao_colaborador_empresa` int(1) NOT NULL,
  `permissao_conta_empresa` int(1) NOT NULL,
  `permissao_estoque_empresa` int(1) NOT NULL,
  `permissao_financeiro_empresa` int(1) NOT NULL,
  `permissao_fornecedor_empresa` int(1) NOT NULL,
  `permissao_produto_empresa` int(1) NOT NULL,
  `permissao_servico_empresa` int(1) NOT NULL,
  `permissao_centro_custo_empresa` int(1) NOT NULL,
  `permissao_banco_empresa` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Fazendo dump de dados para tabela `empresa`
--

INSERT INTO `empresa` (`codigo_empresa`, `nome_empresa`, `status_empresa`, `permissao_cliente_empresa`, `permissao_colaborador_empresa`, `permissao_conta_empresa`, `permissao_estoque_empresa`, `permissao_financeiro_empresa`, `permissao_fornecedor_empresa`, `permissao_produto_empresa`, `permissao_servico_empresa`, `permissao_centro_custo_empresa`, `permissao_banco_empresa`) VALUES
(1, 'Bamboo', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `imovel`
--

CREATE TABLE IF NOT EXISTS `imovel` (
`id_imovel` int(11) NOT NULL,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float(10,8) NOT NULL,
  `longitude` float(11,8) NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` float(10,2) NOT NULL,
  `codigo_empresa` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Fazendo dump de dados para tabela `imovel`
--

INSERT INTO `imovel` (`id_imovel`, `nome`, `descricao`, `endereco`, `cep`, `latitude`, `longitude`, `imagem`, `valor`, `codigo_empresa`) VALUES
(2, 'Apartamento 3 quartos', 'Lorem Ipsum', 'Rua Tenente Coronel Cardoso 898 Campos dos Goytacazes Brasil', '22.876-000', -21.75566864, -41.33570099, 'https://s3-sa-east-1.amazonaws.com/finxi/sala-pequena-e-arrumada.jpg', 1000.00, 1),
(3, 'Infolink', '', 'Avenida das Américas 500 Barra da Tijuca Rio de Janeiro', '22777670', -23.00372505, -43.31765366, 'https://s3.amazonaws.com/finxi2/1461185514.png', 100.00, 1),
(4, 'Flags', '', 'Avenida Adolpho de Vasconcelos 444 Barra da Tijuca', '22987654', -23.00989723, -43.42969894, 'https://s3.amazonaws.com/nat74/1461370287.jpg', 200.00, 1),
(5, 'Barra Shopping', '', 'Avenida das Américas 4500 Barra da Tijuca Rio de Janeiro', '8786786876', -22.99952888, -43.35775375, 'https://s3.amazonaws.com/2016-04-22-07-19-115305/1461370752.jpg', 250.00, 1),
(6, 'Anatel', '', 'Rua da Assembléia 6 Centro Rio de Janeiro', '8768756756', -22.90404320, -43.17459869, 'https://s3.amazonaws.com/nat74/1461370287.jpg', 400.00, 1),
(7, 'Quality', '', 'Avenida Rio Branco 114 Centro Rio de Janeiro', '8978687678', -22.90433884, -43.17822266, 'https://s3.amazonaws.com/2016-04-22-07-19-024779/1461370742.jpg', 350.00, 1),
(8, 'Falcão', '', 'Rua Siqueira Campos 100 Copacabana Rio de Janeiro', '5664543453', -22.96643829, -43.18709564, 'https://s3.amazonaws.com/2016-04-22-07-18-52380/1461370733.jpg', 230.00, 1),
(9, 'Central', '', 'Avenida Presidente Vargas Centro Rio de Janeiro', '7576576576', -22.90543365, -43.19120789, 'https://s3.amazonaws.com/2016-04-22-07-17-553482/1461370676.jpg', 220.00, 1),
(10, 'Mirataia', '', 'Rua Mirataia 350 Pechincha Rio de Janeiro', '6757657657', -22.93612289, -43.36290741, 'https://s3-sa-east-1.amazonaws.com/finxi/sala-pequena-e-arrumada.jpg', 540.00, 1),
(11, 'Comfio', '', 'Rua Dilermando Cruz 179 Tijuca Rio de Janeiro', '7676576576', -22.93799973, -43.24405289, 'https://s3.amazonaws.com/2016-04-22-07-17-553482/1461370676.jpg', 380.00, 1),
(12, 'Clínicas Integradas da Taquara', '', 'Avenida Nelson Cardoso 500', '2876786786', -22.91880035, -43.36593246, 'https://s3.amazonaws.com/2016-04-22-07-17-553482/1461370676.jpg', 2000.00, 1);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `conta`
--
ALTER TABLE `conta`
 ADD PRIMARY KEY (`codigo_conta`), ADD KEY `codigo_empresa_conta` (`codigo_empresa_conta`);

--
-- Índices de tabela `empresa`
--
ALTER TABLE `empresa`
 ADD PRIMARY KEY (`codigo_empresa`);

--
-- Índices de tabela `imovel`
--
ALTER TABLE `imovel`
 ADD PRIMARY KEY (`id_imovel`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `conta`
--
ALTER TABLE `conta`
MODIFY `codigo_conta` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `empresa`
--
ALTER TABLE `empresa`
MODIFY `codigo_empresa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `imovel`
--
ALTER TABLE `imovel`
MODIFY `id_imovel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
